#< envPaths
errlogInit(20000)

dbLoadDatabase("$(TOP)/dbd/tpxDetectorApp.dbd")
tpxDetectorApp_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("PREFIX", "13TIM1:")
epicsEnvSet("PORT",   "TIM1")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "2048")
epicsEnvSet("YSIZE",  "2048")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")

# Create a TPX driver
# tpxDetectorConfig(const char *portName, maxBuffers, size_t maxMemory, int priority, int stackSize)
# tpxDetectorConfig(portName,    # The name of the asyn port to be created
#                 maxBuffers,  # Maximum number of NDArray buffers driver can allocate -1=unlimited
#                 maxMemory,   # Maximum memory bytes driver can allocate. -1=unlimited
#                 priority,    # EPICS thread priority for asyn port driver 0=default
#                 stackSize,   # EPICS thread stack size for asyn port driver 0=default

#tpxDetectorConfig("$(PORT)", 1000000, 2000000000000)

tpxDetectorConfig("$(PORT)", 50, -1,0,0)

asynSetTraceIOMask($(PORT), 0, 2)
#asynSetTraceMask($(PORT),0,0xff)

dbLoadRecords("$(ADCORE)/db/ADBase.template",     "P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(AREA_DETECTOR)/ADTimepixDetector/tpxDetectorApp/Db/tpxDetector.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")

# Create a standard arrays plugin, set it to get data NDArrayBase.templatefrom Driver.
NDStdArraysConfigure("Image1", 3, 0, "$(PORT)", 0)
dbLoadRecords("$(ADCORE)/db/NDPluginBase.template","P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,TYPE=Int16,SIZE=16,FTVL=SHORT,NELEMENTS=10000000")

# Load all other plugins using commonPlugins.cmd
< $(ADCORE)/iocBoot/commonPlugins.cmd

iocInit()

# save things every 3 hundr seconds
create_monitor_set("auto_settings.req", 300, "P=$(PREFIX)")


