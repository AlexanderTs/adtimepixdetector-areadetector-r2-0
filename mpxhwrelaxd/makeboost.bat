call "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86
pushd .
cd boost_1_52_0
call bootstrap.bat msvc
b2 --toolset=msvc-10.0 address-model=64 runtime-link=static link=static -j 8 stage -a --with-system --with-program_options --with-thread --with-chrono --with-date_time --with-regex
popd .