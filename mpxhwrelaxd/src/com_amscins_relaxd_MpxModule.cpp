#include <jni.h>
#include "mpxmodule.h"
#include "mpxerrors.h"
#include "com_amscins_relaxd_MpxModule.h"

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    newMpxModule
 * Signature: (I)J
 */
JNIEXPORT jlong JNICALL Java_com_amscins_relaxd_MpxModule_newMpxModule
  (JNIEnv *env, jobject obj, jint hwId) {
	return (jlong) (new MpxModule(hwId));
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    getDevInfo
 * Signature: (J)[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_amscins_relaxd_MpxModule_getDevInfo
  (JNIEnv *env, jobject obj, jlong handle) {
	MpxModule* module = (MpxModule*) handle;

	DevInfo devInfo;

    module->getDevInfo(&devInfo);

	size_t size = sizeof(devInfo);
	jbyteArray result = env->NewByteArray(sizeof(devInfo));
	env->SetByteArrayRegion(result, 0, size, (jbyte*) (&devInfo));
	return result;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    hwInfoCount
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_hwInfoCount
  (JNIEnv *env, jobject obj, jlong handle) {
	MpxModule* module = (MpxModule*) handle;
	return module->hwInfoCount();
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    init
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_init
  (JNIEnv *env, jobject obj, jlong handle) {
	MpxModule* module = (MpxModule*) handle;
	return module->init();
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    getHwInfo
 * Signature: (JI)[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_amscins_relaxd_MpxModule_getHwInfo
  (JNIEnv *env, jobject obj, jlong handle, jint index) {
	MpxModule* module = (MpxModule*) handle;

	HwInfoItem hwInfo;
	int dataSize = 0;
    module->getHwInfo((int) index, &hwInfo, &dataSize);

	hwInfo.data = malloc(dataSize);
    module->getHwInfo((int) index, &hwInfo, &dataSize);

	size_t size = 3 * 4 + strlen(hwInfo.name) + 1 + strlen(hwInfo.descr) + 1 + dataSize;

	jbyteArray result = env->NewByteArray(size);
	int bIndex = 0;
	int siz = 3 * 4;
	env->SetByteArrayRegion(result, bIndex, siz, (jbyte*) (&hwInfo));
	bIndex += siz; siz = strlen(hwInfo.name) + 1;
	env->SetByteArrayRegion(result, bIndex, siz, (jbyte*) hwInfo.name);
	bIndex += siz; siz = strlen(hwInfo.descr) + 1;
	env->SetByteArrayRegion(result, bIndex, siz, (jbyte*) hwInfo.descr);
	bIndex += siz; siz = dataSize;
	env->SetByteArrayRegion(result, bIndex, siz, (jbyte*) hwInfo.data);

	free(hwInfo.data);
	return result;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    getHwInfoFlags
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_getHwInfoFlags
  (JNIEnv *env, jobject obj, jlong handle, jint index) {
	MpxModule* module = (MpxModule*) handle;
	u32 flags;
	module->getHwInfoFlags((int) index, &flags);
	return (jint) flags;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    setHwInfo
 * Signature: (JI[B)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_setHwInfo
  (JNIEnv *env, jobject obj, jlong handle, jint index, jbyteArray jinfo) {
	MpxModule* module = (MpxModule*) handle;

	jboolean copy=true;
	jbyte *data = env->GetByteArrayElements(jinfo, &copy);
	int result = module->setHwInfo(index, data, env->GetArrayLength(jinfo));
	env->ReleaseByteArrayElements(jinfo, data, JNI_ABORT);
	return result;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    getAcqTime
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL Java_com_amscins_relaxd_MpxModule_getAcqTime
  (JNIEnv *env, jobject obj, jlong handle) {
	jdouble result;
	((MpxModule*) handle)->getAcqTime(&result);
	return result;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    setAcqPars
 * Signature: (J[B)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_setAcqPars
  (JNIEnv *env, jobject obj, jlong handle, jbyteArray acqPars) {
	MpxModule* module = (MpxModule*) handle;
	jboolean copy=true;
	AcqParams *acqParams = (AcqParams*) env->GetByteArrayElements(acqPars, &copy);
	int result = module -> setAcqPars(acqParams);
	env->ReleaseByteArrayElements(acqPars, (jbyte*) acqParams, JNI_ABORT);
	return (jint) result;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    startAcquisition
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_startAcquisition
  (JNIEnv *env, jobject obj, jlong handle) {
	return ((MpxModule*) handle) -> startAcquisition();
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    stopAcquisition
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_stopAcquisition
  (JNIEnv *env, jobject obj, jlong handle) {
	return ((MpxModule*) handle) -> stopAcquisition();
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    configAcqMode
 * Signature: (JZZD)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_configAcqMode
  (JNIEnv *env, jobject obj, jlong handle, jboolean ext_trig, jboolean use_timer, jdouble time_s) {
	MpxModule* module = (MpxModule*) handle;
	return (jint) module->configAcqMode((bool) ext_trig, (bool) use_timer, (double) time_s);
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    setFsrs
 * Signature: (J[SBB[II)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_setFsrs
  (JNIEnv *env, jobject obj, jlong handle, jshortArray dacs, jbyte senseChip, jbyte extDacChip, jintArray codes, jint tpReg) {
	MpxModule* module = (MpxModule*) handle;

	jboolean copy=true;
	jshort *dacVals = env->GetShortArrayElements(dacs, &copy);
	jint *codeVals = codes ? env->GetIntArrayElements(codes, &copy) : NULL;
	jint ret = module->setFsrs((u16*) dacVals, env->GetArrayLength(dacs), senseChip, extDacChip, (int*) codeVals, tpReg);
	env->ReleaseShortArrayElements(dacs, dacVals, JNI_ABORT);
	if (codeVals) env->ReleaseIntArrayElements(codes, codeVals, JNI_ABORT);
	return ret;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    setFsr
 * Signature: (JI[II)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_setFsr
(JNIEnv *env, jobject obj, jlong handle, jint chipnr, jintArray dac, jint col_test_pulse_reg) {
	MpxModule* module = (MpxModule*) handle;
	jboolean copy=true;
	jint *dacs = env->GetIntArrayElements(dac, &copy);
	int ret = module->setFsr((int) chipnr, (int*) dacs, (int) col_test_pulse_reg);
	env->ReleaseIntArrayElements(dac, dacs, JNI_ABORT);
	return (jint) ret;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    refreshFsr
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_refreshFsr
  (JNIEnv *env, jobject obj, jlong handle, jint chipnr) {
	MpxModule* module = (MpxModule*) handle;
	return (jint) module->refreshFsr((int) chipnr);
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    getMpxDac
 * Signature: (JI)D
 */
JNIEXPORT jdouble JNICALL Java_com_amscins_relaxd_MpxModule_getMpxDac
  (JNIEnv *env, jobject obj, jlong handle, jint chipnr) {
	MpxModule* module = (MpxModule*) handle;
	double result = 0;
	int ret = module->getMpxDac((int) chipnr, &result);
	return (jdouble) (ret == 0 ? result : ret);
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    setPixelsCfg
 * Signature: (J[BI)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_setPixelsCfg
  (JNIEnv *env, jobject obj, jlong handle, jbyteArray cfg, jint chipnr) {
	MpxModule* module = (MpxModule*) handle;
	jboolean copy=true;
	jbyte *cfgData = env->GetByteArrayElements(cfg, &copy);
	int result = module->setPixelsCfg((u8*) cfgData, (int) chipnr);
	env->ReleaseByteArrayElements(cfg, cfgData, JNI_ABORT);
	return (jint) result;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    writeMatrix
 * Signature: (J[S)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_writeMatrix
  (JNIEnv *env, jobject obj, jlong handle, jshortArray jBuffer) {
	MpxModule* module = (MpxModule*) handle;
	jboolean copy=true;
	jshort *buffer=env->GetShortArrayElements(jBuffer, &copy);
	jint len=env->GetArrayLength(jBuffer);

	jint ret=module->readMatrix(buffer, len);

	env->ReleaseShortArrayElements(jBuffer, buffer, JNI_ABORT);
	return ret;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    readMatrix
 * Signature: (J[S)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_readMatrix__J_3S
  (JNIEnv *env, jobject obj, jlong handle, jshortArray jBuffer) {
	MpxModule* module = (MpxModule*) handle;

	jboolean copy=false;
	jshort *buffer=env->GetShortArrayElements(jBuffer, &copy);
	jint len=env->GetArrayLength(jBuffer);
	jint ret=module->readMatrix(buffer, len);

	env->ReleaseShortArrayElements(jBuffer, buffer, 0);
	return ret;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    readMatrix
 * Signature: (JLjava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_readMatrix__JLjava_nio_ByteBuffer_2
  (JNIEnv *env, jobject obj, jlong handle, jobject jBuffer) {

	MpxModule* module = (MpxModule*) handle;
	jshort *buffer=(jshort*) env->GetDirectBufferAddress(jBuffer);
	jint len=env->GetDirectBufferCapacity(jBuffer) / 2;
	jint ret=module->readMatrix(buffer, len);

	return ret;
}
/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    readMatrixRaw
 * Signature: (J[B)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_readMatrixRaw__J_3B
  (JNIEnv *env, jobject obj, jlong handle, jbyteArray jByteArray) {
	MpxModule* module = (MpxModule*) handle;

	int lost = 0;
	jboolean copy=false;
	jbyte *byteArray=env->GetByteArrayElements(jByteArray, &copy);
	u32 len= (u32) env->GetArrayLength(jByteArray);
	jint ret=module->readMatrixRaw((u8*) byteArray, &len, &lost);

	env->ReleaseByteArrayElements(jByteArray, byteArray, 0);
	return (ret == 0 || ret == MPXERR_FRAME_LOST_ROWS) ? lost : ret;
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    readMatrixRaw
 * Signature: (JLjava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_readMatrixRaw__JLjava_nio_ByteBuffer_2
  (JNIEnv *env, jobject obj, jlong handle, jobject jBuffer) {
	MpxModule* module = (MpxModule*) handle;

	int lost = 0;
	u8 *byteArray= (u8*) env->GetDirectBufferAddress(jBuffer);
	u32 len= (u32) env->GetDirectBufferCapacity(jBuffer);
	jint ret=module->readMatrixRaw(byteArray, &len, &lost);

	return (ret == 0 || ret == MPXERR_FRAME_LOST_ROWS) ? lost : ret;
}


/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    resetMatrix
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_resetMatrix
  (JNIEnv *env, jobject obj, jlong handle) {
	MpxModule* module = (MpxModule*) handle;
	return module->resetMatrix();
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    reset
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_amscins_relaxd_MpxModule_reset
  (JNIEnv *env, jobject obj, jlong handle) {
	MpxModule* module = (MpxModule*) handle;
	return module->reset();
}

/*
 * Class:     com_amscins_relaxd_MpxModule
 * Method:    isBusy
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL Java_com_amscins_relaxd_MpxModule_isBusy
  (JNIEnv *env, jobject obj, jlong handle) {
	MpxModule* module = (MpxModule*) handle;
	bool busy;
	module->getBusy(&busy);
	return (jboolean) busy;
}
