#include "Frame.h"

extern AsiDecodeWord_t _lut[];
extern const AsiDecodeWord_t _ilut[];

void applyLookupTable(const int startRow, const int endRow, AsiFrame<AsiDecodeWord_t> *output)
{
    int i;
    int row;
    // Apply Look-Up Table
    for (row = startRow; row < endRow; row++)
    {
        AsiDecodeWord_t *pstream = AsiFrame<AsiDecodeWord_t>::getFrameListRowPtr(output, row);
        for (i = 0; i < output->getRowElements(); i++)
        {
            pstream[i] = _lut[pstream[i]];
        }
    }
}

const AsiDecodeWord_t *getLookupTable()
{
    return _lut;
}

const AsiDecodeWord_t *getInverseLookupTable()
{
    return _ilut;
}

void updateInvalidLutEntry(bool invalidLutZero)
{
    if (invalidLutZero == false)
    {
        _lut[0] = (const AsiDecodeWord_t)-1;
    }
    else
    {
        _lut[0] = (const AsiDecodeWord_t)0;
    }
}