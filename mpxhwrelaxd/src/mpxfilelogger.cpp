#include "mpxfilelogger.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/types.h>  // For stat().
#include <sys/stat.h>   // For stat().


#if defined(_WINDLL) || defined(WIN32)

#include <io.h>   // For access().
#include <time.h>
#include <Shlobj.h>

#else

#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include <pwd.h>

#endif



void MpxFileLogger::mkDir(std::string dir)
{
#if defined(_WINDLL) || defined(WIN32)
	size_t f;
	f = dir.find("/");
	while (f != std::string::npos) {
		dir.replace(f, 1, "\\");
		f = dir.find("/");
	}
					 
	CreateDirectory (dir.c_str(), NULL);
#else
    mkdir(dir.c_str(), 0777);
#endif
}

bool MpxFileLogger::dirExists(std::string dir) 
{
#if defined(_WINDLL) || defined(WIN32)
	if ( _access( dir.c_str(), 0 ) == 0 )
		return true;
	return false;
#else
	struct stat st;
	if(stat(dir.c_str(), &st) == 0)
		return true;
	return false;
#endif
}

std::string MpxFileLogger::homeDir() 
{
#if defined(_WINDLL) || defined(WIN32)
/*	// >= Windows Vista
	LPWSTR wszPath = NULL;
	SHGetKnownFolderPath(FOLDERID_Profile, 0, NULL, &wszPath);
	std::wstring hDirW(wszPath);
	std::string hDir(hDirW.length(), ' ');
	std::copy(hDirW.begin(),hDirW.end(), hDir.begin());
*/
	TCHAR szPath[MAX_PATH];
	if (!SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROFILE | CSIDL_FLAG_CREATE, 
        NULL, 0, szPath))) {
		return ".";
	}

	return std::string(szPath);
#else
  // try to get HOME environment variable
  char * userHome = ::getenv(ENV_HOME);
  if (userHome != NULL) {
    return std::string(userHome);
  }
  // else get it from the passwd struct
  passwd *pw = getpwuid(getuid());
  
  const char * homedir = pw->pw_dir;

  return std::string(homedir);
#endif
}


MpxFileLogger::MpxFileLogger(const std::string &dir, std::string prefix, std::string suffix) : MpxLogger()
{
    std::string targetdir = dir;
	if (targetdir.length() == 0) 
    {
		char * mpxLogDirEnv = getenv(MPX_LOG_DIR_ENV_NAME);
		if (mpxLogDirEnv == NULL) 
        {
			targetdir = homeDir() + MPX_LOG_DIR_NAME;
		}
        else 
        {
			targetdir = std::string(mpxLogDirEnv);
		}
	}

	if (!dirExists(targetdir))
	{
		mkDir(targetdir);
	}

	// Open a log file with an appropriate name
	std::ostringstream oss;
    oss << targetdir << "/" << prefix << "-"
        << ( int ) this->getTime()
        << "." << suffix;
    // Create directory if it doesn't exist?
	std::string logname = oss.str();
    _out.open( logname.c_str() );

    if ( !_out.is_open() )
    {
		// for now we'll just end it.
		throw "Failed to open Log";
    }
	// configure epouch time (seconds since 1970), no neat date formatting here.
	
}

void MpxFileLogger::log(MpxLogLevel level, std::string message)
{
	switch (level) {
		case MPX_TRACE:	_out << "[TRACE] "; break;
		case MPX_DEBUG:	_out << "[DEBUG] "; break;
		case MPX_INFO:	_out << "[INFO ] "; break;
		case MPX_WARN:	_out << "[WARN ] "; break;
		case MPX_ERROR:	_out << "[ERROR] "; break;
		case MPX_FATAL:	_out << "[FATAL] "; break;
		default:    _out << "[?????] ";
	}
	_out << std::setw(12) << std::setprecision(3) << std::fixed;
	_out << getTime() << " ";
	_out << message << std::endl;
}


#if defined(_WINDLL) || defined(WIN32)
double MpxFileLogger::getTime()
{
    static LARGE_INTEGER perfFrequency = {0};
    LARGE_INTEGER currentCount;

    if ( perfFrequency.QuadPart == 0 )
        QueryPerformanceFrequency( &perfFrequency );

    QueryPerformanceCounter( &currentCount );
    return currentCount.QuadPart / static_cast<double> ( perfFrequency.QuadPart );
//	return 0;
}
#else // Linux
double MpxFileLogger::getTime()
{
    struct timeval tval;
    gettimeofday( &tval, 0 );

//  return ( 0.000001 * tval.tv_nsec ) + tval.tv_sec;
    return ( 0.000001 * tval.tv_usec ) + tval.tv_sec;
}	
#endif

MpxFileLogger::~MpxFileLogger(void)
{
	_out.close();
}
