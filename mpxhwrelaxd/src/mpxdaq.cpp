/* File to hold stuff that is specific to windows type operating systems */

#include "bt.h"
#include "mpxdaq.h"

MpxDaq::MpxDaq(MpxModule* mpxModule) : 
    _mpxModule(mpxModule)
{
    _thread = NULL;
    _condition = NULL;
    _mutex = NULL;
    _triggersReady = 0;
    _threadRun = true;
#ifdef MULTIPLE_TRIGS_PER_ACQ
    _condition = new boost::condition_variable();
    _mutex = new boost::mutex();
#endif
}

MpxDaq::~MpxDaq()
{
    deleteThread();
    if (_condition != NULL)
    {
        delete _condition;
    }
    if (_mutex != NULL)
    {
        delete _mutex;
    }
}

void MpxDaq::readyForTrigger()
{
    if (_condition != NULL)
    {
        boost::unique_lock<boost::mutex> lock(*_mutex);
        _triggersReady++;
        _condition->notify_one();
    }
}

void MpxDaq::waitForTriggerReady()
{
    if (_condition != NULL)
    {
        boost::unique_lock<boost::mutex> lock(*_mutex);
        while (_triggersReady <= 0)
        {
            _condition->wait(lock);
        }
        _triggersReady--;
    }
}

void MpxDaq::daqThread( )
{
    u32 cnt = 0;
    struct mpixd_reply_msg *mpxr;
    std::vector<mpixd_type> msgs;
    msgs.push_back(MPIXD_TRIGGER);

    // Make a callback to PixelMan

    while (_threadRun == true)
    {
        // Socket read is blocking:
        // when the (proper) message is received data should be available
        u32 header_type = 0;

        waitForTriggerReady();
        if (_threadRun == true)
        {
            _mpxModule->callbackToPixMan( HWCB_ACQSTARTED );
        }

        while (_threadRun == true)
        {
            mpxr = NULL;
            _mpxModule->rxMsg( &mpxr, msgs );
            if (mpxr != NULL)
            {
                header_type = ntohl( mpxr->header.type );

                // Trigger message ?
                if ( header_type == MPIXD_TRIGGER )
                {
                    break;
                }
                _mpxModule->_sockMgr->releaseRxMsg(&mpxr);
            }
        }

        if ((_threadRun == true) && (header_type == MPIXD_TRIGGER))
        {
            // There is data so make the callback(s) to PixelMan
            //mpx->callbackToPixMan( HWCB_ACQSTARTED ); ###SEE ABOVE
            _mpxModule->callbackToPixMan( HWCB_ACQFINISHED );
            ++cnt;
        }
    }

    return;
}

void MpxDaq::startDaqThread()
{

    // Ready for first event
    if (_mpxModule->_callback != NULL)
    {
        if (_thread == NULL)
        {
            _thread = new boost::thread(&MpxDaq::daqThread, this);
        }

        if (_thread == NULL)
        {
            std::ostringstream oss;
            oss  << "### Create thread failed ";
            _mpxModule->_log->error( oss.str() );
        }

        readyForTrigger();
    }
}

void MpxDaq::deleteThread()
{
    if (_thread != NULL)
    {
        // Make the thread -if still active- exit
        if (_thread != NULL)
        {
            _threadRun = false;
            readyForTrigger();
        }

        if (_thread->joinable())
        {
            _thread->join();
        }
        delete _thread;
        _thread = NULL;
    }
}

void MpxDaq::stopDaqThread()
{
}

