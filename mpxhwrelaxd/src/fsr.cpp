/*
       Copyright 2011 NIKHEF

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

*/
#include <iostream>
#include <iomanip>

#include "common.h"
#include "relaxd.h"
#include "fsr.h"

//-----------------------------------------------------------------------------

Fsr::Fsr( int nbits )
    : _nbits( nbits )
{
    // Number of bytes required for the requested number of bits
    int nchars = ( nbits + 7 ) / 8;

    // Allocate memory as multiple of 8 bytes
    int alloc_sz = nchars + ( 8 - ( nchars % 8 ) );
    _bytes = new unsigned char[alloc_sz];

    //_bytes = new unsigned char[nchars];

    // Initialize memory to zeroes
    for ( int i = 0; i < nchars; ++i )
        _bytes[i] = static_cast<unsigned char> ( 0x00 );

    _nbytes = nchars;
}

//-----------------------------------------------------------------------------

Fsr::~Fsr()
{
    delete [] _bytes;
}

//-----------------------------------------------------------------------------

Fsr &Fsr::operator=( const Fsr &f )
{
    // Copy the data bytes, as much as possible
    int i, sz = f.byteLength();

    if ( sz > _nbytes ) sz = _nbytes;

    unsigned char *b = f.bytes();

    for ( i = 0; i < sz; ++i ) _bytes[i] = b[i];

    return *this;
}

//-----------------------------------------------------------------------------

void Fsr::setOnes()
{
    for ( int i = 0; i < _nbytes; ++i ) _bytes[i] = ( unsigned char ) 0xFF;
}

//-----------------------------------------------------------------------------

void Fsr::setZeroes()
{
    for ( int i = 0; i < _nbytes; ++i ) _bytes[i] = ( unsigned char ) 0x00;
}

//-----------------------------------------------------------------------------

void Fsr::setBits( int i_dst, int nbits, int value )
{
    // 'nbits' limited to 32 !
    if ( nbits > 32 ) nbits = 32;

    int bitmask = 0x00000001;
    int i_char, i_bit;

    for ( int i = 0; i < nbits; ++i )
    {
        i_char = ( i_dst / 8 );
        i_bit  = ( i_dst & 0x7 );

        if ( ( value & bitmask ) != 0 )
            // Set bit
            _bytes[i_char] |= ( 1 << i_bit );
        else
            // Clear bit
            _bytes[i_char] &= ~( 1 << i_bit );

        bitmask <<= 1;
        ++i_dst;
    }
}

//-----------------------------------------------------------------------------

int Fsr::bits( int index, int nbits ) const
{
    int value   = 0x00000000;
    int bitmask = 0x00000001;
    int i_char;
    int i_bit;

    for ( int i = 0; i < nbits; ++i )
    {
        i_char = ( index / 8 );
        i_bit  = ( index & 0x7 );

        if ( ( _bytes[i_char] & ( 1 << i_bit ) ) != 0 )
            value |= bitmask;

        bitmask <<= 1;
        ++index;
    }

    return value;
}

//-----------------------------------------------------------------------------

void Fsr::log( std::ofstream &flog )
{
    flog << "length: " << this->bitLength() << " bits, "
         << this->byteLength() << " bytes" << std::endl;
    flog << std::setfill( '0' ) << std::setiosflags( std::ios::hex | std::ios::uppercase );
    flog << std::hex << std::uppercase;
    int i;

    for ( i = 0; i < this->byteLength(); ++i )
    {
        flog << std::setw( 2 );
        flog << static_cast<unsigned int>( _bytes[i] ) << " ";

        if ( ( ( i + 1 ) & 0xF ) == 0 ) flog << std::endl;
    }

    if ( ( ( i + 1 ) & 0xF ) != 0 ) flog << std::endl;

    flog << std::setfill( ' ' ) << std::setiosflags( std::ios::dec );
}

//-----------------------------------------------------------------------------

bool Fsr::toDacSettings( int type, int *dac, int *sz )
{
    if ( _nbits != MPIX_FSR_BITS )
    {
        std::cerr << "### Fsr::toDacSettings: bit length = " << _nbits
             << ", should be " << MPIX_FSR_BITS << std::endl;
        return false;
    }

    for ( int i = 0; i < 16; ++i ) dac[i] = 0;

    switch ( type )
    {
    case MPX_MXR:
        *sz = MXR_DACS;
        dac[MXR_IKRUM]     = this->bits( MXR_FSR_IKRUM_I, MXR_FSR_IKRUM_BITS );

        dac[MXR_DISC]      = this->bits( MXR_FSR_DISC_I, MXR_FSR_DISC_BITS );

        dac[MXR_PREAMP]    = this->bits( MXR_FSR_PREAMP_I, MXR_FSR_PREAMP_BITS );

        dac[MXR_BUFFA]     = this->bits( MXR_FSR_BUFANALOGA30_I,
                                         MXR_FSR_BUFANALOGA30_BITS );
        dac[MXR_BUFFA]    |= ( this->bits( MXR_FSR_BUFANALOGA74_I,
                                           MXR_FSR_BUFANALOGA74_BITS ) <<
                               MXR_FSR_BUFANALOGA30_BITS );

        dac[MXR_BUFFB]     = this->bits( MXR_FSR_BUFANALOGB40_I,
                                         MXR_FSR_BUFANALOGB40_BITS );
        dac[MXR_BUFFB]    |= ( this->bits( MXR_FSR_BUFANALOGB75_I,
                                           MXR_FSR_BUFANALOGB75_BITS ) <<
                               MXR_FSR_BUFANALOGB40_BITS );

        dac[MXR_DELAYN]    = this->bits( MXR_FSR_DELAYN_I, MXR_FSR_DELAYN_BITS );

        dac[MXR_THLFINE]   = ( this->bits( MXR_FSR_THL_I,
                                           MXR_FSR_THL_BITS ) & ( ( 1 << 10 ) - 1 ) );

        dac[MXR_THLCOARSE] = ( this->bits( MXR_FSR_THL_I,
                                           MXR_FSR_THL_BITS ) >> 10 );

        dac[MXR_THHFINE]   = ( this->bits( MXR_FSR_THH_I,
                                           MXR_FSR_THH_BITS ) & ( ( 1 << 10 ) - 1 ) );

        dac[MXR_THHCOARSE] = ( this->bits( MXR_FSR_THH_I,
                                           MXR_FSR_THH_BITS ) >> 10 );

        dac[MXR_FBK]       = this->bits( MXR_FSR_FBK_I, MXR_FSR_FBK_BITS );

        dac[MXR_GND]       = this->bits( MXR_FSR_GND_I, MXR_FSR_GND_BITS );

        dac[MXR_THS]       = this->bits( MXR_FSR_THS_I, MXR_FSR_THS_BITS );

        dac[MXR_BIASLVDS]  = this->bits( MXR_FSR_BIASLVDS_I,
                                         MXR_FSR_BIASLVDS_BITS );

        dac[MXR_REFLVDS]   = 0xff ^ this->bits( MXR_FSR_REFLVDS_I,
                                         MXR_FSR_REFLVDS_BITS );
        break;

    case MPX_TPX:
        *sz = TPX_DACS;
        dac[TPX_IKRUM]     = this->bits( TPX_FSR_IKRUM_I, TPX_FSR_IKRUM_BITS );

        dac[TPX_DISC]      = this->bits( TPX_FSR_DISC_I, TPX_FSR_DISC_BITS );

        dac[TPX_PREAMP]    = this->bits( TPX_FSR_PREAMP_I, TPX_FSR_PREAMP_BITS );

        dac[TPX_BUFFA]     = this->bits( TPX_FSR_BUFANALOGA30_I,
                                         TPX_FSR_BUFANALOGA30_BITS );
        dac[TPX_BUFFA]    |= ( this->bits( TPX_FSR_BUFANALOGA74_I,
                                           TPX_FSR_BUFANALOGA74_BITS ) <<
                               TPX_FSR_BUFANALOGA30_BITS );

        dac[TPX_BUFFB]     = this->bits( TPX_FSR_BUFANALOGB40_I,
                                         TPX_FSR_BUFANALOGB40_BITS );
        dac[TPX_BUFFB]    |= ( this->bits( TPX_FSR_BUFANALOGB75_I,
                                           TPX_FSR_BUFANALOGB75_BITS ) <<
                               TPX_FSR_BUFANALOGB40_BITS );

        dac[TPX_HIST]      = this->bits( TPX_FSR_HIST_I, TPX_FSR_HIST_BITS );

        dac[TPX_THLCOARSE] = ( this->bits( TPX_FSR_THL_I,
                                           TPX_FSR_THL_BITS ) >> 10 );

        dac[TPX_THLFINE]   = ( this->bits( TPX_FSR_THL_I,
                                           TPX_FSR_THL_BITS ) & ( ( 1 << 10 ) - 1 ) );

        dac[TPX_VCAS]      = this->bits( TPX_FSR_VCAS_I, TPX_FSR_VCAS_BITS );

        dac[TPX_FBK]       = this->bits( TPX_FSR_FBK_I, TPX_FSR_FBK_BITS );

        dac[TPX_GND]       = this->bits( TPX_FSR_GND_I, TPX_FSR_GND_BITS );

        dac[TPX_THS]       = this->bits( TPX_FSR_THS_I, TPX_FSR_THS_BITS );

        dac[TPX_BIASLVDS]  = this->bits( TPX_FSR_BIASLVDS_I,
                                         TPX_FSR_BIASLVDS_BITS );

        dac[TPX_REFLVDS]   = 0xff & this->bits( TPX_FSR_REFLVDS_I,
                                         TPX_FSR_REFLVDS_BITS );
        break;

    default:
        *sz = 0;
        std::cerr << "### Fsr::toDacSettings(): unknown device type " << type << std::endl;
        return false;
    }

    return true;
}

//-----------------------------------------------------------------------------

bool Fsr::fromDacSettings( int type, int *dac )
{
    if ( _nbits != MPIX_FSR_BITS )
    {
        std::cerr << "### Fsr::fromDacSettings: bit length = " << _nbits
             << ", should be " << MPIX_FSR_BITS << std::endl;
        return false;
    }

    this->setZeroes();

    switch ( type )
    {
    case MPX_MXR:
        this->setBits( MXR_FSR_IKRUM_I, MXR_FSR_IKRUM_BITS, dac[MXR_IKRUM] );
        this->setBits( MXR_FSR_DISC_I, MXR_FSR_DISC_BITS, dac[MXR_DISC] );
        this->setBits( MXR_FSR_PREAMP_I, MXR_FSR_PREAMP_BITS,
                       dac[MXR_PREAMP] );
        this->setBits( MXR_FSR_BUFANALOGA30_I, MXR_FSR_BUFANALOGA30_BITS,
                       dac[MXR_BUFFA] & ( ( 1 << MXR_FSR_BUFANALOGA30_BITS ) - 1 ) );
        this->setBits( MXR_FSR_BUFANALOGA74_I, MXR_FSR_BUFANALOGA74_BITS,
                       dac[MXR_BUFFA] >> MXR_FSR_BUFANALOGA30_BITS );
        this->setBits( MXR_FSR_BUFANALOGB40_I, MXR_FSR_BUFANALOGB40_BITS,
                       dac[MXR_BUFFB] & ( ( 1 << MXR_FSR_BUFANALOGB40_BITS ) - 1 ) );
        this->setBits( MXR_FSR_BUFANALOGB75_I, MXR_FSR_BUFANALOGB75_BITS,
                       dac[MXR_BUFFB] >> MXR_FSR_BUFANALOGB40_BITS );
        this->setBits( MXR_FSR_DELAYN_I, MXR_FSR_DELAYN_BITS,
                       dac[MXR_DELAYN] );
        this->setBits( MXR_FSR_THL_I, MXR_FSR_THL_BITS,
                       ( dac[MXR_THLCOARSE] << 10 ) | dac[MXR_THLFINE] );
        this->setBits( MXR_FSR_THH_I, MXR_FSR_THH_BITS,
                       ( dac[MXR_THHCOARSE] << 10 ) | dac[MXR_THHFINE] );
        this->setBits( MXR_FSR_FBK_I, MXR_FSR_FBK_BITS, dac[MXR_FBK] );
        this->setBits( MXR_FSR_GND_I, MXR_FSR_GND_BITS, dac[MXR_GND] );
        this->setBits( MXR_FSR_THS_I, MXR_FSR_THS_BITS, dac[MXR_THS] );
        this->setBits( MXR_FSR_BIASLVDS_I, MXR_FSR_BIASLVDS_BITS,
                       dac[MXR_BIASLVDS] );
        this->setBits( MXR_FSR_REFLVDS_I, MXR_FSR_REFLVDS_BITS,
                       dac[MXR_REFLVDS] );
        break;

    case MPX_TPX:
        this->setBits( TPX_FSR_IKRUM_I, TPX_FSR_IKRUM_BITS, dac[TPX_IKRUM] );
        this->setBits( TPX_FSR_DISC_I, TPX_FSR_DISC_BITS, dac[TPX_DISC] );
        this->setBits( TPX_FSR_PREAMP_I, TPX_FSR_PREAMP_BITS,
                       dac[TPX_PREAMP] );
        this->setBits( TPX_FSR_BUFANALOGA30_I, TPX_FSR_BUFANALOGA30_BITS,
                       dac[TPX_BUFFA] & ( ( 1 << TPX_FSR_BUFANALOGA30_BITS ) - 1 ) );
        this->setBits( TPX_FSR_BUFANALOGA74_I, TPX_FSR_BUFANALOGA74_BITS,
                       dac[TPX_BUFFA] >> TPX_FSR_BUFANALOGA30_BITS );
        this->setBits( TPX_FSR_BUFANALOGB40_I, TPX_FSR_BUFANALOGB40_BITS,
                       dac[TPX_BUFFB] & ( ( 1 << TPX_FSR_BUFANALOGB40_BITS ) - 1 ) );
        this->setBits( TPX_FSR_BUFANALOGB75_I, TPX_FSR_BUFANALOGB75_BITS,
                       dac[TPX_BUFFB] >> TPX_FSR_BUFANALOGB40_BITS );
        this->setBits( TPX_FSR_HIST_I, TPX_FSR_HIST_BITS, dac[TPX_HIST] );
        this->setBits( TPX_FSR_THL_I, TPX_FSR_THL_BITS,
                       ( dac[TPX_THLCOARSE] << 10 ) | dac[TPX_THLFINE] );
        this->setBits( TPX_FSR_VCAS_I, TPX_FSR_VCAS_BITS, dac[TPX_VCAS] );
        this->setBits( TPX_FSR_FBK_I, TPX_FSR_FBK_BITS, dac[TPX_FBK] );
        this->setBits( TPX_FSR_GND_I, TPX_FSR_GND_BITS, dac[TPX_GND] );
        this->setBits( TPX_FSR_THS_I, TPX_FSR_THS_BITS, dac[TPX_THS] );
        this->setBits( TPX_FSR_BIASLVDS_I, TPX_FSR_BIASLVDS_BITS,
                       dac[TPX_BIASLVDS] );
        this->setBits( TPX_FSR_REFLVDS_I, TPX_FSR_REFLVDS_BITS,
                       dac[TPX_REFLVDS] );
        break;

    default:
        std::cerr << "### Fsr::fromDacSettings(): unknown device type "
             << type << std::endl;
        return false;
    }

    return true;
}

//-----------------------------------------------------------------------------
