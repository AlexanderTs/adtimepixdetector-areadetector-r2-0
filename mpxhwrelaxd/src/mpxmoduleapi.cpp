/*
       Copyright 2011 NIKHEF

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

*/
#include "mpxmodulemgrpimpl.h"
#include "mpxmoduleapi.h"
#include "mpxmodulemgr.h"
#include "mpxplatform.h"
#include "mpxerrors.h"
#include <iostream>
#include <boost/bind/protect.hpp>
// ----------------------------------------------------------------------------
// MPX2 Interface

Mpx2Interface mpx2Interface =
{
    mpx2FindDevices,
    mpx2Init,
    mpx2CloseDevice,
    mpx2SetCallback,
    mpx2SetCallbackData,
    mpx2GetHwInfoCount,
    mpx2GetHwInfoFlags,
    mpx2GetHwInfo,
    mpx2SetHwInfo,
    mpx2GetDevInfo,
    mpx2Reset,
    mpx2SetDACs,
    mpx2GetMpxDacVal,
    mpx2SetExtDacVal,
    mpx2SetPixelsCfg,
    mpx2SetAcqPars,
    mpx2StartAcquisition,
    mpx2StopAcquisition,
    mpx2GetAcqTime,
    mpx2ResetMatrix,
    mpx2ReadMatrix,
    mpx2WriteMatrix,
    mpx2SendTestPulses,
    mpx2IsBusy,
    mpx2GetLastError,
    mpx2GetLastDevError,
    mpx2ReadMatrixRaw
};

// ----------------------------------------------------------------------------

MPXMODULE_API Mpx2Interface* getMpx2Interface()
{
    return &mpx2Interface;
}

// ----------------------------------------------------------------------------

int mpx2FindDevices(int ids[], int* count)
{
    int ret = MPXERR_NOERROR;
    int hw_id, cnt = 0;

    MpxModuleMgr::instance()->scanDevices();
    for (hw_id = 0; hw_id < 32; hw_id++)
    {
        MpxModule* p = MpxModuleMgr::instance()->device(hw_id);
        if (p != NULL)
        {
            ids[cnt] = hw_id;
            ++cnt;
        }
    }
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    if (groupSize > 0)
    {
        *count = (cnt / MpxModuleMgr::instance()->getGroupSize());
    }
    else
    {
        *count = 0;
        ret = MPXERR_INVALID_PARVAL;
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2Init(int hw_id)
{
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->init();
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2CloseDevice(int hw_id)
{
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }
    }

    return 0;
}

// ----------------------------------------------------------------------------

int mpx2SetCallback(HwCallback callback)
{
    int hw_id = 0;
    MpxModule* dev;

    do
    {
        dev = MpxModuleMgr::instance()->device(hw_id++);

        if (dev)
        {
            dev->setCallback(callback);
        }
    } while (dev != NULL);

    return 0;
}

// ----------------------------------------------------------------------------

int mpx2SetCallbackData(int hw_id, INTPTR data)
{
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->setCallbackData(data);
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2GetHwInfoCount()
{
    MpxModule* dev = MpxModuleMgr::instance()->device(0);

    if (!dev)
    {
        return 0;
    }

    return dev->hwInfoCount();
}

// ----------------------------------------------------------------------------

int mpx2GetHwInfoFlags(int hw_id, int index, u32* flags)
{
    MpxModule* dev = MpxModuleMgr::instance()->device(hw_id);

    if (!dev)
    {
        return MpxModuleMgr::instance()->lastErr();
    }

    return dev->getHwInfoFlags(index, flags);
}

// ----------------------------------------------------------------------------

int mpx2GetHwInfo(int hw_id, int index, HwInfoItem* hwInfo, int* sz)
{
    MpxModule* dev = MpxModuleMgr::instance()->device(hw_id);

    if (!dev)
    {
        return MpxModuleMgr::instance()->lastErr();
    }

    return dev->getHwInfo(index, hwInfo, sz);
}

// ----------------------------------------------------------------------------

int mpx2SetHwInfo(int hw_id, int index, void* data, int sz)
{
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->setHwInfo(index, data, sz);
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
    }
    return ret;
}

void sumDevInfoLists(const std::vector<DevInfo> &devInfoList, DevInfo* dev_info)
{
    int groupSize = MpxModuleMgr::instance()->getGroupSize();

    memcpy(dev_info, &devInfoList[0], sizeof(DevInfo));
    for (std::vector<DevInfo>::const_iterator cit = (devInfoList.begin() + 1); cit != devInfoList.end(); cit++)
    {
        dev_info->pixCount += cit->pixCount;
        dev_info->numberOfChips += cit->numberOfChips;
        //dev_info->numberOfRows += cit->numberOfRows;
        dev_info->rowLen += cit->rowLen;
    }
    // If groupsize is 4, then assume they are in a 2x2 configuration
    // Other groupsizes are in a line
    if (groupSize == 4)
    {
        dev_info->numberOfRows *= 2;
        dev_info->rowLen /= 2;
    }
}

// ----------------------------------------------------------------------------

int mpx2GetDevInfo(int hw_id, DevInfo* dev_info)
{
    DevInfo info;
    std::vector<DevInfo> devInfoList;
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->getDevInfo(&info);
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
        devInfoList.push_back(info);
    }
    sumDevInfoLists (devInfoList, dev_info);
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2Reset(int hw_id)
{
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->reset();
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2SetDACs(int hw_id,
                DACTYPE dac_vals[],
                int sz,
                byte sense_chip,
                byte ext_dac_chip,
                int codes[],
                u32 tp_reg)
{
    int ret = MPXERR_NOERROR;
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    int dacListSize = sz / groupSize;
    int chipNr = 0;
    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->setFsrs(dac_vals + (dacListSize * chipNr), dacListSize, sense_chip,
                           ext_dac_chip, codes, tp_reg);
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
        chipNr++;
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2GetMpxDacVal(int hw_id, int chip_nr, double* val)
{
    MpxModule* dev = MpxModuleMgr::instance()->device(hw_id);

    if (!dev)
    {
        return MpxModuleMgr::instance()->lastErr();
    }

    return dev->getMpxDac(chip_nr, val);
}

// ----------------------------------------------------------------------------

int mpx2SetExtDacVal(int hw_id, double val)
{
    UNREFERENCED_PARAMETER(val);
    MpxModule* dev = MpxModuleMgr::instance()->device(hw_id);

    if (!dev)
    {
        return MpxModuleMgr::instance()->lastErr();
    }

    return 0;
}

// ----------------------------------------------------------------------------

int mpx2SetPixelsCfg(int hw_id, byte cfgs[], u32 sz)
{
    //UNREFERENCED_PARAMETER(sz);
    int ret = MPXERR_NOERROR;
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    int detSize = sz / groupSize;
    byte* cfgp = cfgs;
    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->setPixelsCfg(cfgp);
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
        cfgp += detSize;
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2SetAcqPars(int hw_id, AcqParams* pars)
{
    int ret = MPXERR_NOERROR;
    bool masterSlaveMode = false;
    if (pars->mode == ACQMODE_SWTRIGMASTER_HWTRIGSLAVE)
    {
        masterSlaveMode = true;
        pars->mode = ACQMODE_ACQSTART_TIMERSTOP;
    }
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->setAcqPars(pars);
        if (masterSlaveMode == true)
        {
            pars->mode = ACQMODE_HWTRIGSTART_HWTRIGSTOP;
        }
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
    }

    return ret;
}

static int getResults(int hw_id, const int groupSize, int* rets)
{
    int chipNr = 0;
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        if (rets[chipNr] != MPXERR_NOERROR)
        {
            ret = rets[chipNr];
            break;
        }
        chipNr++;
    }
    return ret;
}


// ----------------------------------------------------------------------------
void mpx2StartAcquisitionHelper(MpxModule* dev, int *ret)
{
    *ret = dev->startAcquisition();
}

int mpx2StartAcquisition(int hw_id)
{
    int ret = MPXERR_NOERROR;
    int chipNr = 0;
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    int rets[ASI_MAX_GROUP_SIZE];
    // Start acq messages are a bit weird because in SWTRIGMASTER_HWTRIGSLAVE mode
    // the master (i.e. first relaxd board) must start its acqusition after all of the
    // slaves are ready. So you must do the start acq on all of the slaves 1st
    // then you can do the start acq on the master. 
    // The code below does all of the slaves in parallel, and then when they are all done
    // it does the start acq on the master.
    for (int hid = (hw_id + 1); hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        MpxModuleMgr::getPimpl()->_startAcqJobs->enqueueJob(boost::protect(boost::bind(&mpx2StartAcquisitionHelper, dev, &rets[chipNr])));
        chipNr++;
    }
    MpxModuleMgr::getPimpl()->_startAcqJobs->waitForJobs();
    // Now that the slaves are all ready to start an acq, the master is started.
    mpx2StartAcquisitionHelper(MpxModuleMgr::instance()->device(hw_id), &rets[chipNr]);
    ret = getResults(hw_id, groupSize, rets);
    return ret;
}

// ----------------------------------------------------------------------------

void mpx2StopAcqusitionHelper(MpxModule* dev, int *ret)
{
    *ret = dev->stopAcquisition();
}

int mpx2StopAcquisition(int hw_id)
{
    int ret = MPXERR_NOERROR;
    int chipNr = 0;
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    int rets[ASI_MAX_GROUP_SIZE];
    MpxModuleMgr::getPimpl()->_stopAcqJobs->waitForJobs();

    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }
        MpxModuleMgr::getPimpl()->_stopAcqJobs->enqueueJob(boost::protect(boost::bind(&mpx2StopAcqusitionHelper, dev, &rets[chipNr])));
        //mpx2StopAcqusitionHelper(dev, &rets[chipNr]);

        chipNr++;
    }
    MpxModuleMgr::getPimpl()->_stopAcqJobs->waitForJobs();
    ret = getResults(hw_id, groupSize, rets);
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2GetAcqTime(int hw_id, double* time)
{
    MpxModule* dev = MpxModuleMgr::instance()->device(hw_id);

    if (!dev)
    {
        return MpxModuleMgr::instance()->lastErr();
    }

    return dev->getAcqTime(time);
}

// ----------------------------------------------------------------------------

int mpx2ResetMatrix(int hw_id)
{
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->resetMatrix();
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
    }
    return ret;
}

// ----------------------------------------------------------------------------

static void mpx2ReadMatrixHelper(MpxModule* dev, i16* data, int frameSize, int *ret)
{
    *ret = dev->readMatrix(data, frameSize);
}

int mpx2ReadMatrix(int hw_id, i16* data, u32 sz)
{
    int ret = MPXERR_NOERROR;
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    int frameSize = sz / groupSize;
    int chipNr = 0;
    int rets[ASI_MAX_GROUP_SIZE];
    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        MpxModuleMgr::getPimpl()->_readFrameJobs->enqueueJob(boost::protect(boost::bind(&mpx2ReadMatrixHelper, dev, data + (frameSize * chipNr), frameSize, &rets[chipNr])));
        //mpx2ReadMatrixHelper(dev, data + (frameSize * chipNr), frameSize, &rets[chipNr]);
        chipNr++;
    }
    MpxModuleMgr::getPimpl()->_readFrameJobs->waitForJobs();
    ret = getResults(hw_id, groupSize, rets);
    return ret;
}

// ----------------------------------------------------------------------------

static void mpx2ReadMatrixRawHelper(MpxModule* dev, u8* data, unsigned int frameSize, int *ret)
{
    dev->readMatrixRaw(data, &frameSize, ret);
}

int mpx2ReadMatrixRaw(int hw_id, u8* data, u32 sz) {

    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    unsigned int frameSize = sz / groupSize;
	u8 *p = data;
    int rets[ASI_MAX_GROUP_SIZE];

    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        MpxModuleMgr::getPimpl()->_readFrameJobs->enqueueJob(boost::protect(boost::bind(&mpx2ReadMatrixRawHelper, dev, p, frameSize, &rets[hid-hw_id])));
		p += frameSize;
    }
    MpxModuleMgr::getPimpl()->_readFrameJobs->waitForJobs();

    int lost_rows = 0;
	for (int i = 0; i < groupSize; i++) lost_rows += rets[i];

    return lost_rows;
}

// ----------------------------------------------------------------------------

int mpx2WriteMatrix(int hw_id, i16* data, u32 sz)
{
    int ret = MPXERR_NOERROR;
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    int frameSize = sz / groupSize;
    int chipNr = 0;
    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->writeMatrix(data + (frameSize * chipNr), frameSize);
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
        chipNr++;
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2SendTestPulses(int hw_id, double pulse_height,
                       double period, u32 pulse_count)
{
    int ret = MPXERR_NOERROR;
    for (int hid = hw_id; hid < (hw_id + MpxModuleMgr::instance()->getGroupSize()); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hid);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }

        ret = dev->generateTestPulses(pulse_height, period, pulse_count);
        if (ret != MPXERR_NOERROR)
        {
            break;
        }
    }
    return ret;
}

// ----------------------------------------------------------------------------

int mpx2IsBusy(int hw_id, BOOL* busy)
{
    int ret = MPXERR_NOERROR;
    int chipNr = 0;
    int groupSize = MpxModuleMgr::instance()->getGroupSize();
    bool busys[ASI_MAX_GROUP_SIZE] = { FALSE };

    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        MpxModule* dev = MpxModuleMgr::instance()->device(hw_id);

        if (!dev)
        {
            return MpxModuleMgr::instance()->lastErr();
        }
        MpxModuleMgr::getPimpl()->_isBusyJobs->enqueueJob(boost::protect(boost::bind(&MpxModule::getBusy, dev, &busys[chipNr])));
        //dev->getBusy(&busys[chipNr]);

        chipNr++;
    }
    chipNr = 0;
    *busy = FALSE;
    MpxModuleMgr::getPimpl()->_isBusyJobs->waitForJobs();
    for (int hid = hw_id; hid < (hw_id + groupSize); hid++)
    {
        if (busys[chipNr])
        {
            *busy = TRUE;
        }
    }
    return ret;
}

// ----------------------------------------------------------------------------

const char* mpx2GetLastError()
{
    return MpxModuleMgr::instance()->lastErrStr().c_str();
}

// ----------------------------------------------------------------------------

const char* mpx2GetLastDevError(int hw_id)
{
    MpxModule* dev = MpxModuleMgr::instance()->device(hw_id);

    if (!dev)
    {
        return MpxModuleMgr::instance()->lastErrStr().c_str();
    }

    return dev->lastErrStr().c_str();
}

// ----------------------------------------------------------------------------
// MPX3 Interface

Mpx3Interface mpx3Interface =
{
};

// ----------------------------------------------------------------------------
