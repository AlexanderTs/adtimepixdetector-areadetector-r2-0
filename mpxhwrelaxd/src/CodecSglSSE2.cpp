/*
*  CodecPar.cpp
*
*  Created by Chris Desjardins on 25/01/13.
*  Copyright 2013 amscins.com All rights reserved.
*
*/
#include "CodecSglSSE2.h"
#ifdef RLX_WITH_SSE2
#include <emmintrin.h>
#include "luts.h"

void AsiCodecSglSSE2::decodeRows(const int startRow, const int endRow, const AsiFrame<AsiEncodeWord_t> *inFrame, AsiFrame<AsiDecodeWord_t> *outFrame) const
{
    int col, row, i, bits;
    __m128i op;
    __m128i posmask = _mm_set_epi16(
        0x01, 0x02, 0x04, 0x08,
        0x10, 0x20, 0x40, 0x80);
    __m128i bitmask;
    __m128i decoded;
    const int simd_group = inFrame->getColCnt() / RLX_SIMD_SIZE;
    
    const AsiDecodeWord_t *lut = getLookupTable();
    AsiEncodeWord_t *inPtr = inFrame->getFramePtr(startRow);
    AsiDecodeWord_t *outPtr = outFrame->getFramePtr(startRow);

    for (row = startRow; row < endRow; row++)
    {
        for (col = 0; col < simd_group; col++)
        {
            bitmask = _mm_set1_epi16(1 << (inFrame->getBitsPerPixel() - 1));
            decoded = _mm_setzero_si128();
            for (bits = 0; bits < inFrame->getBitsPerPixel(); bits++)
            {
                op =        _mm_set1_epi16(inPtr[col + bits * simd_group]);
                op =        _mm_and_si128(op, posmask);             
                op =        _mm_cmpeq_epi16(op, posmask);
                op =        _mm_and_si128(op, bitmask);
                decoded =   _mm_or_si128(op, decoded);
                bitmask =   _mm_srli_epi16(bitmask, 1);
            }
            _mm_storeu_si128((__m128i*)outPtr, decoded);
            for (i = 0; i < RLX_SIMD_SIZE; ++i, ++outPtr)
            {
                *outPtr = lut[*outPtr];
            }
        }
        inPtr += inFrame->getBitsPerPixel() * simd_group;
    }
}
#endif
