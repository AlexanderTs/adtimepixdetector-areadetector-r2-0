/*
 *  CodecMgr.cpp
 *
 *  Created by Chris Desjardins on 25/01/13.
 *  Copyright 2013 amscins.com All rights reserved.
 *
 */
#ifdef _WIN32
#include <intrin.h>
#endif
#include "bt.h"
#include "CodecMgr.h"

#define FLAG_SSE2        0x04000000

AsiCodecMgr::AsiCodecMgr()
{
    initCodec();
}

AsiCodecMgr::~AsiCodecMgr()
{
}

void AsiCodecMgr::initCodec()
{
    _numThreads = 2;
    _supportsSSE2 = false;
    _usingSSE2 = false;
    detectSSE2();
    setUseSSE2(_supportsSSE2);
}

void AsiCodecMgr::detectSSE2()
{
#ifdef RLX_WITH_SSE2
#ifdef _WIN32
    int info[4];
    __cpuid(info, 0);

    if (info[0] >= 1)
    {
        __cpuid(info, 1);
        if (info[3] & FLAG_SSE2)
        {
            _supportsSSE2 = true;
        }
    }
#else
#define cpuid(func, ax, bx, cx, dx) \
    __asm__ __volatile__ ("cpuid" : \
                          "=a" (ax), "=b" (bx), "=c" (cx), "=d" (dx) : "a" (func));

    int eax, ebx, ecx, edx;
    cpuid(0, eax, ebx, ecx, edx);
    if (eax >= 1)
    {
        cpuid(1, eax, ebx, ecx, edx);
        if (edx & FLAG_SSE2)
        {
            _supportsSSE2 = true;
        }
    }
#endif
#endif
}

static void waitForThreads(std::vector<boost::thread*> threads)
{
    std::vector<boost::thread*>::iterator it;
    for (it = threads.begin(); it != threads.end(); it++)
    {
        boost::thread *t;
        t = (*it);
        t->join();
        delete (t);
    }
}

void AsiCodecMgr::spawnDecodeThreads(AsiCodecIntf *codec, AsiFrame<AsiEncodeWord_t>* encodeFrames, AsiFrame<AsiDecodeWord_t>* decodeFrames)
{
    if (_numThreads == 0)
    {
        codec->decodeRows(0, encodeFrames->getRowCnt(), encodeFrames, decodeFrames);
    }
    else
    {
        int startRow;
        int endRow;
        std::vector<boost::thread*> threads;
        int numRowsPerThread = encodeFrames->getRowCnt() / _numThreads;
        for (int i = 0; i < _numThreads; i++)
        {
            startRow = i * numRowsPerThread;
            endRow = (i + 1) * numRowsPerThread;
            threads.push_back(new boost::thread (&AsiCodecIntf::decodeRows, codec, startRow, endRow, encodeFrames, decodeFrames));
        }
        waitForThreads(threads);
    }
}

