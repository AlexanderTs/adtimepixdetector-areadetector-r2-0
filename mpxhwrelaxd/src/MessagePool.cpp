/*
*  MessagePool.cpp
*
*  Created by Chris Desjardins on 30/01/13.
*  Copyright 2013 amscins.com All rights reserved.
*
*/

/*
** The message pool should always be used to get AsiMessages
** It handles allocation/deallocation of messages, and should
** have better runtime performance than just allocating new
** messages on demand.
*/

#include "MessagePool.h"
#include "MessageQueue.h"

AsiMessagePool::AsiMessagePool(const int initialPoolSize)
{
    _initialPoolSize = initialPoolSize;
    _totalPoolSize = 0;
    _freeMessages = new AsiMessageQueue();
    allocateMessages(initialPoolSize);
}

AsiMessagePool::~AsiMessagePool()
{
    if (_freeMessages != NULL)
    {
        deleteMessages();
    }
}

void AsiMessagePool::allocateMessages(const int numMessages)
{
    for (int i = 0; i < numMessages; i++)
    {
        mpixd_reply_msg *msg = new mpixd_reply_msg();
        _freeMessages->enqueueMessage(msg);
        _totalPoolSize++;
    }
}

void AsiMessagePool::deleteMessages()
{
    bool msgs;
    mpixd_reply_msg *msg;
    do
    {
        msgs = _freeMessages->dequeueMessage(&msg);
        if (msgs == true)
        {
            delete msg;
        }
    } while (msgs == true);
    delete _freeMessages;
}

const bool AsiMessagePool::getMessage(mpixd_reply_msg** msg)
{
    bool ret;
    ret = _freeMessages->dequeueMessage(msg);
    if (ret == false)
    {
        allocateMessages(std::max(_initialPoolSize / 10, 10));
        ret = _freeMessages->dequeueMessage(msg);
    }
    if (ret == true)
    {
        (*msg)->header.length = 0;
        (*msg)->header.type = 0xfeedface;
    }
    return ret;
}

void AsiMessagePool::releaseMessage(mpixd_reply_msg *msg)
{
    _freeMessages->enqueueMessage(msg);
}
