#include "mpxcodecmgr.h"
#include "CodecSgl.h"
#include "CodecPar.h"
#include "CodecSglSSE2.h"
#include "CodecParSSE2.h"

MpxCodecMgr::MpxCodecMgr()
{
    _codecPar = NULL;
    _codecSgl = NULL;
    _mpxDecodeFrames = new MpxFrame<AsiDecodeWord_t>[MPIX_MAX_DEVS];
    _mpxEncodeFrames = new MpxFrame<AsiEncodeWord_t>[MPIX_MAX_DEVS];
    initCodec();
    allocateCodec();
}

MpxCodecMgr::~MpxCodecMgr()
{
    deleteCodec();
    delete []_mpxDecodeFrames;
    delete []_mpxEncodeFrames;
}

void MpxCodecMgr::deleteCodec()
{
    if (_codecSgl != NULL)
    {
        delete _codecSgl;
    }
    if (_codecPar != NULL)
    {
        delete _codecPar;
    }
}

void MpxCodecMgr::allocateCodec()
{
    deleteCodec();
#ifdef RLX_WITH_SSE2
    if (this->useSSE2() == true)
    {
        _codecSgl = new AsiCodecSglSSE2();
        _codecPar = new AsiCodecParSSE2();
    }
    else
#endif
    {
        _codecSgl = new AsiCodecSgl();
        _codecPar = new AsiCodecPar();
    }
}



void MpxCodecMgr::decodePar(u8 *bytes, i16 *pixels)
{
    _mpxEncodeFrames[0].setFramePtr(bytes + (_mpxEncodeFrames[0].getFrameElements() * 0));
    _mpxEncodeFrames[1].setFramePtr(bytes + (_mpxEncodeFrames[1].getFrameElements() * 1));
    _mpxEncodeFrames[2].setFramePtr(bytes + (_mpxEncodeFrames[2].getFrameElements() * 2));
    _mpxEncodeFrames[3].setFramePtr(bytes + (_mpxEncodeFrames[3].getFrameElements() * 3));
    _mpxDecodeFrames[0].setFramePtr((AsiDecodeWord_t*)(pixels + (_mpxDecodeFrames[0].getFrameElements() * 0)));
    _mpxDecodeFrames[1].setFramePtr((AsiDecodeWord_t*)(pixels + (_mpxDecodeFrames[1].getFrameElements() * 1)));
    _mpxDecodeFrames[2].setFramePtr((AsiDecodeWord_t*)(pixels + (_mpxDecodeFrames[2].getFrameElements() * 2)));
    _mpxDecodeFrames[3].setFramePtr((AsiDecodeWord_t*)(pixels + (_mpxDecodeFrames[3].getFrameElements() * 3)));

    _codecPar->decodeSetup(_mpxDecodeFrames);
    spawnDecodeThreads(_codecPar, _mpxEncodeFrames, _mpxDecodeFrames);
    _codecPar->decodeCleanup(_mpxDecodeFrames);
}

void MpxCodecMgr::decodeSgl(u8 *stream, i16 *data)
{
    _mpxEncodeFrames->setFramePtr(stream);
    _mpxDecodeFrames->setFramePtr((AsiDecodeWord_t*)data);

    _codecSgl->decodeSetup(_mpxDecodeFrames);
    spawnDecodeThreads(_codecSgl, _mpxEncodeFrames, _mpxDecodeFrames);
    _codecSgl->decodeCleanup(_mpxDecodeFrames);
}

void MpxCodecMgr::encodeSgl(i16 *mask, u8 *stream)
{
    _mpxEncodeFrames->setFramePtr(stream);
    _mpxDecodeFrames->setFramePtr((AsiDecodeWord_t*)mask);
    _codecSgl->encodeRows(_mpxDecodeFrames, _mpxEncodeFrames);
}

void MpxCodecMgr::setUseSSE2(const bool useSSE2)
{
    AsiCodecMgr::setUseSSE2(useSSE2);
    allocateCodec();
}
