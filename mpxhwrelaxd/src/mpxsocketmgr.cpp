#include "mpxsocketmgr.h"
#include <sstream>
#include <stdarg.h>
#include <string.h>
#include "mpxplatform.h"
#include "MessageQueue.h"
#include "MessagePool.h"
#include "bt.h"

#if defined(_WINDLL) || defined(WIN32)
#include <winsock.h>
#define SOCKET_RETRY    WSAEWOULDBLOCK
#else
#define SOCKET_RETRY    EAGAIN
#endif

/*
** Using connect + send/recv vs no connect + sendto/recvfrom
** is a bit faster. However it breaks linux external triggers
** because the signal does not work with sockets that have
** connections.
*/

#define USE_CONNECT

struct MpxSocketMgrPimpl
{
    MpxSocketMgrPimpl()
        : _rxThread(NULL),
        _waitTime(boost::chrono::milliseconds(1))
    {
    }
    boost::thread *_rxThread;
    std::vector<AsiMessageQueue *> _rxMsgQueueList;
    std::map<int, int> _msgIdToIndex;
    boost::chrono::system_clock::duration _waitTime;
};

MpxSocketMgr::MpxSocketMgr(MpxLogger *log)
{
#if defined(_WINDLL) || defined(WIN32)
    // Start up Winsock
    WSADATA wsadata;
    WSAStartup( MAKEWORD(2,2), &wsadata );

    // Did we get the right Winsock version?
    if ( wsadata.wVersion != MAKEWORD(2,2) ) 
    {
        WSACleanup(); // Clean up Winsock
    }
#endif
    
    _log = log;
    _sock = 0;

    _txPacketCnt = 0;
    _rxPacketCnt = 0;
    _txByteCnt = 0;
    _rxByteCnt = 0;
    _isNonBlocking = false;
    _threadRun = true;
    _socketMgrData = new MpxSocketMgrPimpl();
    _timeoutMs = 1000;
    /*
    ** Hate to do this, but have to do something... 
    ** Be sure to update this if any new messages are added.
    */
    _socketMgrData->_msgIdToIndex[MPIXD_ERROR            ] = 0;
    _socketMgrData->_msgIdToIndex[MPIXD_ACK              ] = 1;
    _socketMgrData->_msgIdToIndex[MPIXD_TRIGGER          ] = 2;
    _socketMgrData->_msgIdToIndex[MPIXD_GET_REG          ] = 3;
    _socketMgrData->_msgIdToIndex[MPIXD_SET_REG          ] = 4;
    _socketMgrData->_msgIdToIndex[MPIXD_SET_DACS         ] = 5;
    _socketMgrData->_msgIdToIndex[MPIXD_SET_MATRIX       ] = 6;
    _socketMgrData->_msgIdToIndex[MPIXD_READOUT          ] = 7;
    _socketMgrData->_msgIdToIndex[MPIXD_SET_CONFIG       ] = 8;
    _socketMgrData->_msgIdToIndex[MPIXD_GET_ADC          ] = 9;
    _socketMgrData->_msgIdToIndex[MPIXD_PING             ] = 10;
    _socketMgrData->_msgIdToIndex[MPIXD_SET_TESTPULSE    ] = 11;
    _socketMgrData->_msgIdToIndex[MPIXD_GET_TEMPERATURE  ] = 12;
    _socketMgrData->_msgIdToIndex[MPIXD_SET_DACONVERTER  ] = 13;
    _socketMgrData->_msgIdToIndex[MPIXD_GET_CHIPID       ] = 14;
    _socketMgrData->_msgIdToIndex[MPIXD_GET_DACS         ] = 15;
    _socketMgrData->_msgIdToIndex[MPIXD_STORE_ID         ] = 16;
    _socketMgrData->_msgIdToIndex[MPIXD_STORE_DACS       ] = 17;
    _socketMgrData->_msgIdToIndex[MPIXD_ERASE_DACS       ] = 18;
    _socketMgrData->_msgIdToIndex[MPIXD_STORE_CONFIG     ] = 19;
    _socketMgrData->_msgIdToIndex[MPIXD_ERASE_CONFIG     ] = 20;
    _socketMgrData->_msgIdToIndex[MPIXD_GET_SWVERSION    ] = 21;
    _socketMgrData->_msgIdToIndex[MPIXD_LOAD_CONFIG      ] = 22;
    _socketMgrData->_msgIdToIndex[MPIXD_WRITE_FIRMWARE   ] = 23;
    _socketMgrData->_msgIdToIndex[MPIXD_READ_FIRMWARE    ] = 24;

    _freeMsgPool = new AsiMessagePool(2048);
    for (size_t i = 0; i < _socketMgrData->_msgIdToIndex.size(); i++)
    {
        _socketMgrData->_rxMsgQueueList.push_back(new AsiMessageQueue());
    }
}

MpxSocketMgr::~MpxSocketMgr()
{
    std::vector<AsiMessageQueue *>::iterator it;
    if (_socketMgrData != NULL)
    {
        if (_socketMgrData->_rxThread != NULL)
        {
            _threadRun = false;
            _socketMgrData->_rxThread->join();
            delete _socketMgrData->_rxThread;
        }
        for (it = _socketMgrData->_rxMsgQueueList.begin(); it != _socketMgrData->_rxMsgQueueList.end(); it++)
        {
            AsiMessageQueue *rxMsgQueue = *it;
            if (rxMsgQueue != NULL)
            {
                delete rxMsgQueue;
            }
        }
        _socketMgrData->_rxMsgQueueList.clear();

        delete _socketMgrData;
        _socketMgrData = NULL;
    }

    if (_freeMsgPool != NULL)
    {
        delete _freeMsgPool;
    }
    closeSocket();
#if defined(_WINDLL) || defined(WIN32)
    WSACleanup(); // Clean up Winsock
#endif
}

int MpxSocketMgr::initSocket()
{
    int ret = 0;
    _sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
    if ( _sock == INVALID_SOCKET )
    {
        std::ostringstream oss;
        oss  << "### _sock socket creation failed: "<< getErrorString();
        _log->error( oss.str() );
        ret = -1;
    }
    return ret;
}

int MpxSocketMgr::ioctlSock(long cmd, u_long *argp)
{
    int ret = 0;
#if defined(_WINDLL) || defined(WIN32) 

    ret = ioctlsocket( _sock, cmd, argp );
    if (ret != 0)
    {
        std::ostringstream oss;
        oss  << "### _sock ioctlsocket("<<cmd<<") failed: "<< getErrorString();
        _log->error( oss.str() );
    }
#endif
    return ret;
}

int MpxSocketMgr::fcntlSock(long cmd)
{
    // return call depends on cmd, and is never checked in mpx, so this is fine for now...
    // higher level code can issue logs on error if necessary
    int ret = 0;
#if defined(_WINDLL) || defined(WIN32) 
    UNREFERENCED_PARAMETER(cmd);
#else
    ret = fcntl( _sock, cmd);
#endif
    return ret;
}

int MpxSocketMgr::fcntlSock(long cmd, int args)
{
    // return call depends on cmd, and is never checked in mpx, so this is fine for now...
    // higher level code can issue logs on error if necessary
    int ret = 0;
#if defined(_WINDLL) || defined(WIN32)
    UNREFERENCED_PARAMETER(cmd);
    UNREFERENCED_PARAMETER(args);
#else
    ret = fcntl( _sock, cmd, args);
#endif
    return ret;
}

int MpxSocketMgr::setSockOptions(int optname, const char *optval, int optlen)
{
    int ret = setsockopt(_sock, SOL_SOCKET, optname, optval, optlen);
    if (ret != 0)
    {
        std::ostringstream oss;
        oss  << "### _sock setsockopt("<<optname<<") failed: "<< getErrorString();
        _log->error( oss.str() );
    }
    return ret;
}

int MpxSocketMgr::getSockOptions(int optname, char *optval, int *optlen)
{
    int ret = getsockopt(_sock, SOL_SOCKET, optname, optval, (socklen_t*)optlen);
    if (ret != 0)
    {
        std::ostringstream oss;
        oss  << "### _sock getsockopt("<<optname<<") failed: "<< getErrorString();
        _log->error( oss.str() );
    }
    return ret;
}

int MpxSocketMgr::connectToSocket(u32 ipAddress, u32 portNumber)
{
    int ret = 0;
    struct sockaddr_in saddr;

    memset( static_cast<void *> ( &saddr ), 0, sizeof( struct sockaddr_in ) );

    saddr.sin_family      = AF_INET;
    saddr.sin_port        = htons( (u_short) portNumber );
    saddr.sin_addr.s_addr = htonl( ipAddress ); // My address!
    _sendToaddr = saddr;

#ifdef USE_CONNECT
    ret = connect( _sock, ( struct sockaddr * ) &saddr,
                  sizeof( struct sockaddr_in ) );
    if ( ret != 0 )
    {
        std::ostringstream oss;
        oss  << "### _sock socket connect failed: "<<getErrorString();
        _log->error( oss.str() );
    }
#endif
    setNonBlocking(true);
    _socketMgrData->_rxThread = new boost::thread(&MpxSocketMgr::socketRxThread, this);

    return ret;
}

int MpxSocketMgr::bindSocketFunc(u32 ipAddress, u32 portNumber, const char *dbg)
{
    int ret;
    struct sockaddr_in saddr;

    UNREFERENCED_PARAMETER(ipAddress);

    memset( static_cast<void *> ( &saddr ), 0, sizeof( struct sockaddr_in ) );

    saddr.sin_family      = AF_INET;
    saddr.sin_port        = htons( (u_short) portNumber );
    saddr.sin_addr.s_addr = htonl( INADDR_ANY);
    ret = bind( _sock, ( struct sockaddr * ) &saddr,
               sizeof( struct sockaddr_in ) ); 
    if ( ret != 0 )
    {
        std::ostringstream oss;
        oss  << "### _sock socket bind failed ("<<dbg<<"): "<<inet_ntoa(saddr.sin_addr)<<":"<<portNumber<<" "<< getErrorString();
        _log->error( oss.str() );
        //juhe - 05/04/2012
        //ret = MPXERR_UNEXPECTED; // bind failed
        // this seems never actually work?? 
    }
    return ret;
}

void MpxSocketMgr::closeSocket()
{
    if (_sock)
    {
#if defined(_WINDLL) || defined(WIN32)
        closesocket( _sock );
#else
        close( _sock );
#endif
    }
}

void MpxSocketMgr::socketRxThread()
{
    int bytes_recvd = 0;
    mpixd_reply_msg *msg = NULL;
    boost::chrono::system_clock::duration waitTime = boost::chrono::milliseconds(1);
    int usCnt = 0;

    while (_threadRun == true)
    {
        if (msg == NULL)
        {
            _freeMsgPool->getMessage(&msg);
        }
        bytes_recvd = recv(_sock, (char*)msg, sizeof(mpixd_reply_msg),  0);
        if (bytes_recvd > 0)
        {
            u32 msgId;
            msgId = ntohl(msg->header.type);
            _socketMgrData->_rxMsgQueueList[_socketMgrData->_msgIdToIndex[msgId]]->enqueueMessage(msg);
            msg = NULL;
            waitTime = _socketMgrData->_waitTime;
            usCnt = 0;
        }
        else
        {
            boost::this_thread::sleep_for(waitTime);
            /*
            ** If 1000 sleeps have elapsed since our last message then
            ** start waiting for 1 millisecond to reduce load on the cpu
            ** when the _waitTime is 1 microsecond.
            */
            if (usCnt++ > 1000)
            {
                waitTime = boost::chrono::milliseconds(1);
            }
        }
    }
    if (msg != NULL)
    {
        _freeMsgPool->releaseMessage(msg);
    }
}

void MpxSocketMgr::releaseRxMsg(mpixd_reply_msg **buf)
{
    _freeMsgPool->releaseMessage(*buf);
    *buf = NULL;
}

void MpxSocketMgr::clearRxQueue(mpixd_type msgId)
{
    int cnt = 0;
    mpixd_reply_msg *msg = NULL;
    while (_socketMgrData->_rxMsgQueueList[_socketMgrData->_msgIdToIndex[msgId]]->dequeueMessage(&msg)) 
    {
        releaseRxMsg(&msg);
        cnt++;
    }
    if (cnt > 0)
    {
        _log->scratch<<"clearRxQueue: cleared "<<std::dec<<cnt<<" msgs";
        _log->logScratch(MPX_DEBUG);
    }
}

bool MpxSocketMgr::recvData(mpixd_reply_msg **buf, const std::vector<mpixd_type> &msgs)
{
    std::vector<mpixd_type>::const_iterator it;
    bool ret = false;
    mpixd_type type;
    if (msgAvailable(msgs, &type) == true)
    {
        ret = recvData(buf, type);
    }
    return ret;
}

bool MpxSocketMgr::recvData(mpixd_reply_msg **buf, const mpixd_type msgId)
{
    return recvData(buf, msgId, _timeoutMs);
}

bool MpxSocketMgr::recvData(mpixd_reply_msg **buf, const mpixd_type msgId, const int timeoutMs)
{
    return _socketMgrData->_rxMsgQueueList[_socketMgrData->_msgIdToIndex[msgId]]->waitForMessage(buf, timeoutMs);
}

int MpxSocketMgr::sendDataFunc(const char *buf, int len, const char *dbg)
{
#ifdef USE_CONNECT
    int ret = send( _sock, buf, len, 0);
#else
    int ret = sendto( _sock, buf, len, 0, (sockaddr*)&_sendToaddr, sizeof(sockaddr_in));
#endif

    if ( ret == -1 )
    {
        //setSockBlocking(true);
        std::ostringstream oss;
        oss  << "### sendDataFunc("<<dbg<<") send: " << getErrorString();
        _log->error( oss.str() );
    }
    else
    {
        _txPacketCnt++;
        _txByteCnt += len;
    }
    return ret;
}

void MpxSocketMgr::setSockTimeout( bool forever, int ms )
{
    if (forever)
    {
        _timeoutMs = 0;
    }
    else
    {
        _timeoutMs = ms;
    }
}

bool MpxSocketMgr::msgAvailable(mpixd_type msgId)
{
    bool ret = false;
    if (_socketMgrData->_rxMsgQueueList[_socketMgrData->_msgIdToIndex[msgId]]->numEnqueued() > 0)
    {
        ret = true;
    }
    return ret;
}

bool MpxSocketMgr::msgAvailable(const std::vector<mpixd_type> &msgs, mpixd_type *msgId)
{
    bool ret = false;
    boost::chrono::system_clock::time_point time_limit = boost::chrono::system_clock::now() + boost::chrono::milliseconds(_timeoutMs);
    std::vector<mpixd_type>::const_iterator it;
    do
    {
        for (it = msgs.begin(); it != msgs.end(); it++)
        {
            ret = msgAvailable(*it);
            if (ret == true)
            {
                *msgId = *it;
                break;
            }
        }
        if (ret == false)
        {
            boost::this_thread::sleep_for(boost::chrono::microseconds(1));
        }
    } while ((ret == false) && (boost::chrono::system_clock::now() < time_limit));

    return ret;
}

int MpxSocketMgr::getErrno()
{
    int errCode;
#if defined(_WINDLL) || defined(WIN32)
    errCode = WSAGetLastError();
#else
    errCode = errno;
#endif
    return errCode;
}

std::string MpxSocketMgr::getErrorString()
{
    std::string ret = "";
    int errCode = getErrno();
#if defined(_WINDLL) || defined(WIN32)
    LPSTR errString = NULL;
    FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        0, errCode, 0, (LPSTR)&errString, 0, 0 );
    ret = errString;
    LocalFree( errString ) ;
#else // Linux
    std::ostringstream oss;
    oss<<strerror(errCode)<<" errno="<< errCode;
    ret = oss.str();
#endif
    return ret;
}

void MpxSocketMgr::setNonBlocking(bool nonBlocking)
{
#if defined(_WINDLL) || defined(WIN32)
    u_long arg = (nonBlocking == true) ? 1 : 0;
    ioctlSock(FIONBIO, &arg);
#else
    int flags = fcntlSock(F_GETFL, 0);
    if (nonBlocking == true)
    {
        flags |= O_NONBLOCK;
    }
    else
    {
        flags &= ~O_NONBLOCK;
    }
    fcntlSock(F_SETFL, flags);
#endif
    _isNonBlocking = nonBlocking;
}

void MpxSocketMgrRt::socketThreadPriority()
{
#if defined(_WINDLL) || defined(WIN32)
#else
#ifndef __APPLE__
    sched_param param;
    int priority = sched_get_priority_max(SCHED_FIFO) - 5;
    param.sched_priority = priority;
    if (sched_setscheduler(0, SCHED_FIFO, &param) == -1)
    {
        std::ostringstream oss;
        oss  << "Unable to set rx thread priority (might not have preempt_rt, ";
        oss  << "or might not have permission to invoke sched_setscheduler)";
        _log->error(oss.str());
    }
    else
    {
        std::ostringstream oss;
        oss  << "Set rx thread priority to " << priority;
        _log->info(oss.str());
    }
#endif
#endif
}

void MpxSocketMgrRt::socketRxThread()
{
    socketThreadPriority();
    _socketMgrData->_waitTime = boost::chrono::microseconds(1);
    MpxSocketMgr::socketRxThread();
}

