/*
*  CodecPar.cpp
*
*  Created by Chris Desjardins on 25/01/13.
*  Copyright 2013 amscins.com All rights reserved.
*
*/
#include "CodecParSSE2.h"
#ifdef RLX_WITH_SSE2
#include <emmintrin.h>
#include "luts.h"

#ifdef _WIN32
#define ALIGN_SSE(x)     __declspec(align(16)) x
#else
#define ALIGN_SSE(x)     x __attribute__((aligned (16)))
#endif

void AsiCodecParSSE2::decodeRows(const int startRow, const int endRow, const AsiFrame<AsiEncodeWord_t> *inFrame, AsiFrame<AsiDecodeWord_t> *outFrame) const
{
    for (int row = startRow; row < endRow; row ++)
    {
        decodeRows(row, inFrame, outFrame);
    }
}

void AsiCodecParSSE2::decodeRows(const int startRow, const AsiFrame<AsiEncodeWord_t> *inFrame, AsiFrame<AsiDecodeWord_t> *outFrames) const
{
    AsiDecodeWord_t *pdev0, *pdev1, *pdev2, *pdev3;
    int col, bits, offset;
    __m128i op;
    __m128i posmask = _mm_set_epi16(
        0x01, 0x02, 0x04, 0x08,
        0x10, 0x20, 0x40, 0x80);
   __m128i bitmask;
    __m128i decoded;
    const AsiDecodeWord_t *lut = getLookupTable();
    const int simd_group = (inFrame->getColCnt() / RLX_SIMD_SIZE) * MPIX_MAX_DEVS;

    ALIGN_SSE(AsiDecodeWord_t outreg[8]);
    AsiEncodeWord_t *inPtr = AsiFrame<AsiEncodeWord_t>::getFrameListRowPtr(inFrame, startRow * MPIX_MAX_DEVS);

    pdev3 = outFrames[3].getFramePtr();
    pdev2 = outFrames[2].getFramePtr();
    pdev1 = outFrames[1].getFramePtr();
    pdev0 = outFrames[0].getFramePtr();

    offset = outFrames[0].getRowElements() * startRow;

    for (col = 0; col < simd_group; col++)
    {
        bitmask = _mm_set1_epi16(1 << (inFrame->getBitsPerPixel() - 1));
        decoded = _mm_setzero_si128();
        for (bits = 0; bits < inFrame->getBitsPerPixel(); bits++)
        {
            op =        _mm_set1_epi16(inPtr[col + bits * simd_group]);
            op =        _mm_and_si128(op, posmask);             
            op =        _mm_cmpeq_epi16(op, posmask);
            op =        _mm_and_si128(op, bitmask);
            decoded =   _mm_or_si128(op, decoded);
            bitmask =   _mm_srli_epi16(bitmask, 1);
        }

        _mm_storeu_si128((__m128i*)&outreg, decoded);
        if ((col & 1) == 0)
        {
            pdev2[offset + 0] = lut[outreg[4]];
            pdev2[offset + 1] = lut[outreg[5]];
            pdev2[offset + 2] = lut[outreg[6]];
            pdev2[offset + 3] = lut[outreg[7]];
            pdev3[offset + 0] = lut[outreg[0]];
            pdev3[offset + 1] = lut[outreg[1]];
            pdev3[offset + 2] = lut[outreg[2]];
            pdev3[offset + 3] = lut[outreg[3]];
        }
        else
        {
            pdev0[offset + 0] = lut[outreg[4]];
            pdev0[offset + 1] = lut[outreg[5]];
            pdev0[offset + 2] = lut[outreg[6]];
            pdev0[offset + 3] = lut[outreg[7]];
            pdev1[offset + 0] = lut[outreg[0]];
            pdev1[offset + 1] = lut[outreg[1]];
            pdev1[offset + 2] = lut[outreg[2]];
            pdev1[offset + 3] = lut[outreg[3]];

            offset += 4;
        }
    }
}
#endif
