#include "mpxlogger.h"

void MpxLogger::logScratch(MpxLogLevel level) {
    std::string s = scratch.str();
	this->log(level, trim(s));
	scratch.str(""); 
};
