/*
*  MessageQueue.cpp
*
*  Created by Chris Desjardins on 31/01/13.
*  Copyright 2013 amscins.com All rights reserved.
*
*/

/*
** This provides a queue for messages, it is used by the MessagePool
** and the ConnectionIntf. It is just a simple queue for messages,
** and only provides the ability to enqueue/dequeue messages as well
** as wait (block) for new messages to be enqueued.
*/

#include "MessageQueue.h"
#include <boost/chrono/chrono.hpp>

using namespace boost;
using namespace boost::chrono;

AsiMessageQueue::AsiMessageQueue():
    _queue(),
    _queueMutex(),
    _msgNotification()
{
}

AsiMessageQueue::~AsiMessageQueue()
{
}

void AsiMessageQueue::enqueueMessage(mpixd_reply_msg *msg)
{
    { // create a new scope for the mutex 
        mutex::scoped_lock lock(_queueMutex);
        pushMessage(msg);
        _msgNotification.notify_all();
    }
}

bool AsiMessageQueue::dequeueMessage(mpixd_reply_msg** msg)
{
    bool ret = false;
    { // create a new scope for the mutex 
        mutex::scoped_lock lock(_queueMutex);
        ret = popMessage(msg);
    }
    return ret;
}

const bool AsiMessageQueue::waitForMessage(mpixd_reply_msg** msg, const int msTimeout)
{
    bool ret = false;
    { // create a new scope for the mutex 
        mutex::scoped_lock lock(_queueMutex);
        system_clock::time_point time_limit = system_clock::now() + milliseconds(msTimeout);
        while (_queue.empty() == true)
        {
            // if timeout is specified, then wait until the time is up 
            // otherwise wait forever
            if (msTimeout > 0)
            {
                _msgNotification.wait_until(_queueMutex, time_limit);
                if (system_clock::now() >= time_limit)
                {
                    break;
                }
            }
            else
            {
                _msgNotification.wait(_queueMutex);
            }
        }
        ret = popMessage(msg);
    }
    return ret;
}

void AsiMessageQueue::pushMessage(mpixd_reply_msg* msg)
{
    // This function assumes that _queueMutex is locked already! 
    _queue.push(msg);
}

bool AsiMessageQueue::popMessage(mpixd_reply_msg** msg)
{
    // This function assumes that _queueMutex is locked already! 
    bool ret = false;
    if (_queue.empty() == false)
    {
        *msg = _queue.front();
        _queue.pop();
        ret = true;
    }
    return ret;
}

const int AsiMessageQueue::numEnqueued()
{
    return _queue.size();
}
