/*
       Copyright 2011 NIKHEF
*/

#include <sstream>
#include <iomanip>
#include <time.h>

#include "mpxmodule.h"
#include "mpxerrors.h"
#include "mpxplatform.h"
#include "mpxdaq.h"
#include "luts.h"

//#define DEBUG_LOG

// #define MPX_AUTO_ARM

#define	REQUIRED_FW_VERSION		170
#ifdef DEBUG_LOG
#define LOGCALL(a)          if(_logVerbose){_log->trace(a);}
#define LOGCALL1(a,b)       if(_logVerbose){ std::stringstream __logSs; __logSs << a << "," << b; _log->trace(__logSs.str()); }
#define LOGCALL2(a,b,c)     if(_logVerbose){ std::stringstream __logSs; __logSs << a << "," << b << "," << c; _log->trace(__logSs.str());}
#define LOGCALL3(a,b,c,d)   if(_logVerbose){ std::stringstream __logSs; __logSs << a << "," << b << "," << c << "," << d; _log->trace(__logSs.str());}
#define LOGCALL4(a,b,c,d,e) if(_logVerbose){ std::stringstream __logSs; __logSs << a << "," << b << "," << c << "," << d << "," << e; _log->trace(__logSs.str());}
//#define LOGCALL(a)        _fLog<<a<<std::endl
//#define LOGCALL1(a,b)     _fLog<<a<<", "<<b<<std::endl
//#define LOGCALL2(a,b,c)   _fLog<<a<<", "<<b<<", "<<c<<std::endl
//#define LOGCALL3(a,b,c,d) _fLog<<a<<", "<<b<<", "<<c<<", "<<d<<std::endl
#else
#define LOGCALL(a)
#define LOGCALL1(a,b)
#define LOGCALL2(a,b,c)
#define LOGCALL3(a,b,c,d)
#define LOGCALL4(a,b,c,d, e)
#endif

static char        ImgFileNameChar[MPX_MAX_PATH] = "NOT USED";
static const char *TYPENAME[] = { "???", "ORG", "MXR", "TPX", "MX3" };
const  int         MAX_TYPES  = 5;

// Library version string
const std::string MpxModule::_versionStr =
    std::string( "MpxModule class v1.9.15, 2015-05-21" );
u32 MpxModule::_version = 1915;

// Socket receive time out, in milliseconds
#define SOCKET_RCVTIMEO_MS  250

// Descriptions of MPIXD_ERR_xxx (see relaxd.h) errors from Relaxd module
static const std::string MPIXD_ERR_STR[16] =
{
    "MPIXD_ERR_ILLEGAL",       // 0x0001  // Illegal message type
    "MPIXD_ERR_LENGTH",        // 0x0002  // Incorrect message length
    "MPIXD_ERR_OFFSET",        // 0x0004  // Incorrect address offset
    "MPIXD_ERR_ROW",           // 0x0008  // Incorrect row number sequence
    "MPIXD_ERR_BUS",           // 0x0010  // Wishbone bus error
    "MPIXD_ERR_CONNECT",       // 0x0020  // Medipix2 chip unconnected error
    "MPIXD_ERR_WATCHDOG",      // 0x0040  // Medipix2 controller watchdog error
    "MPIXD_ERR_FATAL",         // 0x0080  // Medipix2 controller fatal error
    "MPIXD_ERR_TRIGGER",       // 0x0100  // Medipix2 external trigger
    "????",                    // 0x0200
    "????",                    // 0x0400
    "????",                    // 0x0800
    "????",                    // 0x1000
    "MPIXD_ERR_TIMEOUT",       // 0x2000  // Timed out
    "MPIXD_ERR_INTERRUPT",     // 0x4000  // Interrupted
    "MPIXD_ERR_UNKNOWN",       // 0x8000  // Unknown error
};

struct MpxPimpl
{
    boost::recursive_mutex _regReadMutex;
};

void MpxModule::initVars(int id)
{
    _confReg = 0;
    _confRegValid = false;
    _waitingForTrigger = false;
    _id = id;
    _hwInfoItems = NULL;
    _relaxdFwVersion = 0;
    _logVerbose = false;
    _frameCnt = 0;
    _callbackData = 0;
    _callback = 0;
    _ipAddress = ( MPIXD_IP_ADDR + ( id << 8 ) );
    _portNumber = MPIXD_PORT;
    _parReadout = true;
    _storePixelsCfg = false;
    _lastMpxErr = 0;
    _lastErrStr =  "";
    _sockMgr = NULL;
    _sockMonMgr = NULL;
    _newFrame = false;
    _rmNotRequired = false;
    _codec = 0;
    _daqThreadHandler = NULL;
    _lostRowRetryCnt = 2;
    _hasGaps = false;
    _invalidLutZero = true; // Invalid lut entry is 0 by default, -1 otherwise (set with hwInfo)
    _pimpl = new MpxPimpl();
}

// ----------------------------------------------------------------------------

MpxModule::MpxModule( int id,
                      int mpx_type,
                      int ndevices,
                      int nrows,
                      int first_chipnr,
					  MpxLogger *log )
                      :_pimpl(NULL)
{
    initVars(id);
    setupLog(log);
	
    populateIpAddrChar();

    _log->info(_versionStr);
	_log->scratch << "==> Relaxd module ID=" << id
          << " IP=" << _ipAddressChar << std::endl;
	_log->logScratch(MPX_INFO);
	
	initSocket();

    checkFirmwareRevision();

    // Get chip identifiers from the module
    u32 dummy;

    setupChipIdMap(false, 0, ndevices);
    
    // CJD - 12/7/2012 This seems pointless?? what is going on here?
    // CJD - 03/15/2013, figured it out, the readChipId function actually updates the
    // chipId array... in addition to returning the chipId which is then thrown away... 
    // smart programming there... ;)
    for ( int i = 0; i < MPIX_MAX_DEVS; ++i )
    {
        // If module is not connected stop reading the chip IDs
        if ( this->readChipId( i, &dummy ) == MPXERR_COMM )
        {
            break;
        }
    }

    // Are we going to determine ourselves what devices are connected and where?
    if ( ndevices == 0 )
    {
        ndevices = getNumDevices(&mpx_type, &first_chipnr);

        // Set the number of 'rows'
        nrows = setNumRows(nrows, ndevices);
    }

    // if no mpx type has been specified, find it in the
    // no of devices specified.
    if ( mpx_type == 0 )
    {
        mpx_type = getMpxType(ndevices);
    }
    
    // remap the chip ids.
    setupChipIdMap(true, first_chipnr, ndevices);

    logDevicesInfo(nrows, ndevices, first_chipnr, mpx_type);

    // would be better if devInfo was a proper object with constructor etc...
    initDevInfo(nrows, ndevices, mpx_type);

    setupBoardId(id, ndevices);
    
    // would be better if accPars was a proper object with constructor etc...
    initAccPars();

    initFsrs(true);

    this->initHwInfoItems();

    _testPulseFreq = 0;

    // The following assignments should match the initial (AD5328) DAC settings
    // done in the Relaxd Mico32 software
    _biasAdjust    = 0;
    _testPulseLow  = 0.0;
    _testPulseHigh = 0.0;


    if ( mpx_type == MPX_TPX )
    {
        _vddAdjust = 7;
    }
    else
    {
        _vddAdjust = 78;
    }

    _codec = new MpxCodecMgr();
}

// ----------------------------------------------------------------------------

MpxModule::MpxModule( int id, bool ping )
:_pimpl(NULL)
{
    UNREFERENCED_PARAMETER(ping);
    initVars(id);

    // This is a special 'partial' constructor, just to be able to quickly 'ping'
    // a Relaxd module, i.e. to establish its presence, using method ping()
    initFsrs(false);
    setupLog(NULL);
    _sockMgr = NULL;
    this->openSockMon();
}

// ----------------------------------------------------------------------------

MpxModule::~MpxModule()
{
    for ( int i = 0; i < MPIX_MAX_DEVS; ++i )
    {
        if (_fsr[i] != NULL)
        {
            delete _fsr[i];
        }
    }

    this->closeSock();
	if (_logManaged) 
    {
        delete (MpxFileLogger*)_log;
    }
    if (_codec)
    {
        delete _codec;
    }
	if (_hwInfoItems != NULL)
    {
        delete (_hwInfoItems);
    }
    if (_pimpl != NULL)
    {
        delete _pimpl;
    }
}

void MpxModule::initDevInfo(int nrows, int ndevices, int mpx_type)
{
    _devInfo.ifaceName      = "Relaxd";  // Name of interface
    _devInfo.numberOfChips  = ndevices;  // Number of chips
    _devInfo.numberOfRows   = nrows;     // Chip rows(quad has 4 chips in 2 rows)
    _devInfo.pixCount       = ndevices * MPIX_PIXELS; // Total number of pixels
    // Row length in pixels
    // (e.g 256 for single chip, 512 for quad, but also 512 for 2 chips in 1 row)
    _devInfo.rowLen         = ( ndevices * MPIX_COLS ) / nrows;
    // Medipix type: MPX_ORIG, MPX_MXR or MPX_TPX (1,2,3)
    _devInfo.mpxType        = mpx_type;
    // Clock frequency [MHz] for readout
    _devInfo.clockReadout   = 125.0;
    _devInfo.clockTimepix   = 100.0;

    // Hardware timer capabilities
    _devInfo.timerMaxVal    = 42949.67295;// Maximum value of hw timer [s]
    _devInfo.timerMinVal    = .000010;    // Minimum value of hw timer [s]: 10 us
    _devInfo.timerStep      = .000010;    // Step of hw timer [s]: 10 us

    // Supported acq. mode bitwise
    _devInfo.suppAcqModes   = ( ACQMODE_ACQSTART_TIMERSTOP |
                                //ACQMODE_ACQSTART_HWTRIGSTOP |
                                ACQMODE_ACQSTART_SWTRIGSTOP |
                                ACQMODE_HWTRIGSTART_TIMERSTOP |
                                ACQMODE_HWTRIGSTART_HWTRIGSTOP |
                                ACQMODE_SWTRIGSTART_TIMERSTOP |
                                ACQMODE_SWTRIGSTART_SWTRIGSTOP
                                //| ACQMODE_EXTSHUTTER
                                |ACQMODE_SWTRIGMASTER_HWTRIGSLAVE
                              );

    // Callback is supported (acq. is finished, triggers)
// juhe - 13/12/2012 callback is not supported on other platforms than Windows
#if defined(_WINDLL) || defined(WIN32)
    _devInfo.suppCallback   = TRUE;
#else
	_devInfo.suppCallback   = FALSE;
#endif

    // Testpulse capabilities
    _devInfo.maxPulseCount  = 10000; // Maximum number of pulses that can be sent
    _devInfo.maxPulseHeight = 1.2;   // Max pulse height [V]
    _devInfo.maxPulsePeriod = 2e-3;  // Min frequency = 0.5KHz, Max freq is 10KHz

    // Ext DAC capabilities
    _devInfo.extDacMaxV     = 3.0;      // Maximum external DAC voltage
    _devInfo.extDacMinV     = 0;        // Minimum external DAC voltage
    _devInfo.extDacStep     = 3.0 / 4095; // Ext. DAC step size
}

void MpxModule::initAccPars()
{
    // Initialize acquisition parameters
    _acqPars.enableCst        = false;
    _acqPars.mode             = ACQMODE_ACQSTART_TIMERSTOP;
    _acqPars.polarityPositive = true;
    _acqPars.time             = 0;
    _acqPars.useHwTimer       = false;
}

void MpxModule::initFsrs(bool allocate)
{
    // Allocate space for FSRs to remember
    for ( int i = 0; i < MPIX_MAX_DEVS; ++i )
    {
        if (allocate == true)
        {
            _fsr[i] = new Fsr( MPIX_FSR_BITS );
        }
        else
        {
            _fsr[i] = NULL;
        }
        _fsrValid[i] = false;
    }
}

void MpxModule::checkFirmwareRevision()
{
	std::stringstream oss;

    if ( getFirmwareVersion( ( &this->_relaxdFwVersion ) ) != MPXERR_NOERROR )
    {
        setLastError( "Failed to read firmware version" );
        //juhe - 05/04/2012 error check changed
        //cjd - 12/7/2012 where is err set that is relevant to this function?
        //this->_lastMpxErr = err;
    }

    if ( this->_relaxdFwVersion < REQUIRED_FW_VERSION )
    {
        oss << "Invalid software version! Required: " << REQUIRED_FW_VERSION <<
            ", Found: " << _relaxdFwVersion;

        setLastError( oss.str() );
		oss.clear();
    }
}

void MpxModule::setupBoardId(int id, int ndevices)
{
	std::stringstream oss;
    // 'Chip board ID'
    oss.str( std::string( "" ) );
    //oss << "MPX-" << TYPENAME[mpx_type] << "-" << id;
    oss << id;

    for ( int i = 0; i < ndevices; ++i )
    {
        oss << "_" << this->chipName( i );
    }

    strncpy( _devInfo.chipboardID, oss.str().c_str(), MPX_MAX_CHBID );
}

void MpxModule::setupChipIdMap(bool remap, int first_chipnr, int ndevices)
{
    int i;
    if (remap == false)
    {
        for ( i = 0; i < MPIX_MAX_DEVS; ++i ) 
        {
            _chipId[i] = 0;
            _chipMap[i] = i;
        }
    }
    else
    {
        u32 chipMap[MPIX_MAX_DEVS] = {0};
        if (_hasGaps == true)
        {
            int index = 0;
            for ( int i = 0; i < MPIX_MAX_DEVS; ++i )
            {
                if (_chipId[i] != 0)
                {
                    chipMap[index] = _chipMap[i];
                    index++;
                }
            }
            _parReadout = false;
        }
        else
        {
            for ( int i = 0; i < ndevices; ++i )
            {
                chipMap[i] = ( first_chipnr + i ) % MPIX_MAX_DEVS;
            }
        }
        memcpy(_chipMap, chipMap, sizeof(chipMap));
    }
}

void MpxModule::setupLog(MpxLogger *log)
{
    if (log != NULL) 
    {
        _log = log;
        _logManaged = false;
    }
    else
    {
        _log = new MpxFileLogger();	// defaults are good
        _logManaged = true;
    }
}

int MpxModule::getNumDevices(int *mpx_type, int *first_chipnr)
{
    bool mightHaveGaps = false;
    int ndevices = 0;
    *mpx_type     = 0;
    *first_chipnr = -1;

    for ( int i = 0; i < MPIX_MAX_DEVS; ++i )
    {
        if ( _chipId[i] != 0 )
        {
            ++ndevices;

            if ( (*mpx_type) == 0 )
            {
                *mpx_type = this->chipType( i );
            }
            else if ( (*mpx_type) != this->chipType( i ) )
            {
                break; // Device of different types? No way...
            }

            if ( (*first_chipnr) == -1 )
            {
                (*first_chipnr) = i;
            }
            if (mightHaveGaps == true)
            {
                _hasGaps = true;
            }
        }
        else
        {
            if (ndevices > 0)
            {
                mightHaveGaps = true;
            }
        }
    }
    return ndevices;
}

int MpxModule::getMpxType(int ndevices)
{
    int mpx_type = 0;
    for ( int i = 0 ; i < ndevices ; ++i  )
    {
        if ( this->chipType( i ) != 0 )
        {
            mpx_type = this->chipType( i );
            break;
        }
    }
    return mpx_type;
}

int MpxModule::setNumRows(int nrows, int ndevices)
{
    if (nrows == MPIX_LO_ONE_COLUMN) 
    {
        nrows = ndevices;            
    }
    else
    {
        if ( ndevices == 4 )
        {
            nrows = 2;
        }
        else
        {
            nrows = 1;
        }
    }
    return nrows;
}

void MpxModule::logDevicesInfo(int nrows, int ndevices, int first_chipnr, int mpx_type)
{
    _log->scratch	<< "==> Chip IDs: ";

    for ( int i = 0; i < ndevices; ++i )
    {
        if ( _chipId[_chipMap[i]] != 0 )
        {
            _log->scratch << i << "=" << this->chipName( i ) << " ";
        }
    }

	_log->logScratch(MPX_INFO);
    
    _log->scratch << "==> TYPE=" << TYPENAME[mpx_type]
            << ", " << ndevices << " chips in " << nrows << " rows, "
            << "ChipNr=" << first_chipnr << std::endl;
	_log->logScratch(MPX_INFO);
}

void MpxModule::populateIpAddrChar()
{
    std::ostringstream oss;
    oss << ( ( _ipAddress & 0xFF000000 ) >> 24 ) << "."
        << ( ( _ipAddress & 0x00FF0000 ) >> 16 ) << "."
        << ( ( _ipAddress & 0x0000FF00 ) >>  8 ) << "."
        << ( ( _ipAddress & 0x000000FF ) >>  0 );
    std::string str = oss.str();

    for ( u32 i = 0; i < str.length(); ++i )
    {
        _ipAddressChar[i] = str[i];
    }

    _ipAddressChar[str.length()] = '\0';
}

void MpxModule::populateChipNames()
{
    std::ostringstream oss;
    oss.str( "" );
    
    for (int i = 0; i < this->chipCount(); i++) 
    {
        oss << this->chipName(i) <<" ";
    }
    std::string str = oss.str();
    MpxLogger::trim(str);
    for ( u32 i = 0; i < str.length(); ++i )
    {
        _chipNames[i] = str[i];
    }
    _chipNames[str.length()] = '\0';
}

// ----------------------------------------------------------------------------

void MpxModule::initHwInfoItems()
{
    LOGCALL( "initHwInfoItems()" );
    _hwInfoItems = new std::vector<HwInfoItem>;
    _hwInfoItems->clear();

    HwInfoItem hwi;

    // 0 HW_ITEM_LIBVERSION
    hwi.count = 1;
    hwi.data  = &_version;
    hwi.name  = "HW-lib version";
    hwi.descr = "Hardware-lib version";
    hwi.flags = 0;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 1 HW_ITEM_RELAXDSWVERSION
    hwi.count = 1;
    hwi.data  = &_relaxdFwVersion;
    hwi.name  = "FW version";
    hwi.descr = "Relaxd firmware version";
    hwi.flags = 0;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 2 HW_ITEM_IPADDR
    hwi.count = ( u32 ) strlen(_ipAddressChar) + 1;
    //hwi.count = 1; // Does not work for TYPE_STRING ?
    hwi.data  = static_cast<void *> ( _ipAddressChar );
    hwi.name  = "IP Address";
    hwi.descr = "Relaxd IP Address";
    hwi.flags = 0;
    //hwi.type  = TYPE_CHAR;
    hwi.type  = TYPE_STRING;
    _hwInfoItems->push_back( hwi );

    // 3 HW_ITEM_PORTNR
    hwi.count = 1;
    hwi.data  = &_portNumber;
    hwi.name  = "IP Port number";
    hwi.descr = "Relaxd IP Port number";
    hwi.flags = 0;
    //hwi.flags = MPX_HWINFO_CFGSAVE | MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 4 HW_ITEM_IMGFILENAME
    hwi.count = ( u32 ) strlen( ImgFileNameChar ) + 1;
    hwi.data  = static_cast<void *> ( ImgFileNameChar );
    hwi.name  = "Image file name";
    hwi.descr = "Data file name for image";
    hwi.flags = 0;
    hwi.type  = TYPE_STRING;
    _hwInfoItems->push_back( hwi );

    // 5 HW_ITEM_BOARDTEMP
    hwi.count = 1;
    hwi.data  = &_u32ForPixelman;
    hwi.name  = "BoardTemperature";
    hwi.descr = "Board temperature [C]";
    hwi.flags = 0;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 6 HW_ITEM_DETECTORTEMP
    hwi.count = 1;
    hwi.data  = &_u32ForPixelman;
    hwi.name  = "DetectorTemperature";
    hwi.descr = "Detector temperature [C]";
    hwi.flags = 0;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 7 HW_ITEM_VDD
    hwi.count = 1;
    hwi.data  = &_floatForPixelman;
    hwi.name  = "MpixVDD";
    hwi.descr = "Medipix digital voltage [Vadc]";
    hwi.flags = 0;
    hwi.type  = TYPE_FLOAT;
    _hwInfoItems->push_back( hwi );

    // 8 HW_ITEM_VDDA
    hwi.count = 1;
    hwi.data  = &_floatForPixelman;
    hwi.name  = "MpixVDDA";
    hwi.descr = "Medipix analog voltage [Vadc]";
    hwi.flags = 0;
    hwi.type  = TYPE_FLOAT;
    _hwInfoItems->push_back( hwi );

    // 9 HW_ITEM_TESTPULSELO
    hwi.count = 1;
    hwi.data  = &_floatForPixelman;
    hwi.name  = "TestPulseLow";
    hwi.descr = "Test pulse voltage low [V]";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_FLOAT;
    _hwInfoItems->push_back( hwi );

    // 10 HW_ITEM_TESTPULSEHI
    hwi.count = 1;
    hwi.data  = &_floatForPixelman;
    hwi.name  = "TestPulseHigh";
    hwi.descr = "Test pulse voltage high [V]";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_FLOAT;
    _hwInfoItems->push_back( hwi );

    // 11 HW_ITEM_TESTPULSEFREQ
    hwi.count = 1;
    hwi.data  = &_testPulseFreq;
    hwi.name  = "TestPulseFreq";
    hwi.descr = "Test pulse frequency [0.5kHz]";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 12 HW_ITEM_BIAS_VOLTAGE_ADJUST
    hwi.count = 1;
    hwi.data  = &_biasAdjust;
    hwi.name  = "BiasAdjust";
    hwi.descr = "Sensor bias voltage adjustment [0..100%]";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 13 HW_ITEM_VDD_ADJUST
    hwi.count = 1;
    hwi.data  = &_vddAdjust;
    hwi.name  = "MpixVddAdjust";
    hwi.descr = "Medipix digital voltage adjustment [0..100%] (ca 1.8-2.3V)";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 14 HW_ITEM_ANALOGCURR
    hwi.count = 1;
    hwi.data  = &_floatForPixelman;
    hwi.name  = "MpixAnalogCurrent";
    hwi.descr = "Medipix analog current [Vadc]";
    hwi.flags = 0;
    hwi.type  = TYPE_FLOAT;
    _hwInfoItems->push_back( hwi );

    // 15 HW_ITEM_DIGITALCURR
    hwi.count = 1;
    hwi.data  = &_floatForPixelman;
    hwi.name  = "MpixDigitalCurrent";
    hwi.descr = "Medipix digital current [Vadc]";
    hwi.flags = 0;
    hwi.type  = TYPE_FLOAT;
    _hwInfoItems->push_back( hwi );

    // 16 HW_ITEM_CHIPIDS
    hwi.count = _devInfo.numberOfChips;
    hwi.data  = &_chipId[0];
    hwi.name  = "ChipID";
    hwi.descr = "Chip identifier (24-bits)";
    hwi.flags = 0;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 17 HW_ITEM_CHIPNAMES

    populateChipNames();

    hwi.count = ( u32 ) strlen(_chipNames) + 1;
    //hwi.count = 1; // Does not work for TYPE_STRING ?
    hwi.data  = static_cast<void *> ( _chipNames );
    hwi.name  = "ChipNames";
    hwi.descr = "ChipIDs translated to names";
    hwi.flags = 0;
    //hwi.type  = TYPE_CHAR;
    hwi.type  = TYPE_STRING;
    _hwInfoItems->push_back( hwi );

    // 18 HW_ITEM_FIRSTCHIPNR
    hwi.count = 1;
    hwi.data  = &_chipMap[0];
    hwi.name  = "FirstChipNumber";
    hwi.descr = "First chip number";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 19 HW_ITEM_LOGVERBOSE
    hwi.count = 1;
    hwi.data  = &_boolForPixelman;
    hwi.name  = "LogVerbose";
    hwi.descr = "Logging verbose";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

    // 20 HW_ITEM_PARREADOUT
    hwi.count = 1;
    hwi.data  = &_boolForPixelman;
    hwi.name  = "ParReadout";
    hwi.descr = "Parallel readout";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

    // 21 HW_ITEM_STOREPIXELSCFG
    hwi.count = 1;
    hwi.data  = &_boolForPixelman;
    hwi.name  = "StorePixelsCfg";
    hwi.descr = "Store next pixels configuration";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

    // 22 HW_HW_ITEM_STOREDACS
    hwi.count = 1;
    hwi.data  = &_boolForPixelman;
    hwi.name  = "StoreDacs";
    hwi.descr = "Store current DAC configuration";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

    // 23 HW_ITEM_ERASE_STORED_CFG
    hwi.count = 1;
    hwi.data  = &_boolForPixelman;
    hwi.name  = "EraseStoredConfig";
    hwi.descr = "Erase stored pixels+DAC configuration";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

    // 24 HW_ITEM_CONF_TPX_CLOCK
    hwi.count = 1;
    hwi.data  = &_u32ForPixelman;
    hwi.name  = "ConfTpxClock";
//    hwi.descr =
//        "Timepix measurement clock (MHz: 0=100 1=25 2=50 3=10 4=2.5 5=Ext)";
// juhe 2012-11-12 fixed description for the new (red) relaxd board
    hwi.descr =
        "Timepix measurement clock (MHz: 0=100 1=50 2=10 3=Ext)";
    hwi.flags = MPX_HWINFO_CANCHANGE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 25 HW_ITEM_CONF_RO_CLOCK_125MHZ
    hwi.count = 1;
    hwi.data  = &_boolForPixelman;
    hwi.name  = "ConfRoClock125MHz";
    hwi.descr = "Readout clock (True=150MHz False=65.2MHz)";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

    // 26 HW_ITEM_CONF_TPX_PRECLOCKS
    hwi.count = 1;
    hwi.data  = &_boolForPixelman;
    hwi.name  = "ConfTpxPreclocks";
    hwi.descr = "Use preclocks when a trigger is received";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

     // 27 HW_ITEM_RESERVED
     hwi.count = 1;
     hwi.data  = &_boolForPixelman;
     hwi.name  = "ConfHvBiasDisable";
     hwi.descr = "Disables sensor bias supply (True=supply off False=supply on)";
     hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
     hwi.type  = TYPE_BOOL;
     _hwInfoItems->push_back( hwi );

    // 28 HW_ITEM_SET_TRIG_TYPE
    hwi.count = 1;
    hwi.data  = &_u32ForPixelman;
    hwi.name  = "TriggerType";
    hwi.descr = "Trigger type (0=Negative, 1=Positive, 2=Pulse)";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 29 HW_ITEM_LOST_ROW_RETRY_CNT
    hwi.count = 1;
    hwi.data  = &_u32ForPixelman;
    hwi.name  = "LostRowRetryCnt";
    hwi.descr = "Lost row retry count";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );

    // 30 HW_ITEM_INVALID_LUT_ZERO
    hwi.count = 1;
    hwi.data  = &_invalidLutZero;
    hwi.name  = "InvalidLutZero";
    hwi.descr = "Invalid lookup table entry is zero";
    hwi.flags = MPX_HWINFO_CANCHANGE | MPX_HWINFO_CFGSAVE;
    hwi.type  = TYPE_BOOL;
    _hwInfoItems->push_back( hwi );

    // 31 Dummy... HW_ITEM_DUMMY
    hwi.count = 1;
    hwi.data  = &_chipMap[0];
    hwi.name  = "Dummy";
    hwi.descr = "Dummy";
    hwi.flags = 0;
    hwi.type  = TYPE_U32;
    _hwInfoItems->push_back( hwi );
}

// ----------------------------------------------------------------------------

int MpxModule::init()
{
/*
    LOGCALL( "init()" );
    // Reset devices and reload onboard stored settings
 
    //juhe - 05/04/2012 added MPXERR_MPXDEV_NOTINIT error - errors were not not checked before
    int ret=this->loadConfig();
    if (ret==MPXERR_NOERROR) {
        return MPXERR_NOERROR;        
    } else {
        //return MPXERR_MPXDEV_NOTINIT;
        return ret;
    }
*/
    
    return MPXERR_NOERROR;
}

void MpxModule::getHwInfoVCParam(int nParam)
{
    u32 adc_val = 0;

    this->readAdc( nParam, &adc_val );
    _floatForPixelman = ( ( ( float ) adc_val /
        static_cast<float> ( ADC_DIGITAL_MAX_VALUE ) ) *
        ADC_ANALOG_MAX_VALUE );
}

void MpxModule::getHwInfoBool(bool bParam)
{
    if ( bParam )
    {
        _boolForPixelman = 1;
    }
    else
    {
        _boolForPixelman = 0;
    }
}

int MpxModule::getHwInfoTpxClock()
{
    int ret = MPXERR_NOERROR;
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        ret = MPXERR_UNEXPECTED;
    }
    else
    {
        _u32ForPixelman = ( ( reg & MPIX2_CONF_TPX_CLOCK_MASK ) >>
            MPIX2_CONF_TPX_CLOCK_SHIFT );

        switch ( _u32ForPixelman )
        {
        case 0:
            _devInfo.clockTimepix = 100.0;
            break;

        case 1:
            _devInfo.clockTimepix = 50.0;
            break;

        case 2:
            _devInfo.clockTimepix = 10.0;
            break;

        default:
            _devInfo.clockTimepix = 0.0;
            break;
        }
    }
    return ret;
}

int MpxModule::getHwInfoRoClock125()
{
    int ret = MPXERR_NOERROR;
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        ret = MPXERR_UNEXPECTED;
    }
    else
    {
        _boolForPixelman = ( reg & MPIX2_CONF_RO_CLOCK_125MHZ ) != 0;

        if ( _boolForPixelman )
        {
            _devInfo.clockReadout = 125.0;
        }
        else
        {
            _devInfo.clockReadout = 62.5;
        }
    }
    return ret;
}

int MpxModule::getHwInfoTpxPreClocks()
{
    int ret = MPXERR_NOERROR;
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        ret = MPXERR_UNEXPECTED;
    }
    else
    {
        _boolForPixelman = ( reg & MPIX2_CONF_TPX_PRECLOCKS ) != 0;
    }
    return ret;
}

int MpxModule::getHwInfoTrigType()
{
    int ret = MPXERR_NOERROR;
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        ret = MPXERR_UNEXPECTED;
    }
    else
    {
        _u32ForPixelman = ( reg & MPIX2_CONF_TPX_TRIG_TYPE_MASK ) >> MPIX2_CONF_TPX_TRIG_TYPE_SHIFT;
    }
    return ret;
}

int MpxModule::getLostRowRetryCnt()
{
    _u32ForPixelman = _lostRowRetryCnt;
    return MPXERR_NOERROR;
}

int MpxModule::updateHwInfo(int index, HwInfoItem *hw_item, int *sz)
{
    int ret = MPXERR_NOERROR;
    // Copy local hardware info item to pointer location
    HwInfoItem *local_item = &(*_hwInfoItems)[index];
    hw_item->type  = local_item->type;
    hw_item->count = local_item->count;
    hw_item->flags = local_item->flags;
    hw_item->name  = local_item->name;
    hw_item->descr = local_item->descr;

    int size = sizeofType[local_item->type] * local_item->count;

    if ( *sz == 0 )
    {
        *sz = size; // Inform caller about required size
    }
    else if ( *sz < size )
    {
        std::ostringstream oss;
        oss << "Invalid hw-info-item data buffer size, need "
            << size << ", available " << *sz;
        setLastError(oss.str());
        *sz = size; // Inform caller about required size
        ret = MPXERR_BUFFER_SMALL;
    }
    else
    {
        memcpy( hw_item->data, local_item->data, size );
    }
    return ret;
}

// ----------------------------------------------------------------------------

int MpxModule::getHwInfo( int index, HwInfoItem *hw_item, int *sz )
{
    int ret = MPXERR_NOERROR;
    LOGCALL2( "getHwInfo()", index, *sz );

    if ( !this->validHwInfoIndex( index ) ) 
    {
        return MPXERR_INVALID_PARVAL;
    }

    switch ( index )
    {
    case HW_ITEM_RELAXDSWVERSION:
        this->getFirmwareVersion( &_relaxdFwVersion );
        break;

    case HW_ITEM_BOARDTEMP:
        this->readTsensor( 1, &_u32ForPixelman );
        break;

    case HW_ITEM_DETECTORTEMP:
        this->readTsensor( 0, &_u32ForPixelman );
        break;

    case HW_ITEM_ANALOGCURR:
        getHwInfoVCParam(ADC_MPIXA_CURRENT_I);
        break;

    case HW_ITEM_DIGITALCURR:
        getHwInfoVCParam(ADC_MPIXD_CURRENT_I);
        break;

    case HW_ITEM_VDD:
        getHwInfoVCParam(ADC_MPIXD_VOLTAGE_I);
        break;

    case HW_ITEM_VDDA:
        getHwInfoVCParam(ADC_MPIXA_VOLTAGE_I);
        break;

    case HW_ITEM_TESTPULSELO:
        _floatForPixelman = ( _testPulseLow * DAC_ANALOG_MAX_VALUE ) / DAC_DIGITAL_MAX_VALUE;
        break;

    case HW_ITEM_TESTPULSEHI:
        _floatForPixelman = ( _testPulseHigh * DAC_ANALOG_MAX_VALUE ) / DAC_DIGITAL_MAX_VALUE;
        break;

    case HW_ITEM_LOGVERBOSE:
        getHwInfoBool(_logVerbose);
        break;

    case HW_ITEM_PARREADOUT:
        getHwInfoBool(_parReadout);
        break;

    case HW_ITEM_STOREPIXELSCFG:
        getHwInfoBool(_storePixelsCfg);
        break;

    case HW_ITEM_STOREDACS:
    case HW_ITEM_ERASE_STORED_CFG:
        getHwInfoBool(false);
        break;

    case HW_ITEM_CONF_TPX_CLOCK:
        ret = getHwInfoTpxClock();
        break;

    case HW_ITEM_CONF_RO_CLOCK_125MHZ:
        ret = getHwInfoRoClock125();
        break;

    case HW_ITEM_CONF_TPX_PRECLOCKS:
        ret = getHwInfoTpxPreClocks();
        break;

    case HW_ITEM_SET_TRIG_TYPE:
        ret = getHwInfoTrigType();
        break;

    case HW_ITEM_LOST_ROW_RETRY_CNT:
        ret = getLostRowRetryCnt();
        break;

    default:
        break;
    }
    if (ret == MPXERR_NOERROR)
    {
        ret = updateHwInfo(index, hw_item, sz);
    }

    return ret;
}

// ----------------------------------------------------------------------------
// Getting HwInfo item flag
// [in] index - index of HwItem (0..HwGetHwInfoCountType()-1)
// [out] flags - flag of item
// ----------------------------------------------------------------------------

int MpxModule::getHwInfoFlags( int index, u32 *flags )
{
    //LOGCALL1( "getHwInfoFlags()", index );
    if ( !this->validHwInfoIndex( index ) )
    {
        return MPXERR_INVALID_PARVAL;
    }

    *flags = (*_hwInfoItems)[index].flags;
    return MPXERR_NOERROR;
}

int MpxModule::setHwInfoTestPulseHiLo(HwInfoItem *info, int testPulse)
{
    int errval;
    float val_f;
    float pulse_dac;
    val_f = *( static_cast<float *> ( info->data ) );

    if ( val_f > static_cast<float> ( _devInfo.maxPulseHeight ) )
    {
        val_f = static_cast<float> ( _devInfo.maxPulseHeight );
    }
    //pulse_lo_dac = ((val_f + (float) 0.015) *
    pulse_dac = ( val_f * DAC_DIGITAL_MAX_VALUE ) / DAC_ANALOG_MAX_VALUE;

    errval = this->writeDac( testPulse, static_cast<u32> ( pulse_dac ) );

    if ( errval == MPXERR_NOERROR ) 
    {
        _testPulseLow = pulse_dac;
    }
    return errval;
}

int MpxModule::setHwInfoTestPulseFreq(HwInfoItem *info)
{
    int errval;
    u32 val_u;

    val_u = *( static_cast<u32 *> ( info->data ) );

    if ( val_u > 20 )
    {
        val_u = 20;
    }

    errval = this->configTestPulse( static_cast<u32> ( _testPulseLow ),
        static_cast<u32> ( _testPulseHigh ), val_u, 0 );

    if ( errval == MPXERR_NOERROR ) 
    {
        _testPulseFreq = val_u;
    }

    return errval;
}

int MpxModule::setHwInfoBiasVoltageAdjust(HwInfoItem *info)
{
    int errval;
    u32 val_u;
    u32 dac_value;
    // No values over 100%
    val_u = *( static_cast<u32 *> ( info->data ) );

    if ( val_u > 100 )
    {
        val_u = 100;
    }

    // Convert value to DAC count
    dac_value = ( val_u * DAC_DIGITAL_MAX_VALUE ) / 100;

    errval = this->writeDac( DAC_BIAS_ADJUST_I, dac_value );

    if ( errval == MPXERR_NOERROR )
    {
        _biasAdjust = val_u;
    }
    return errval;
}

int MpxModule::setHwInfoVddAdjust(HwInfoItem *info)
{
    int errval;
    u32 val_u;
    u32 dac_value;
    // No values over 100%
    val_u = *( static_cast<u32 *> ( info->data ) );

    if ( val_u > 100 )
    {
        val_u = 100;
    }

    // Convert value to DAC count (NB: 0%=max DAC, 100%=min DAC)
    dac_value = ( DAC_DIGITAL_MAX_VALUE -
        ( val_u * DAC_DIGITAL_MAX_VALUE ) / 100 );

    errval = this->writeDac( DAC_VDD_ADJUST_I, dac_value );

    if ( errval == MPXERR_NOERROR )
    {
        _vddAdjust = val_u;
    }

    return errval;
}

void MpxModule::setHwInfoFirstChipNr(HwInfoItem *info)
{
    if (_hasGaps == false)
    {
        u32 val_u;
        val_u = *( static_cast<u32 *> ( info->data ) );
        if ( val_u >= MPIX_MAX_DEVS )
        {
            val_u = MPIX_MAX_DEVS - 1;
        }

        // Adjust map for chip numbering and selection
        for (int i = 0; i < _devInfo.numberOfChips; ++i )
        {
            _chipMap[i] = ( val_u + i ) % MPIX_MAX_DEVS;
        }
    }
}

void MpxModule::setHwInfoBool(HwInfoItem *info, bool *bParam, const char* dbg)
{
    u32   val_u;
    val_u = *( static_cast<u32 *> ( info->data ) );

    if ( val_u > 0 )
    {
        (*bParam) = true;
    }
    else
    {
        (*bParam) = false;
    }

    _log->scratch << dbg << _logVerbose;
    _log->logScratch(MPX_INFO);
}

int MpxModule::setHwInfoStoreDacs(HwInfoItem *info)
{
    int errval = MPXERR_NOERROR;
    BOOL val_b;
    val_b = *( static_cast<BOOL *> ( info->data ) );

    if ( val_b )
    {
        errval = this->storeDacs();
    }
    return errval;
}

int MpxModule::setHwInfoEraseStoredCfg(HwInfoItem *info)
{
    int errval = MPXERR_NOERROR;
    BOOL val_b;
    val_b = *( static_cast<BOOL *> ( info->data ) );

    if ( val_b )
    {
        errval = this->eraseDacs();

        if ( errval == MPXERR_NOERROR )
        {
            errval = this->erasePixelsCfg();
        }
    }
    return errval;
}

int MpxModule::setHwInfoConfTpxClock(HwInfoItem *info)
{
    int errval = MPXERR_NOERROR;
    u32   val_u;
    val_u = *( static_cast<u32 *> ( info->data ) );

    if ( val_u > 8 )
    {
        errval = MPXERR_INVALID_PARVAL;
    }
    else
    {
        this->rmwReg( MPIX2_CONF_REG_OFFSET,
            val_u << MPIX2_CONF_TPX_CLOCK_SHIFT,
            MPIX2_CONF_TPX_CLOCK_MASK );
    }
    return errval;
}

int MpxModule::setHwInfoTrigType(HwInfoItem *info)
{
    int errval = MPXERR_NOERROR;
    u32   val_u;
    val_u = *( static_cast<u32 *> ( info->data ) );

    if ( val_u > 2 )
    {
        errval = MPXERR_INVALID_PARVAL;
    }
    else
    {
        this->rmwReg( MPIX2_CONF_REG_OFFSET,
            val_u << MPIX2_CONF_TPX_TRIG_TYPE_SHIFT,
            MPIX2_CONF_TPX_TRIG_TYPE_MASK );
    }
    return errval;
}

int MpxModule::setLostRowRetryCnt(HwInfoItem *info)
{
    _lostRowRetryCnt = *( static_cast<u32 *> ( info->data ) );
    return MPXERR_NOERROR;
}

void MpxModule::setHwInfoConfigRegBits(HwInfoItem *info, u32 bits)
{
    BOOL	val_b;
    val_b = *( static_cast<BOOL *> ( info->data ) );
    this->rmwReg( MPIX2_CONF_REG_OFFSET, val_b ? bits : 0, bits );
}

void MpxModule::setInvalidLutZero(BOOL invalidLutZero)
{
    _invalidLutZero = invalidLutZero;
    updateInvalidLutEntry(_invalidLutZero ? true : false);
}

// ----------------------------------------------------------------------------
// Setting data of HwInfo item
// [in] index - index of HwItem (0..HwGetHwInfoCountType()-1)
// [in] data - data for specified HwItem
// [in] dataSize - size of data
// ----------------------------------------------------------------------------

int MpxModule::setHwInfo( int index, void *data, int sz )
{
    int ret = MPXERR_NOERROR;

    LOGCALL2( "setHwInfo()", index, ( *( static_cast<u32 *>( data ) ) ) );

    if ( !this->validHwInfoIndex( index ) )
    {
        return MPXERR_INVALID_PARVAL;
    }

    HwInfoItem *info = &(*_hwInfoItems)[index];

    if ( (u32)sz != (sizeofType[info->type] * info->count) )
    {
		setLastError("Invalid size of data for hw specific info item");
        return MPXERR_INVALID_PARVAL;
    }

    if ( info->flags & MPX_HWINFO_CANCHANGE )
    {
        memcpy( info->data, data, sz );

        switch ( index )
        {
        case HW_ITEM_TESTPULSELO: // Testpulse A
            ret = setHwInfoTestPulseHiLo(info, DAC_TESTPULSE_A_I);
            break;

        case HW_ITEM_TESTPULSEHI: // Testpulse B
            ret = setHwInfoTestPulseHiLo(info, DAC_TESTPULSE_B_I);
            break;

        case HW_ITEM_TESTPULSEFREQ: // TestPulseFreq
            ret = setHwInfoTestPulseFreq(info);
            break;

        case HW_ITEM_BIAS_VOLTAGE_ADJUST:// Sensor bias voltage adjustment in %
            ret = setHwInfoBiasVoltageAdjust(info);
            break;

        case HW_ITEM_VDD_ADJUST: // Medipix VDD voltage adjustment in %
            ret = setHwInfoVddAdjust(info);
            break;

        case HW_ITEM_FIRSTCHIPNR:
            setHwInfoFirstChipNr(info);
            break;

        case HW_ITEM_LOGVERBOSE:
            setHwInfoBool(info, &_logVerbose, "_logVerbose");
            break;

        case HW_ITEM_PARREADOUT:
            setHwInfoBool(info, &_parReadout, "_parReadout");
            break;

        case HW_ITEM_STOREPIXELSCFG:
            setHwInfoBool(info, &_storePixelsCfg, "_storePixelsCfg");
            break;

        case HW_ITEM_STOREDACS:
            ret = setHwInfoStoreDacs(info);
            break;

        case HW_ITEM_ERASE_STORED_CFG:
            ret = setHwInfoEraseStoredCfg(info);
            break;

        case HW_ITEM_CONF_TPX_CLOCK:
            ret = setHwInfoConfTpxClock(info);
            break;

        case HW_ITEM_CONF_RO_CLOCK_125MHZ:
            setHwInfoConfigRegBits(info, MPIX2_CONF_RO_CLOCK_125MHZ);
            break;

        case HW_ITEM_CONF_TPX_PRECLOCKS:
            setHwInfoConfigRegBits(info, MPIX2_CONF_TPX_PRECLOCKS);
            break;

        case HW_ITEM_SET_TRIG_TYPE:
            ret = setHwInfoTrigType(info);
            break;

        case HW_ITEM_LOST_ROW_RETRY_CNT:
            ret = setLostRowRetryCnt(info);
            break;

        case HW_ITEM_INVALID_LUT_ZERO:
            setInvalidLutZero((*( static_cast<BOOL *> ( info->data ) )) ? TRUE : FALSE);
            break;
        }
    }

    return ret;
}

// ----------------------------------------------------------------------------
// Fills DevInfo structure (informations about mpx and interface capabilities)
// [out] devInfo - "device info" structure that should be filled
// ----------------------------------------------------------------------------

int MpxModule::getDevInfo( DevInfo *dev_info )
{
    LOGCALL( "getDevInfo()" );
    memcpy( dev_info, &_devInfo, sizeof( DevInfo ) );
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------
// Sets acquisition parameters
// [in] pars - acquisition parameters/settings
// ----------------------------------------------------------------------------

int MpxModule::setAcqPars( AcqParams *pars )
{
    LOGCALL3( "setAcqPars(); mode,time,hwtimer:",
              pars->mode, pars->time, pars->useHwTimer );

    // Check for supported acquisition-mode bits
    if ( ( pars->mode & _devInfo.suppAcqModes ) != (u32)pars->mode )
    {
        std::ostringstream oss;
        oss << "### Invalid acquisition mode";
        _lastErrStr = oss.str();
        _log->error( _lastErrStr );
        return MPXERR_INVALID_PARVAL;
    }

    memcpy( &_acqPars, pars, sizeof( AcqParams ) );

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::setCallbackData( INTPTR data )
{
    LOGCALL1( "setCallbackData()", data );
    _callbackData = data;
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::setCallback( HwCallback callback )
{
    LOGCALL( "setCallback()" );
    _callback = callback;
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

void MpxModule::callbackToPixMan( int evt_type )
{
    LOGCALL1( "callbackToPixMan()", evt_type );
    if (evt_type == HWCB_ACQFINISHED)
    {
        // see GETBUSY NEWFRAME CALLBACK HACK
        // for more information as to why this is set here
        _newFrame = true;
    }
    if ( _callback )
    {
        _callback( _callbackData, evt_type, 0 );
    }
    else
    {
        std::ostringstream oss;
        oss  << "### No callback() function set";
        _lastErrStr = oss.str();
        _log->error( _lastErrStr );
    }
}

// ----------------------------------------------------------------------------

int MpxModule::startAcquisition()
{
    /* For Relaxd we'll want these acquisition modes (14 April 2010):
    1. SW start, SW stop
    2. SW start, Timer stop
    3. HW start, HW stop
    4. HW start, Timer stop
    5. SW start, HW stop
    */
    LOGCALL1( "startAcquisition(), mode=", _acqPars.mode );

    //_startTime = this->getTime();

    // Get current configuration register settings
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Reset 'use Timer'
    reg &= ~MPIX2_CONF_TIMER_USED;
    // Reset 'burst readout' bit, just in case
    reg &= ~MPIX2_CONF_BURST_READOUT;
    // Disable external trigger
    reg &= ~MPIX2_CONF_EXT_TRIG_ENABLE;
    // Close shutter (re-enabled below when appropriate)
    reg |= MPIX2_CONF_SHUTTER_CLOSED;

    // Try to match the Pixelman provided 'acquisition mode' and parameters
    // with a Relaxd parameter setting
    int result;

    switch ( _acqPars.mode )
    {
    case ACQMODE_ACQSTART_TIMERSTOP:     // 0x0001
    case ACQMODE_HWTRIGSTART_TIMERSTOP:  // 0x0010
    case ACQMODE_SWTRIGSTART_TIMERSTOP:  // 0x0040
        if ( _acqPars.useHwTimer )
        {
            // Timer setting in units of 10 microseconds
            u32 tmr = static_cast<u32> ( _acqPars.time / _devInfo.timerStep );

            if ( tmr == 0 ) tmr = 1;

            if ( this->writeReg( MPIX2_TIMER_REG_OFFSET, tmr ) != MPXERR_NOERROR )
                return MPXERR_UNEXPECTED;

            // Set 'use Timer' in configuration register
            reg |= MPIX2_CONF_TIMER_USED;

#ifdef DEBUG_LOG

			if ( _logVerbose ) {
                _log->scratch  << "startAcq(): use timer, "
                      << tmr << "*10 mus";
				_log->logScratch(MPX_TRACE);
			}



#endif
        }

        // Open shutter
        reg &= ~MPIX2_CONF_SHUTTER_CLOSED;

        result = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

        _waitingForTrigger = false;

#ifdef DEBUG_LOG

		if ( _logVerbose )
            _log->trace("startAcq(): open shutter");

#endif
        break;

    case ACQMODE_ACQSTART_HWTRIGSTOP:    // 0x0002
    case ACQMODE_ACQSTART_SWTRIGSTOP:    // 0x0004
    case ACQMODE_SWTRIGSTART_SWTRIGSTOP: // 0x0080
        // Open shutter
        reg &= ~MPIX2_CONF_SHUTTER_CLOSED;

        result = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

        _waitingForTrigger = false;

#ifdef DEBUG_LOG

        if ( _logVerbose )
            _log->info("startAcq(): open shutter");

#endif
        break;

    case ACQMODE_HWTRIGSTART_HWTRIGSTOP: // 0x0020
    case ACQMODE_EXTSHUTTER:             // 0x0100
    case ACQMODE_BURST:                  // 0x1000
        _newFrame = false;
        // Enable external trigger
        reg |= MPIX2_CONF_EXT_TRIG_ENABLE;

        // Trigger on rising edge
        reg &= ~MPIX2_CONF_EXT_TRIG_FALLING_EDGE;

        // Ready for next (external) trigger
        reg &= ~MPIX2_CONF_EXT_TRIG_INHIBIT;

        result = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

        _waitingForTrigger = true;

        // Hardware-triggered shutter, wait for data using a thread
        _daqThreadHandler->startDaqThread();
        break;

    default:
        result = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
        _waitingForTrigger = false;
        break;
    }

    return result;
}

// ----------------------------------------------------------------------------

int MpxModule::stopAcquisition()
{
    LOGCALL( "stopAcquisition()" );

    // Get current configuration register settings
    u32 reg = 0;
    if( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        return MPXERR_UNEXPECTED;
    }

    // Stop external trigger or operate shutter ?
    int result;

    if ( _acqPars.mode == ACQMODE_HWTRIGSTART_HWTRIGSTOP ||
            _acqPars.mode == ACQMODE_EXTSHUTTER ||
            _acqPars.mode == ACQMODE_BURST )
    {
        if ( _waitingForTrigger )
        {
            _daqThreadHandler->stopDaqThread();
        }

        // Disable external trigger
        reg &= ~MPIX2_CONF_EXT_TRIG_ENABLE;

        result = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

        _waitingForTrigger = false;
    }
    else
    {
        // Reset 'use Timer'
        // (essential to restart the hardware timer -if used- next time!)
        reg &= ~MPIX2_CONF_TIMER_USED;

        // Reset burst-readout bit
        reg &= ~MPIX2_CONF_BURST_READOUT;

        // Close shutter
        reg |= MPIX2_CONF_SHUTTER_CLOSED;

        result = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

#ifdef DEBUG_LOG

        if ( _logVerbose )
            _log->info("stopAcq(): close shutter");

#endif
    }

    return result;
}

// ----------------------------------------------------------------------------

int MpxModule::configAcqMode( bool ext_trig, bool use_timer, double time_s )
{
    /* For Relaxd we'll want these acquisition modes (14 April 2010):
    1. SW start, SW stop
    2. SW start, Timer stop
    3. HW start, HW stop
    4. HW start, Timer stop
    5. SW start, HW stop
    */
    LOGCALL3( "configAcqMode(); ext_trig, use_timer, time_s:",
              ext_trig, use_timer, time_s );

    // Get current configuration register settings
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Adjust configuration settings
    reg &= ~MPIX2_CONF_BURST_READOUT; // Reset 'burst readout' bit, just in case

    if ( use_timer )
    {
        // Trigger pulse duration 'time' given in seconds,
        // timer setting is in units of 10 microseconds
        u32 ts_10us = static_cast<u32> ( time_s / _devInfo.timerStep );

        if ( ts_10us == 0 ) ts_10us = 1;

        if ( this->writeReg( MPIX2_TIMER_REG_OFFSET, ts_10us ) != MPXERR_NOERROR )
            return MPXERR_UNEXPECTED;

        // Set 'use Timer' in configuration register
        reg |= MPIX2_CONF_TIMER_USED;

        // To keep getBusy() working...
        _acqPars.useHwTimer = true;
        _acqPars.mode       = ACQMODE_ACQSTART_TIMERSTOP;
    }
    else
    {
        // Reset 'use Timer'
        reg &= ~MPIX2_CONF_TIMER_USED;
    }

    if ( ext_trig )
    {
        // Enable external trigger
        reg |= MPIX2_CONF_EXT_TRIG_ENABLE;

        // Trigger on rising edge
        reg &= ~MPIX2_CONF_EXT_TRIG_FALLING_EDGE;

        // Ready for next (external) trigger
        reg &= ~MPIX2_CONF_EXT_TRIG_INHIBIT;
    }
    else
    {
        // Disable external trigger
        reg &= ~MPIX2_CONF_EXT_TRIG_ENABLE;
    }

    int errval = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

    if ( reg & MPIX2_CONF_EXT_TRIG_ENABLE )
        _waitingForTrigger = true;
    else
        _waitingForTrigger = false;

    return errval;
}

void MpxModule::setFrsMxr(Fsr *fsr, 
                          int *dac_i, 
                          u8  sense_chip,
                          u8  ext_dac_chip, 
                          int codes[], 
                          u32 col_testpulse_reg)
{
    // DAC values for the current chip number
    fsr->fromDacSettings( _devInfo.mpxType, dac_i );

    // Column Test Pulse Register (CTPR)
    fsr->setBits( MXR_FSR_CTPR_I, MXR_FSR_CTPR_BITS, col_testpulse_reg );

    // From MXR manual page 27:
    // "Two special features can be done with the DACs, to sense out
    //  the value of each DAC and to use an external DAC to substitute
    //  any of the on-chip DACs. To perform this operation 6 bits
    //  from the FSR are used:
    //  DAC Code (B0-B3)              : bit 37(LSB), 38, 40 and 41(MSB)
    //  Sense DAC (Active High)       : bit 42
    //  External DAC_SEL (Active High): bit 43"
    if ( codes != 0 )
    {
        fsr->setBits( MXR_FSR_DACCODEB0B1_I, MXR_FSR_DACCODEB0B1_BITS, codes[0] & 0x3 );
        fsr->setBits( MXR_FSR_DACCODEB2B3_I, MXR_FSR_DACCODEB2B3_BITS, ( codes[0] & 0xC ) >> 2 );
    }

    fsr->setBits( MXR_FSR_SENSEDAC_I, MXR_FSR_SENSEDAC_BITS, sense_chip );
    fsr->setBits( MXR_FSR_EXTDACSELECT_I, MXR_FSR_EXTDACSELECT_BITS, ext_dac_chip );
}

void MpxModule::setFrsTpx(Fsr *fsr, 
                          int *dac_i, 
                          u8  sense_chip,
                          u8  ext_dac_chip, 
                          int codes[], 
                          u32 col_testpulse_reg,
                          int chipnr)
{
    // Set FSR to all 1-s
    fsr->setOnes();

    // Retry if necessary (added 30 Mar 2011, for AMOLF problem)
    if ( this->writeFsr( *fsr ) != MPXERR_NOERROR )
    {
        _log->error("writeFsr(1s) retry");
        this->selectChip( _chipMap[chipnr] );
        this->writeFsr( *fsr );
    }

    // DAC values for the current chip number
    fsr->fromDacSettings( _devInfo.mpxType, dac_i );

    // Column Test Pulse Register (CTPR)
    fsr->setBits( TPX_FSR_CTPR_I, TPX_FSR_CTPR_BITS, col_testpulse_reg );

    // From Timepix manual page 36:
    // "Two special features can be done with the DACs, to sense out
    //  the value of each DAC and to use an external DAC to substitute
    //  any of the on-chip DACs. To perform this operation 6 bits
    //  from the FSR are used:
    //  DAC Code (B0-B3)              : bit 37(LSB), 38, 40 and 41(MSB)
    //  Sense DAC (Active High)       : bit 42
    //  External DAC_SEL (Active High): bit 43"
    if ( codes != 0 )
    {
        fsr->setBits( TPX_FSR_DACCODEB0B1_I, TPX_FSR_DACCODEB0B1_BITS,
            codes[0] & 0x3 );
        fsr->setBits( TPX_FSR_DACCODEB2B3_I, TPX_FSR_DACCODEB2B3_BITS,
            ( codes[0] & 0xC ) >> 2 );
    }

    fsr->setBits( TPX_FSR_SENSEDAC_I, TPX_FSR_SENSEDAC_BITS, sense_chip );
    fsr->setBits( TPX_FSR_EXTDACSELECT_I, TPX_FSR_EXTDACSELECT_BITS,
        ext_dac_chip );
}

void MpxModule::logFsrChange(Fsr *fsr, int chipnr)
{
    if ( _logVerbose )
    {
        // Log it
        u8 *bytes = fsr->bytes();
        _log->scratch << "Upload FSR, chip " << chipnr << " (LSB first):" << std::hex;

        for (int i = 0; i < MPIX_FSR_BYTES; ++i )
        {
            _log->scratch << " " << std::setw( 2 ) << std::setfill( '0' ) << static_cast<u32> ( bytes[i] );
        }
        _log->scratch << std::dec;
        _log->logScratch(MPX_TRACE);
    }
}

void MpxModule::logChipIdWafer()
{
    if ( _logVerbose )
    {
        // Log the Chip-IDs found (and decode to Wafer number and X/Y position)
        const u32 NIBBLE_REVERSED[0x10] = { 0x0, 0x8, 0x4, 0xC,
                                            0x2, 0xA, 0x6, 0xE,
                                            0x1, 0x9, 0x5, 0xD,
                                            0x3, 0xB, 0x7, 0xF
                                          };
		_log->debug("ChipIDs returned:");

        for (int chipnr = 0; chipnr < _devInfo.numberOfChips; ++chipnr )
        {
            _log->scratch << "  "  << std::hex << std::setw( 6 ) << std::setfill( '0' ) << _chipId[_chipMap[chipnr]];

            if ( _chipId[_chipMap[chipnr]] != 0 )
            {
                // Chip ID contents:
                // - Wafer number in bits 0-11 (reversed bit order)
                // - X in bits 12-15 (reversed bit order)
                // - Y in bits 16-19 (reversed bit order)
                // - Bits 20-23 should be 0
                u32  id, nibble, wafer = 0, x, y, zero;
                bool timepix;
                id = _chipId[_chipMap[chipnr]];
                nibble = ( id & 0x00000F ) >> 0;
                wafer |= ( NIBBLE_REVERSED[nibble] << 8 );
                nibble = ( id & 0x0000F0 ) >> 4;
                wafer |= ( NIBBLE_REVERSED[nibble] << 4 );
                nibble = ( id & 0x000F00 ) >> 8;
                wafer |= ( NIBBLE_REVERSED[nibble] << 0 );

                if ( wafer & 0x800 )
                {
                    timepix = true;
                }
                else
                {
                    timepix = false;
                }

                wafer &= 0x7FF; // Remove Timepix-type bit from wafer number
                nibble = ( id & 0x00F000 ) >> 12;
                x      = NIBBLE_REVERSED[nibble];
                nibble = ( id & 0x0F0000 ) >> 16;
                y      = NIBBLE_REVERSED[nibble];
                nibble = ( id & 0xF00000 ) >> 20;
                zero   = NIBBLE_REVERSED[nibble];
				_log->scratch << std::dec << " (";

                if ( timepix )
                {
                    _log->scratch << "TPX ";
                }
                else
                {
                    _log->scratch << "MXR ";
                }
                _log->scratch << "wafer=" << wafer << ", x=" << x
                      << ", y=" << y << ", zero=" << zero
                      << " ==> Name: " << this->chipName( chipnr ) << ")";
            }

			_log->logScratch(MPX_DEBUG);
        }
    }
}

int MpxModule::saveFrsResults(Fsr *fsr, int chipnr)
{
    int errval = MPXERR_NOERROR;
    // Keep a copy of the DAC settings
    *_fsr[chipnr] = *fsr;
    _fsrValid[chipnr] = true;

    errval = this->writeFsr( *fsr );

    // Retry (added 30 Mar 2011, for AMOLF problem)
    if ( errval != MPXERR_NOERROR )
    {
        _log->error("writeFsr(fsr) retry");
        this->selectChip( _chipMap[chipnr] );
        errval = this->writeFsr( *fsr );
    }

    if ( errval == MPXERR_NOERROR )
    {
        // Extract the chip id from the returned FSR data (in bits 195-218)
        // (24-bits significant, but read out 32 bits
        //  to spot problems more easily)
        _chipId[_chipMap[chipnr]] = fsr->bits( 195, 32 );

        // In case of a TPX-type chip (MSB of wafer number is 1)
        // one extra bitshift is required to actually get the bits
        // in their proper position (as shown in the documentation);
        // note that the MSB of the wafer number is the LSB in the id
        if ( _chipId[_chipMap[chipnr]] & 0x2 )
        {
            _chipId[_chipMap[chipnr]] >>= 1;
        }
    }
    else
    {
        _chipId[_chipMap[chipnr]] = 0;
    }
    return errval;
}

// ----------------------------------------------------------------------------
// Setting MPX DACs
// [in] dac_vals     - array of DAC values (number of chips)*(number of DACs),
//                     order of DACs is given by enum type DACS_ORDER
// [in] size         - size of dacVals array (number of items for size check)
// [in] sense_chip   - 8bit mask for enabling/disabling preparation of
//                     ADC conversion of DAC selected in codes array
// [in] ext_dac_chip - 8bit mask for enabling/disabling usage of external DAC
//                     for DAC selected in codes array
// [in] codes        - is [number of chip] array which contains DAC codes for
//                     individual chips for ADC conversion/external DAC
//                     selection
// [in] tp_reg       - setting test pulse register in MXR, ignored for
//                     original chip
// ----------------------------------------------------------------------------

int MpxModule::setFsrs( u16 dac_vals[],
                        int size,
                        u8  sense_chip,
                        u8  ext_dac_chip,
                        int codes[],
                        u32 col_testpulse_reg )
{
    LOGCALL( "setFsrs()" );
    Fsr  fsr( MPIX_FSR_BITS );
    u16 *dac;
    int  i, dac_i[16], chipnr, errval;

    bool wasHighSpeed = highSpeedReadout();
    if (wasHighSpeed)
    {
        setHighSpeedReadout(false);
    }
    errval = MPXERR_NOERROR;

    for ( chipnr = 0; chipnr < _devInfo.numberOfChips; ++chipnr )
    {
        this->selectChip( _chipMap[chipnr] );

        dac = &dac_vals[chipnr * ( size / _devInfo.numberOfChips )];

        // Copy DAC settings to an int-array
        for ( i = 0; i < size / _devInfo.numberOfChips; ++i )
        {
            dac_i[i] = dac[i];
        }

        switch ( _devInfo.mpxType )
        {
        case MPX_MXR:
            setFrsMxr(&fsr, dac_i, sense_chip, ext_dac_chip, codes, col_testpulse_reg);
            break;

        case MPX_TPX:
            setFrsTpx(&fsr, dac_i, sense_chip, ext_dac_chip, codes, col_testpulse_reg, chipnr);
            break;

        default:
            break;
        }

        logFsrChange(&fsr, chipnr);
        errval = saveFrsResults(&fsr, chipnr);
    }
    if (wasHighSpeed)
    {
        setHighSpeedReadout(true);
    }
    logChipIdWafer();

    // Update the chip names string (is a 'hw info item')
    populateChipNames();

    return errval;
}

// ----------------------------------------------------------------------------

int MpxModule::setFsr( int chipnr, int *dac, u32 col_testpulse_reg )
{
    LOGCALL1( "setFsr()", chipnr );
    Fsr fsr( MPIX_FSR_BITS );
    int errval;

    errval = this->selectChip( _chipMap[chipnr] );

    if ( errval != MPXERR_NOERROR ) return errval;

    switch ( _devInfo.mpxType )
    {
    case MPX_MXR:
        // Fill FSR with DAC settings
        fsr.fromDacSettings( _devInfo.mpxType, dac );

        // Column Test Pulse Register (CTPR)
        fsr.setBits( MXR_FSR_CTPR_I, MXR_FSR_CTPR_BITS, col_testpulse_reg );
        break;

    case MPX_TPX:
        // Set FSR to all 1-s
        fsr.setOnes();

        // Retry if necessary (added 13 Apr 2011, for AMOLF problem)
        if ( this->writeFsr( fsr ) != MPXERR_NOERROR )
        {
            _log->error("writeFsr(1s) retry");
            this->selectChip( _chipMap[chipnr] );
            this->writeFsr( fsr );
        }

        // Fill FSR with DAC settings
        fsr.fromDacSettings( _devInfo.mpxType, dac );

        // Column Test Pulse Register (CTPR)
        fsr.setBits( TPX_FSR_CTPR_I, TPX_FSR_CTPR_BITS, col_testpulse_reg );
        break;

    default:
        break;
    }

    if ( _logVerbose )
    {
        // Log it
        u8 *bytes = fsr.bytes();
		_log->scratch << "Upload FSR, chip " << chipnr << " (LSB first):" << std::hex;

        for ( int i = 0; i < MPIX_FSR_BYTES; ++i )
            _log->scratch << " " << std::setw( 2 ) << std::setfill( '0' )
                  << static_cast<u32> ( bytes[i] );

        _log->scratch << std::dec;
		_log->logScratch(MPX_DEBUG);
    }

    // Keep a copy of the DAC settings
    *_fsr[chipnr] = fsr;
    _fsrValid[chipnr] = true;

    errval = this->writeFsr( fsr );

    // Retry (added 13 Apr 2011, for AMOLF problem)
    if ( errval != MPXERR_NOERROR )
    {
        _log->error("writeFsr(fsr) retry");
        this->selectChip( _chipMap[chipnr] );
        errval = this->writeFsr( fsr );
    }

    if ( errval == MPXERR_NOERROR )
    {
        // Extract the chip id from the returned FSR data (in bits 195-218)
        // (24-bits significant, but read out 32 bits
        //  to spot problems more easily)
        _chipId[_chipMap[chipnr]] = fsr.bits( 195, 32 );

        // In case of a TPX-type chip (MSB of wafer number is 1)
        // one extra bitshift is required to actually get the bits
        // in their proper position (as shown in the documentation);
        // note that the MSB of the wafer number is the LSB in the id
        if ( _chipId[_chipMap[chipnr]] & 0x2 ) _chipId[_chipMap[chipnr]] >>= 1;
    }
    else
    {
        _chipId[_chipMap[chipnr]] = 0;
    }

    if ( _logVerbose )
    {
        // Log the Chip-ID found
        _log->scratch << "ChipID returned: " << this->chipName( chipnr );
		_log->logScratch(MPX_DEBUG);
    }

    // Update the chip names string (is a 'hw info item')
    populateChipNames();
    return errval;
}

// ----------------------------------------------------------------------------

int MpxModule::refreshFsr( int chipnr )
{
    LOGCALL1( "refreshFsr()", chipnr );
    int errval;

    // Do we have DAC settings to refresh ?
    if ( _fsrValid[chipnr] == false )
    {
        // No, so read them from the Relaxd module itself
        errval = this->readFsr( _chipMap[chipnr], _fsr[chipnr]->bytes() );

        if ( errval != MPXERR_NOERROR ) return MPXERR_ATTRIB_NOTFOUND;

        _fsrValid[chipnr] = true;
    }

    errval = this->selectChip( _chipMap[chipnr] );

    if ( errval != MPXERR_NOERROR ) return errval;

    errval = this->writeFsr( *_fsr[chipnr] );

    // Retry if necessary
    if ( errval != MPXERR_NOERROR )
    {
        _log->error("writeFsr(fsr) retry");
        this->selectChip( _chipMap[chipnr] );
        errval = this->writeFsr( *_fsr[chipnr] );
    }

    return errval;
}

// ----------------------------------------------------------------------------
// Reading (sensing) analog value of DAC, DAC which is read is set by
// DAC code in codes array in HwSetMpxDACsType
// [in]  chipnr - chip that should be sensed
// [out] value   - sensed analog value
// ----------------------------------------------------------------------------

int MpxModule::getMpxDac( int chipnr, double *value )
{
    LOGCALL1( "getMpxDac()", chipnr );
    int errval;
    u32 adc_val = 0;

    int adc_i = _chipMap[chipnr];

    // ###HARDWARE ISSUE: chip 0 and 1 positions have to be swapped,
    //                    but the ADC does not change position
    if ( adc_i == 0 )
        adc_i = 1;
    else if ( adc_i == 1 )
        adc_i = 0;

    // Read the current ADC value
    *value = 0;
    errval = this->readAdc( adc_i, &adc_val );

    if ( errval != MPXERR_NOERROR ) return errval;

    // Convert to voltage
    *value = ( ( adc_val * ADC_ANALOG_MAX_VALUE ) /
               static_cast<double> ( ADC_DIGITAL_MAX_VALUE ) );

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------
// Set pixels mask
// [in] cfg - array [number of chips * MPIX_PIXELS] of pixel configuration bits
// ----------------------------------------------------------------------------

int MpxModule::setPixelsCfg( u8 *cfg, int chipnr )
{
    i16 smask[MPIX_PIXELS];
    u8  bytes[MPIX_FRAME_BYTES];
    u8 *pcfg = cfg;
    u32 reg;
    int errval;

    // Are we uploading pixel configurations for all devices
    // or one in particular?
    int chip_start, chip_end;

    if ( chipnr > -1 )
    {
        chip_start = chipnr;
        chip_end   = chipnr + 1;
    }
    else
    {
        chip_start = 0;
        chip_end   = _devInfo.numberOfChips;
    }

    // throttle down high-speed read out (if set)
    // setting pixel config may fail otherwise.
    bool wasHsRo = highSpeedReadout();
    setHighSpeedReadout(false);
    int clock = 0;
    if (_devInfo.mpxType == MPX_TPX) 
    {
        readReg( MPIX2_CONF_REG_OFFSET, &reg );
        // save clock

        clock = reg & MPIX2_CONF_TPX_CLOCK_MASK;
        // clear clock, set to 50 MHz
        reg &= ~MPIX2_CONF_TPX_CLOCK_MASK;
        reg |= MPIX2_CONF_TPX_CLOCK_50MHZ;
        writeReg( MPIX2_CONF_REG_OFFSET, reg );
    }
    

    // Upload the configuration to the devices
    for ( int chip = chip_start; chip < chip_end; ++chip )
    {
        // Remap the configuration bits according to the Medipix chip type
        if ( _devInfo.mpxType == MPX_MXR )
        {
            for ( int i = 0; i < MPIX_PIXELS; ++i )
            {
                smask[i] = this->mxrCfgToI16( pcfg[i] );
            }
        }
        else if ( _devInfo.mpxType == MPX_TPX )
        {
            for ( int i = 0; i < MPIX_PIXELS; ++i )
            {
                smask[i] = this->tpxCfgToI16( pcfg[i] );
            }
        }

        // Create the bit stream
        this->mask2Stream( smask, bytes );

        // Send it row-by-row
        errval = this->selectChip( _chipMap[chip] );

        if ( errval != MPXERR_NOERROR ) 
        {
            setHighSpeedReadout(wasHsRo);   // restore read-out clock
            return errval;
        }

        errval = writeDualRowPixelCfg(bytes);

        // Retry (added 31 Mar 2011, for AMOLF problem)
        if ( errval != MPXERR_NOERROR )
        {
            _log->error("selectChip()/writeRow() retry");
            this->flushSockInput( true );
            // Send it row-by-row
            errval = this->selectChip( _chipMap[chip] );
            writeDualRowPixelCfg(bytes);
        }

        // Configuration settings for the next chip
        pcfg += MPIX_PIXELS;
    }

    if (_devInfo.mpxType == MPX_TPX)
    {
        reg &= ~MPIX2_CONF_TPX_CLOCK_MASK;
        reg |= clock;

        writeReg( MPIX2_CONF_REG_OFFSET, reg );
    }
    setHighSpeedReadout(wasHsRo);   // restore read-out clock

    if ( _storePixelsCfg )
    {
        _storePixelsCfg = false;
    }

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::setPixelsCfg( const char* filename, int mode )
{
    LOGCALL( "setPixelsCfg(string filename, int mode)" );
    std::ifstream ifs;
    u8  *cfg = new u8[MPIX_PIXELS];

    ifs.open( filename, std::ios_base::binary | std::ios_base::in );

    if ( !ifs.is_open() )
    {
        delete[] cfg;
        return MPXERR_OPENCFG;
    }

    // Read the configuration from the file and upload to the devices
    for ( int chipnr = 0; chipnr < _devInfo.numberOfChips; ++chipnr )
    {
        ifs.read( ( char * ) cfg, MPIX_PIXELS );

        if ( ifs.fail() )
        {
            delete[] cfg;
            return MPXERR_READCFG;
        }

        if ( mode != TPX_MODE_DONT_SET && _devInfo.mpxType == MPX_TPX )
        {
            for ( int i = 0; i < MPIX_PIXELS; ++i )
            {
                // Replace/set the mode bits in each configuration byte
                cfg[i] &= ~TPX_CFG8_MODE_MASK;
                cfg[i] |= ( mode << TPX_CFG8_MODE_MASK_SHIFT );
            }
        }

        // ###DEBUG
        _log->scratch << "Cfg Bytes: ";

        for ( int i = 0; i < 16; ++i )
        {
            _log->scratch << std::hex << ( int ) cfg[i] << ' ';
        }
		_log->logScratch(MPX_TRACE);

        setPixelsCfg(cfg, chipnr);
    }

    delete[] cfg;
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::setPixelsCfg( int  mode, int  thresh1, int thresh2,
                             bool test, bool mask )
{
    LOGCALL( "setPixelsCfg(int mode, int thresh1, ...)" );
    u8  cfg;
    i16 cfg16 = 0;
    i16 smask[MPIX_PIXELS];
    u8  bytes[MPIX_FRAME_BYTES];
    int i;
    int errval = MPXERR_NOERROR;

    // Determine the configuration byte
    cfg = 0;

    if ( _devInfo.mpxType == MPX_MXR )
    {
        if ( !test )
        {
            cfg |= MXR_CFG8_TEST;
        }

        if ( !mask )
        {
            cfg |= MXR_CFG8_MASK;
        }

        thresh1 &= ( MXR_CFG8_LO_THR_MASK >> MXR_CFG8_LO_THR_MASK_SHIFT );
        cfg |= ( thresh1 << MXR_CFG8_LO_THR_MASK_SHIFT );

        thresh2 &= ( MXR_CFG8_HI_THR_MASK >> MXR_CFG8_HI_THR_MASK_SHIFT );
        cfg |= ( thresh2 << MXR_CFG8_HI_THR_MASK_SHIFT );

        // Remap the pixel configuration byte to a Medipix2 configuration word
        cfg16 = this->mxrCfgToI16( cfg );
    }
    else if ( _devInfo.mpxType == MPX_TPX )
    {
        mode &= ( TPX_CFG8_MODE_MASK >> TPX_CFG8_MODE_MASK_SHIFT );
        cfg |= ( mode << TPX_CFG8_MODE_MASK_SHIFT );

        if ( !test )
        {
            cfg |= TPX_CFG8_TEST;
        }

        if ( !mask )
        {
            cfg |= TPX_CFG8_MASK;
        }

        // Bits when high are active (see documentation),
        // so does a high thresh1 value represent a low threshold ?
        // If it is the opposite uncomment the following line
        // (inverting the bits)
        //thresh1 ^= (TPX_CFG8_THRADJ_MASK >> TPX_CFG8_THRADJ_MASK_SHIFT);
        thresh1 &= ( TPX_CFG8_THRADJ_MASK >> TPX_CFG8_THRADJ_MASK_SHIFT );
        cfg |= ( thresh1 << TPX_CFG8_THRADJ_MASK_SHIFT );

        // Remap the pixel configuration byte to a Timepix configuration word
        cfg16 = this->tpxCfgToI16( cfg );
    }

    // throttle down high-speed read out (if set)
    // setting pixel config may fail otherwise.
    bool wasHsRo = highSpeedReadout();
    setHighSpeedReadout(false); 

    // Create the bit stream
    for ( i = 0; i < MPIX_PIXELS; ++i )
    {
        smask[i] = cfg16;
    }

    this->mask2Stream( smask, bytes );

    // Upload the configuration to the devices
    for ( int chipnr = 0; chipnr < _devInfo.numberOfChips; ++chipnr )
    {
        // Send it row-by-row
        errval = this->selectChip( _chipMap[chipnr] );

        if ( errval != MPXERR_NOERROR ) 
        {
            setHighSpeedReadout(wasHsRo);   // restore read-out clock
            return errval;
        }

		errval = writeDualRowPixelCfg(bytes);
    }

    setHighSpeedReadout(wasHsRo);   // restore read-out clock

    return errval;
}

int MpxModule::writeDualRowPixelCfg(u8* pbytes)
{
    int errval = MPXERR_NOERROR;
    // Determine whether to store this configuration in Relaxd onboard EEPROM
    int cmd;
    
    if ( _storePixelsCfg )
    {
        LOGCALL( "writeDualRowPixelCfg(u8 *cfg) incl. onboard storage" );
        cmd = MPIXD_STORE_CONFIG;
    }
    else
    {
        LOGCALL( "writeDualRowPixelCfg(u8 *cfg)" );
        cmd = MPIXD_SET_CONFIG;
    }

    for ( int row = 0; row < MPIX_ROWS; row += 2 )
    {
        errval = this->writeRow( row, pbytes, MPIX_ROW_BYTES * MPIX_NUM_ROWS_PER_MSG_WRITE, cmd );

        if ( errval != MPXERR_NOERROR )
        {
            _log->warn("writeDualRowPixelCfg: Error in writeRow, retry.");
            errval = this->writeRow( row, pbytes, MPIX_ROW_BYTES * MPIX_NUM_ROWS_PER_MSG_WRITE, cmd );

            if ( errval != MPXERR_NOERROR )
            {
                _log->error("writeDualRowPixelCfg: writeRow retry unsuccessful.");
                break;
            }
            _log->warn("writeDualRowPixelCfg: writeRow retry successful.");
        }

        pbytes += MPIX_ROW_BYTES * MPIX_NUM_ROWS_PER_MSG_WRITE;
    }
    return errval;
}

// ----------------------------------------------------------------------------
// Write matrix to medipix (for each chip MPIX_PIXELS matrix)
// [in] data - matrix that should be written, size of buffer has to be
//             at least (MPIX_PIXELS*numberOfChips)
// [in] sz   - size of buff (number of items for size check)
// ----------------------------------------------------------------------------

int MpxModule::writeMatrix( i16 *data, u32 sz )
{
    LOGCALL( "writeMatrix()" );
    i16  smask[MPIX_PIXELS];
    u8   bytes[MPIX_FRAME_BYTES];
    i16 *pdata = data;
    int errval;
    const AsiDecodeWord_t *ilut = getInverseLookupTable();
    UNREFERENCED_PARAMETER(sz);

    this->flushSockInput();

    // throttle down high-speed read out (if set)
    // setting pixel config may fail otherwise.
    bool wasHsRo = highSpeedReadout();
    setHighSpeedReadout(false); 

    for ( int chipnr = 0; chipnr < _devInfo.numberOfChips; ++chipnr )
    {
        // Map the pixel data
        for ( u32 i = 0; i < MPIX_PIXELS; ++i ) 
        {
            smask[i] = ilut[pdata[i]];
        }
        // Create the bit/byte stream
        this->mask2Stream( smask, bytes );

        // Send it row-by-row
        u8 *pbytes = bytes;
        errval = this->selectChip( _chipMap[chipnr] );

        if ( errval != MPXERR_NOERROR )
        {
            setHighSpeedReadout(wasHsRo);   // restore read-out clock
            return errval;
        }

        for ( int row = 0; row < MPIX_ROWS; row += 2 )
        {
            errval = this->writeRow( row, pbytes, MPIX_ROW_BYTES * MPIX_NUM_ROWS_PER_MSG_WRITE );

            if ( errval != MPXERR_NOERROR ) {
                setHighSpeedReadout(wasHsRo);   // restore read-out clock
                return errval;
            }

            pbytes += MPIX_ROW_BYTES * MPIX_NUM_ROWS_PER_MSG_WRITE;
        }

        // Data for the next chip
        pdata += MPIX_PIXELS;
    }

    setHighSpeedReadout(wasHsRo);   // restore read-out clock

    return MPXERR_NOERROR;
}

int MpxModule::checkLostRows(int lost_rows)
{
    if (lost_rows != 0)
    {
        return MPXERR_FRAME_LOST_ROWS;
    }
    return MPXERR_NOERROR;
}

int MpxModule::toggleTimerUsedBit(const u32 configReg)
{
    int errval;
    u32 reg = configReg;
    // Ready for next trigger
    reg &= ~( MPIX2_CONF_BURST_READOUT | MPIX2_CONF_EXT_TRIG_INHIBIT );

    if ( reg & MPIX2_CONF_TIMER_USED )
    {
        // Toggle 'use timer' bit, or it won't restart
        reg &= ~MPIX2_CONF_TIMER_USED;
        this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
        reg |= MPIX2_CONF_TIMER_USED;
    }
    errval = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
    return errval;
}

// ----------------------------------------------------------------------------

int MpxModule::readMatrixRawSeq( u8 *bytes, u32 *sz, int *lost_rows )
{
    // Reads out the Medipix devices one-by-one
    LOGCALL1( "readMatrixRawSeq()", sz );
    u8 *pdata = bytes;
    int  errval, lost = 0;

    UNREFERENCED_PARAMETER(sz);

    ++_frameCnt;

    u32 reg;
    errval = this->readReg( MPIX2_CONF_REG_OFFSET, &reg );

    if ( errval != MPXERR_NOERROR )
    {
        return errval;
    }

#ifndef AUTO_BURST_EXT    // Autoburst makes this all redundant
    reg |= MPIX2_CONF_BURST_READOUT;
    reg &= ~MPIX2_CONF_MODE_MASK; // Reset mode bits, i.e. set READOUT mode
    errval = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

    if ( errval != MPXERR_NOERROR )
    {
        return errval;
    }
#endif
    *lost_rows = 0;

    for ( int chipnr = 0; chipnr < _devInfo.numberOfChips; ++chipnr )
    {
        for (int retry = 0; retry < 2; retry++)
        {
            // Initialize destination data array to 1-s (result is zero pixels)
            memset( static_cast<void *> ( pdata ), 0xFF, MPIX_FRAME_BYTES );

            this->selectChip( _chipMap[chipnr] );

            if ( errval != MPXERR_NOERROR )
            {
                return errval;
            }

            errval = this->burstReadout( pdata, chipnr, &lost, false );
            if ( errval != MPXERR_NOERROR )
            {
                // Retry (added 4 April 2011, for AMOLF problem)
                _log->error("burstReadout() retry");
                this->flushSockInput( true );
            }
            else
            {
                break;
            }
        }

        pdata += MPIX_FRAME_BYTES;

        *lost_rows += lost;

        if ( errval != MPXERR_NOERROR ) 
        {
            return errval;
        }
    }

    // Get thread ready for next event (in case of hardware trigger)
    _daqThreadHandler->readyForTrigger();

    errval = toggleTimerUsedBit(reg);
    return errval;
}

// ----------------------------------------------------------------------------

int MpxModule::readMatrixRawPar( u8 *bytes, u32 *sz, int *lost_rows )
{
    // Reads out the Medipix devices in-parallel and stores the data in bytes[].
    // Returns the number of bytes read/stored in *sz
    LOGCALL( "readMatrixRawPar()" );
    u8  *pbytes = bytes;
    int errval;
#ifndef AUTO_BURST_EXT    // Autoburst makes this all redundant
	bool resetMode = false;
#endif
    ++_frameCnt;

    // Return number of bytes written to 'bytes'
    *sz = _devInfo.numberOfChips * MPIX_FRAME_BYTES;

    // Initialize destination data array to 1-s (result is zero pixels)
    memset( static_cast<void *> ( pbytes ), 0xFF, MPIX_MAX_DEVS * MPIX_FRAME_BYTES );

    //this->setBurstReadout( true );
    //this->selectChipAll();
    // Above 2 calls replaced by what follows:
    // Set BURST_READOUT and CHIP_ALL bit in configuration register

    u32 reg;
    
    errval = this->readReg( MPIX2_CONF_REG_OFFSET, &reg );

    if ( errval != MPXERR_NOERROR )
    {
        return errval;
    }

#ifndef AUTO_BURST_EXT    // Autoburst makes this all redundant

	if ((reg & MPIX2_CONF_MODE_MASK) != 0)
    {
		resetMode = true;
    }
	
	reg |= MPIX2_CONF_BURST_READOUT | MPIX2_CONF_CHIP_ALL;
    reg &= ~MPIX2_CONF_MODE_MASK; // Reset mode bits, i.e. set READOUT mode


    // NB: write twice because the BURST_READOUT bit can only be written to
    //     when the mode has been set to READOUT first !
    //     (discovered: 18 Jan 2011, Henk B)

	// VvB 20120117: Added check for whether or not mode reset is required. Saves one setReg
	if (resetMode == true)
    {
		this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
    }
	errval = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

    if ( errval != MPXERR_NOERROR )
    {
        return errval;
    }
#endif
    errval = this->burstReadout( pbytes, -1, lost_rows, true );

    if ( errval != MPXERR_NOERROR )
    {
        return errval;
    }

#ifndef MPX_AUTO_ARM
    errval = toggleTimerUsedBit(reg);
#else

    if ( reg & MPIX2_CONF_TIMER_USED )
    {
      // Toggle 'use timer' bit, or it won't restart
        reg &= ~MPIX2_CONF_TIMER_USED;
        errval = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
        if ( errval != MPXERR_NOERROR )
        {
            return errval;
        }
        reg |= MPIX2_CONF_TIMER_USED;
        errval = this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
        if ( errval != MPXERR_NOERROR )
        {
            return errval;
        }
    }

#endif
    LOGCALL( "readMatrixRawPar() done" );
    return errval;
}

// ----------------------------------------------------------------------------

int MpxModule::readMatrixSeq( i16 *data, u32 sz )
{
    // Reads out the Medipix devices parallel
    LOGCALL1( "readMatrixSeq()", sz );
    u8  *pbytes = new u8[MPIX_MAX_DEVS * MPIX_FRAME_BYTES];
    int errval, lost_rows;
    u32 rawSz;

    UNREFERENCED_PARAMETER(sz);

    errval = this->readMatrixRawSeq(pbytes, &rawSz, &lost_rows);

    if (errval != MPXERR_NOERROR)
    {
        return errval;
    }

    // Unpack data bits
    this->seqStream2Data( _devInfo.numberOfChips, pbytes, data );
    delete[] pbytes;
    errval = checkLostRows(lost_rows);

    return errval;
}

// ----------------------------------------------------------------------------

// juhe - 28-09-20120 - function rewritten to use readMatrixRawPar instead of re-implementing it.
int MpxModule::readMatrixPar( i16 *data, u32 sz )
{
    // Reads out the Medipix devices parallel
    LOGCALL1( "readMatrixPar()", sz );
    u8  *pbytes = new u8[MPIX_MAX_DEVS * MPIX_FRAME_BYTES];
    int errval, lost_rows;
    u32 rawSz;

    UNREFERENCED_PARAMETER(sz);

    errval = this->readMatrixRawPar(pbytes, &rawSz, &lost_rows);

    if (errval != MPXERR_NOERROR)
    {
        return errval;
    }

    // Unpack data bits
    this->parStream2Data( pbytes, data );
    delete[] pbytes;
    errval = checkLostRows(lost_rows);

    return errval;
}

// ----------------------------------------------------------------------------
// readMatrix
//
// matrix readout (deserialized and converted from pseudo)
// [out] data - output buffer, size of buffer has to be at least
//              (MPIX_PIXELS*numberOfChips)
// [in]  sz   - size of buff (number of items for size check)
// ----------------------------------------------------------------------------

int MpxModule::readMatrix( i16 *data, u32 sz )
{
    if ( _parReadout && _devInfo.numberOfChips == MPIX_MAX_DEVS )
    {
        return this->readMatrixPar( data, sz );
    }
    else
    {
        return this->readMatrixSeq( data, sz );
    }
}

// ----------------------------------------------------------------------------

int MpxModule::readMatrixRaw( u8 *bytes, u32 *sz, int *lost_rows )
{
    int errval = MPXERR_NOERROR;
    // Reads out the Medipix devices one-by-one and stores the data in bytes[].
    // Returns the number of bytes read/stored in *sz
    if ( _parReadout && (_devInfo.numberOfChips == MPIX_MAX_DEVS) )
    {
        errval = this->readMatrixRawPar( bytes, sz, lost_rows );
    }
    else
    {
        errval = this->readMatrixRawSeq( bytes, sz, lost_rows );
    }
    if (errval == MPXERR_NOERROR)
    {
        errval = checkLostRows(*lost_rows);
    }
    return errval;
}

// ----------------------------------------------------------------------------
// resetMatrix
//
// The first readout after setting the matrix returns the matrix data.
// Therefore a reset Matrix command must be issued.
// ----------------------------------------------------------------------------

int MpxModule::resetMatrix()
{
  if (_rmNotRequired) {
    _rmNotRequired = false;
    return MPXERR_NOERROR;
  }
    LOGCALL( "resetMatrix()" );
//    u8 bytes[MPIX_MAX_DEVS * MPIX_FRAME_BYTES];
    u8 *pbytes = new u8[MPIX_MAX_DEVS * MPIX_FRAME_BYTES];
//    u8  *pbytes = new u8[MPIX_FRAME_BYTES];

    this->setBurstReadout( true );

    for ( int chipnr = 0; chipnr < _devInfo.numberOfChips; ++chipnr )
    {
        int errval, lost;
        errval = this->selectChip( _chipMap[chipnr] );

        if ( errval != MPXERR_NOERROR )
        {
        	delete[] pbytes;
        	return errval;
        }

        errval = this->burstReadout( pbytes, chipnr, &lost, false );

        if ( errval != MPXERR_NOERROR )
        {
            // Retry (added 4 April 2011, for AMOLF problem)
            _log->error("burstReadout() retry");
            this->flushSockInput( true );
            errval = this->selectChip( _chipMap[chipnr] );

            if ( errval != MPXERR_NOERROR )
        	{
        		delete[] pbytes;
        		return errval;
        	}
            errval = this->burstReadout( pbytes, chipnr, &lost, false );

            if ( errval != MPXERR_NOERROR )
        	{
        		delete[] pbytes;
        		return errval;
        	}
        }
    }
    delete[] pbytes;
    return MPXERR_NOERROR;
}


int MpxModule::resetFrameCounter()
{
    int r = writeReg( 0x4C, 0 );

    if ( r != MPXERR_NOERROR )
        return r;

    _lastFrameCount = 0;
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------
// Resets interface and medipix to default state
// ----------------------------------------------------------------------------

int MpxModule::reset()
{
    LOGCALL( "reset()" );
    return this->resetChips();
}

// ----------------------------------------------------------------------------
// Check if device is busy, based on elapsed time since start of acquisition
// [out] busy - set TRUE if busy
// ----------------------------------------------------------------------------

int MpxModule::getBusy( bool *busy )
{
    *busy = true;

    switch ( _acqPars.mode )
    {
    case ACQMODE_ACQSTART_TIMERSTOP:     // 0x0001
    case ACQMODE_HWTRIGSTART_TIMERSTOP:  // 0x0010
    case ACQMODE_SWTRIGSTART_TIMERSTOP:  // 0x0040
        if ( _acqPars.useHwTimer )
        {
            u32 reg = 0;

            if ( this->readReg( MPIX2_STATUS_REG_OFFSET, &reg ) == MPXERR_NOERROR )
            {
                if ( reg & MPIX2_STATUS_SHUTTER_READY )
                {
                    *busy = false; // Done
                }
            }

            //_fLog << "getBusy(): reg=0x" << hex << reg << dec << endl;
        }

        break;
// juhe - 13/11/2012 ... external trigger did not work in Pixelman and SoPhy in case the callback function was not working (Linux, Mac, SoPhy)
// CJD - 12/21/2012 I fixed the callback on linux, so this should not be necessary anymore.
//          Additionally it causes a blip on the scope when this code hits for some strange reason.
	case ACQMODE_HWTRIGSTART_HWTRIGSTOP:  // 0x0020
		//*busy=!this->newFrame(true, true);
        /*
        ** GETBUSY NEWFRAME CALLBACK HACK
        **
        ** Can the code get more ridiculous? 
        ** You cannot call newFrame if there is a callback installed because 
        ** then both the callback thread and newFrame will read the socket at the
        ** same time both looking for the same thing... only one of them will get it
        ** causing an infinite loop in the other one...
        ** SO if there is no callback then call newFrame here, otherwise
        ** just look at the _newFrame boolean
        ** amazing... 
        ** With this in mind, you have 3 options when using an external trigger:
        ** 1) Use the callback to get notification of shutter closed
        ** 2) Poll getBusy to get notification of shutter closed
        ** 3) Poll newFrame to get notification of shutter closed
        ** CJD.
        */
        if (_callback == NULL)
        {
            *busy=!this->newFrame(true);
        }
        else
        {
		    *busy = !_newFrame;
        }
		break;
// end of edit (juhe - 13/11/2012)
    default:
        //if( this->elapsedTime() > _acqPars.time ) *busy = false;  // Ready
        break;
    }

    LOGCALL1( "getBusy()", *busy );
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

bool MpxModule::timerExpired()
{
    u32 reg = 0;

    if ( this->readReg( MPIX2_STATUS_REG_OFFSET, &reg ) == MPXERR_NOERROR )
    {
        if ( reg & MPIX2_STATUS_SHUTTER_READY ) 
        {
            return true;
        }
    }

    return false;
}

// ----------------------------------------------------------------------------

int MpxModule::enableTimer( bool enable, int us )
{
    // Enable or disable the shutter timer, set in units of 10 microseconds
    LOGCALL2( "enableTimer()", enable, us );

    if ( us < 10 )
    {
        us = 10;
    }

    if ( this->writeReg( MPIX2_TIMER_REG_OFFSET, us / 10 ) != MPXERR_NOERROR )
    {
        return MPXERR_UNEXPECTED;
    }
    // Get current configuration register settings
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        return MPXERR_UNEXPECTED;
    }
    reg &= ~MPIX2_CONF_BURST_READOUT; // Reset 'burst readout' bit, just in case
    reg |= MPIX2_CONF_SHUTTER_CLOSED; // Close shutter, just in case

    // Set 'use Timer' bit in configuration register
    if ( enable )
    {
        reg |= MPIX2_CONF_TIMER_USED;
    }
    else
    {
        reg &= ~MPIX2_CONF_TIMER_USED;
    }

    if ( this->writeReg( MPIX2_CONF_REG_OFFSET, reg ) != MPXERR_NOERROR )
    {
        return MPXERR_UNEXPECTED;
    }
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::enableExtTrigger( bool enable )
{
    // Enable or disable the shutter external trigger
    LOGCALL1( "enableExtTrigger()", enable );

    // Get current configuration register settings
    u32 reg = 0;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    reg &= ~MPIX2_CONF_BURST_READOUT; // Reset 'burst readout' bit, just in case

    if ( enable )
    {
        // Enable external trigger
        reg |= MPIX2_CONF_EXT_TRIG_ENABLE;

        // Trigger on rising edge
        reg &= ~MPIX2_CONF_EXT_TRIG_FALLING_EDGE;

        // Ready for next (external) trigger
        reg &= ~MPIX2_CONF_EXT_TRIG_INHIBIT;
    }
    else
    {
        // Disable external trigger
        reg &= ~MPIX2_CONF_EXT_TRIG_ENABLE;

        // Ready for next (external) trigger
        reg &= ~MPIX2_CONF_EXT_TRIG_INHIBIT;
    }

    if ( this->writeReg( MPIX2_CONF_REG_OFFSET, reg ) != MPXERR_NOERROR )
    {
        return MPXERR_UNEXPECTED;
    }
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------
// generateTestPulses
//
// pulse_height -> pulseHeight in V
// period       -> period time (i.e. frequency) of the testpulse
// count        -> number of testpulses to generate
// ----------------------------------------------------------------------------

int MpxModule::generateTestPulses( double pulse_height,
                                   double period,
                                   u32    count )
{
    LOGCALL3( "generateTestPulses(); pulse_height, period, count:",
              pulse_height, period, count );

    // Determine the testpulse frequency in units of 0.5kHz
    if ( period == 0.0 ) return MPXERR_INVALID_PARVAL;

    double freq = 1.0 / period;
    u32 freq_05khz = static_cast<u32> ( ( freq * 2.0 + 500 ) / 1000.0 );

    if ( freq_05khz == 0 ) freq_05khz = 1;

    if ( freq_05khz > 20 ) freq_05khz = 20; // Maximum in Relaxd hardware

    // Configure the required frequency, pulse height and number of pulses
    // and enable the testpulse generator
    if ( pulse_height == 0.0 )
    {
        // Use pulse height according to previously set _testPulseLow and
        // _testPulseHigh values
        if ( this->configTestPulse( static_cast<u32> ( _testPulseLow ),
                                    static_cast<u32> ( _testPulseHigh ),
                                    freq_05khz, count ) != MPXERR_NOERROR )
            return MPXERR_UNEXPECTED;
    }
    else
    {
        // Pulse height in DAC counts
        double pulse_dac = ( ( pulse_height * DAC_DIGITAL_MAX_VALUE ) /
                             DAC_ANALOG_MAX_VALUE );

        if ( this->configTestPulse( 0, static_cast<u32> ( pulse_dac ),
                                    freq_05khz, count ) != MPXERR_NOERROR )
            return MPXERR_UNEXPECTED;
    }

    // Enable testpulse to Medipix devices
    if ( this->testPulseEnable( true ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Open shutter
    if ( this->openShutter() != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Wait for the shutter to close with time-out proportional
    // to the pulse period and requested number of pulses
    u32 timeout = 0;
    int period_us = ( ( 1000000 / ( 500 * freq_05khz ) + 5 ) / 10 ) * 10;

    while ( !this->timerExpired() && timeout < period_us * count ) ++timeout;

    if ( timeout >= period_us * count )
        //return MPXERR_TESTPULSES;
        return MPXERR_UNEXPECTED;

    // ### OLD: Wait for an amount of time to get the number of counts
    //_startTime = this->getTime();
    //while( this->elapsedTime() < static_cast<double> (period*count) );

    // Disable testpulse to Medipix to stop counting
    this->testPulseEnable( false );

    // Disable the testpulse generator
    this->configTestPulse( 0, 0, 0, 0 );

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

#if defined(_WINDLL) || defined(WIN32)
double MpxModule::getTime()
{
    static LARGE_INTEGER perfFrequency = {0};
    LARGE_INTEGER currentCount;

    if ( perfFrequency.QuadPart == 0 )
    {
        QueryPerformanceFrequency( &perfFrequency );
    }
    QueryPerformanceCounter( &currentCount );
    return currentCount.QuadPart / static_cast<double> ( perfFrequency.QuadPart );
}
#else // Linux
struct timeval MpxModule::getTime()
{
    struct timeval tval;
    gettimeofday( &tval, 0 );
    return tval;
}
#endif

// ----------------------------------------------------------------------------

double MpxModule::elapsedTime()
{
    // Return elapsed time in seconds
    //LOGCALL( "elapsedTime()" );
#if defined(_WINDLL) || defined(WIN32)
    return this->getTime() - _startTime;
#else // Linux
    struct timeval end = this->getTime();
    i32 secs  = end.tv_sec - _startTime.tv_sec;
    i32 usecs = end.tv_usec - _startTime.tv_usec;

    if ( usecs < 0 )
    {
        usecs += 1000000;
        --secs;
    }

    return( static_cast<double> ( secs ) +
            ( static_cast<double> ( usecs ) ) / 1000000.0 );
#endif
}

// ----------------------------------------------------------------------------

bool MpxModule::validHwInfoIndex( int index )
{
    //if( index < 0 || index >= (int) _hwInfoItems->size() )
    if ( index < 0 || index > static_cast<int> ( _hwInfoItems->size() ) )
    {
        std::ostringstream oss;
        oss  << "### Invalid index (" << index
            << ") of hw specific info item";
        _lastErrStr = oss.str();
        _log->error( _lastErrStr );
        return false;
    }

    return true;
}

// ----------------------------------------------------------------------------

int MpxModule::burstReadout( u8  *data,
                                int  chipnr,
                                int *lost_rows,
                                bool parallel,
                                bool send_cmd)
{
    LOGCALL( "burstReadout()" );
    int ret = MPXERR_NOERROR;
    int retries = _lostRowRetryCnt;
    do
    {
        (*lost_rows) = 0;
        if ( send_cmd )
        {
            ret = sendReadoutCmd(chipnr);
        }
        if (ret == MPXERR_NOERROR)
        {
            ret = getBurstReadoutData(data, lost_rows, parallel);
        }
    } while (((*lost_rows) != 0) && (retries-- > 0));
    return ret;
}

int MpxModule::getBurstReadoutData(u8  *data, int *lost_rows, bool parallel)
{
    // Receive data in a series of replies
    int ret = MPXERR_NOERROR;
    struct mpixd_reply_msg *mpxr = NULL;
    bool retval;
    u32 row_cnt;
    bool ack_recvd = false;
#define REPORT_BURST_ERROR
#ifdef REPORT_BURST_ERROR
    bool first_row_reported = false;
#else
    bool first_row_reported = true;
#endif // REPORT_BURST_ERROR

    row_cnt     = 0;
    std::vector<mpixd_type> v;
    v.push_back(MPIXD_READOUT);
    v.push_back(MPIXD_ACK);
    v.push_back(MPIXD_ERROR);

    boost::chrono::system_clock::time_point time_limit = boost::chrono::system_clock::now() + boost::chrono::milliseconds(20);

    while (ack_recvd == false)
    {
        retval = _sockMgr->recvData(&mpxr, v);
        if (retval == true)
        {
            ret = processBurstReadoutMsg(mpxr, data, &row_cnt, &ack_recvd, &first_row_reported);
            _sockMgr->releaseRxMsg(&mpxr);
        }
        else if (boost::chrono::system_clock::now() > time_limit)
        {
            // timeout
            //ret = MPXERR_COMM;
            std::ostringstream oss;
            oss  << "getBurstReadoutData timeout, row_cnt: " << row_cnt;
            _log->error(oss.str());
            break;
        }
        else
        {
            boost::this_thread::sleep_for(boost::chrono::milliseconds(1));
        }
    }

    reportBurstError(row_cnt, lost_rows, parallel);
    return ret;
}

int MpxModule::processBurstReadoutMsg(mpixd_reply_msg *mpxr, u8 *data, u32 *row_cnt, bool *ack_recvd, bool *first_row_reported)
{
    int ret = MPXERR_NOERROR;
    u32  header_type;

    header_type = ntohl( mpxr->header.type );

    switch ( header_type )
    {
    case MPIXD_ACK:
        *ack_recvd = true;
        //_fLog << "ACK recvd, row_cnt=" << row_cnt << endl;
        break;

    case MPIXD_READOUT:
        ret = processBurstReadoutRowBytesMsg(mpxr, data, row_cnt, first_row_reported);
        break;

    case MPIXD_ERROR:
    {
        _lastMpxErr = ntohl( mpxr->data.error );
        std::ostringstream oss;
        oss << "burstReadout() frame #" << _frameCnt
            << " row_cnt=" << row_cnt;
        this->mpxError( oss.str() );
    }

    default:
        ret = MPXERR_READMATRIX;
        break;
    }
    return ret;
}

int MpxModule::processBurstReadoutRowBytesMsg(mpixd_reply_msg *mpxr, u8 *data, u32 *row_cnt, bool *first_row_reported)
{
    int ret = MPXERR_NOERROR;
    u32 msg_length;
    msg_length  = ntohl( mpxr->header.length );

    switch ( msg_length )
    {
    case 4:
        break;

    case ( sizeof(mpxr->header) + MPIX_ROW_BYTES * MPIX_NUM_ROWS_PER_MSG_ORIG):
        processBurstReadoutRowBytesData(mpxr, data, row_cnt, first_row_reported, MPIX_NUM_ROWS_PER_MSG_ORIG);
        break;

    case ( sizeof(mpxr->header) + (MPIX_ROW_BYTES * MPIX_NUM_ROWS_PER_MSG_V2) ):
        processBurstReadoutRowBytesData(mpxr, data, row_cnt, first_row_reported, MPIX_NUM_ROWS_PER_MSG_V2);
        break;

    default:
        {
            std::ostringstream oss;
            oss<< "### burstReadout() unexpexted msg length=" << msg_length << " at row_cnt=" << row_cnt;
            _lastErrStr = oss.str();
            _log->error( _lastErrStr );
            ret = MPXERR_READMATRIX;
        }
    }
    return ret;
}

void MpxModule::processBurstReadoutRowBytesData(mpixd_reply_msg *mpxr, u8 *data, u32 *row_cnt, bool *first_row_reported, int multiplier)
{
    u32 row_nr;
    u32 max_rows;
    row_nr = ntohs( mpxr->data.row.number );

    //_fLog << "len=" << msg_length << ", row_nr=" << row_nr << endl;
    if ( row_nr != *row_cnt )
    {
        if ( (*first_row_reported) == false )
        {
            _log->scratch << "burstReadout() frame #" << _frameCnt << " row_cnt=" << *row_cnt
                << " row_nr=" << row_nr << " (single)" << std::endl;
            _log->logScratch(MPX_WARN);
            *first_row_reported = true;
        }
    }

    if ( *row_cnt == 0 ) 
    {	// only need to do this at first row
        _lastFrameCount = ntohs( mpxr->data.row.framecnt );
        _lastClockTick = ntohl( mpxr->data.row.timestamp );
    }
    max_rows = MPIX_ROWS;
    if ( _parReadout && _devInfo.numberOfChips == MPIX_MAX_DEVS )
    {
        max_rows *= MPIX_MAX_DEVS;
    }
    if ( row_nr < (max_rows / multiplier) )
    {
        u8 *pdata = data + (row_nr * MPIX_ROW_BYTES * multiplier);
        memcpy( static_cast<void *> ( pdata ),
            static_cast<void *> ( mpxr->data.row.bytes ),
            MPIX_ROW_BYTES * multiplier );
    }

    ++(*row_cnt);
}

void MpxModule::reportBurstError(u32 row_cnt, int *lost_rows, bool parallel)
{
    *lost_rows  = 0;
    u32 rows;
    if (parallel == true)
    {
        rows = MPIX_MAX_DEVS * MPIX_ROWS;
    }
    else
    {
        rows = MPIX_ROWS;
    }
    if ((row_cnt != (rows / 2))  && (row_cnt != rows))
    {
        if (row_cnt < (rows / 2))
        {
            *lost_rows = (rows / 2) - row_cnt; // assume 2 lines per packet
        }
        else
        {
            *lost_rows = rows - row_cnt; // assume 1 line per packet
        }
    }
#ifdef REPORT_BURST_ERROR
    if (*lost_rows != 0)
    {
        _log->scratch << "### burstReadout()"<<std::dec
              << " frame #" << _frameCnt
              << " rows " << row_cnt
              << " lost " << *lost_rows;

		_log->logScratch(MPX_DEBUG);
    }
#endif // REPORT_BURST_ERROR
}

// ----------------------------------------------------------------------------

int MpxModule::sendReadoutCmd(int chipnr)
{
    struct mpixd_msg mpxm;
    
    UNREFERENCED_PARAMETER(chipnr);

    // Construct message
    mpxm.header.type   = htonl( (u_long)MPIXD_READOUT );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
#ifdef AUTO_BURST_EXT
    if ( _parReadout && _devInfo.numberOfChips == MPIX_MAX_DEVS )
    {
        mpxm.data.number   = htonl( MPIXD_AB_ENABLE | MPIXD_AB_CHIP_ALL );
    }
    else
    {
        mpxm.data.number   = htonl( MPIXD_AB_ENABLE | ( _chipMap[chipnr] << MPIXD_AB_CHIPNR_SHIFT ) );
    }
#else
    mpxm.data.number   = htonl( 0 );
#endif
	
    if ( _sockMgr->sendData(( char * ) &mpxm,
                 sizeof( mpxm.header ) + sizeof( mpxm.data.number )) == -1 )
    {
        return MPXERR_COMM;
    }

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::readRow( u32 row, u8 *data )
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr = NULL;

    // Construct message
    mpxm.header.type   = htonl( (u_long)MPIXD_READOUT );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
    mpxm.data.number   = htonl( row );

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                        &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "readRow()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_READOUT:
        if ( ntohl( mpxr->data.row.number ) == row )
        {
            //for( int i=0; i<MPIX_ROW_BYTES; ++i )
            //data[i] = NibbleSwap( mpxr.data.row.bytes[i] );
            //data[i] = mpxr.data.row.bytes[i];
            memcpy( static_cast<void *> ( data ),
                    static_cast<void *> ( mpxr->data.row.bytes ),
                    MPIX_ROW_BYTES );
        }
        else
        {
            // number incorrect
            //fprintf(stderr, "Number not correct.\n");
        }

        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readRow()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_READMATRIX;

    default:
        this->replyError( "readRow()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::writeRow( int row, u8 *data, u32 sz, int cmd )
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    int len = sz + sizeof( mpxm.data.row ) - sizeof( mpxm.data.row.bytes );

    mpxm.header.type     = htonl( cmd ); // MPIXD_SET_MATRIX is the default
    mpxm.header.length   = htonl( len );
    mpxm.data.row.number = htons( (u_short) row );

    // Copy data into message
    for ( u32 i = 0; i < sz; ++i )
    {
        mpxm.data.row.bytes[i] = data[i];
    }

    //memcpy( static_cast<void *> (mpxr.data.row.bytes),
    //        static_cast<void *> (data), sz ); ###NOT OKAY

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + len,
                        &mpxr ) != MPXERR_NOERROR )
    {
        std::ostringstream oss;
        oss << "writeRow()/txRxMsg(), row=" << row;
        this->commError( oss.str() );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_SET_MATRIX:
    case MPIXD_SET_CONFIG:
    case MPIXD_STORE_CONFIG:
        break;

    case MPIXD_ERROR:
    {
        _lastMpxErr = ntohl( mpxr->data.error );
        std::ostringstream oss;
        oss << "writeRow(" << row << ") " << std::hex << cmd << std::dec;
        this->mpxError( oss.str() );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_SETMASK;
    }

    default:
    {
        std::ostringstream oss;
        oss << "writeRow(" << row << ")";
        this->replyError( oss.str(), ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    }
    _sockMgr->releaseRxMsg(&mpxr);
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::openShutter( bool open )
{
    LOGCALL1( "openShutter()", open );
    u32 reg = 0;
    // ###WERKT NIET VOOR TIMEPIX, UITZOEKEN WAAROM (SEE BELOW)
    //if( _devInfo.mpxType == MPX_MXR )
    {
        if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        {
            return MPXERR_UNEXPECTED;
        }
    }

    // ###ABOVE PROBLEM DUE TO BURST_READOUT BIT ?
    reg &= ~MPIX2_CONF_BURST_READOUT;

    if ( open )
    {
        reg &= ~MPIX2_CONF_SHUTTER_CLOSED;
    }
    else
    {
        reg |= MPIX2_CONF_SHUTTER_CLOSED;
    }

    return this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
}

// ----------------------------------------------------------------------------

int MpxModule::resetChips()
{
    LOGCALL( "resetChips()" );
    u32 reg;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Set reset bit to '1'
    reg |= MPIX2_CONF_RESET_MPIX;
    this->writeReg( MPIX2_CONF_REG_OFFSET, reg );

    // Set reset bit to '0'
    reg &= ~MPIX2_CONF_RESET_MPIX;

    return this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
}

// ----------------------------------------------------------------------------

int MpxModule::setBurstReadout( bool burst )
{
    LOGCALL1( "setBurstReadout()", burst );
    u32 reg;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    if ( burst )
    {
        reg &= ~MPIX2_CONF_MODE_MASK; // Reset mode bits
        reg |= MPIX2_CONF_BURST_READOUT;
    }
    else
    {
        reg &= ~MPIX2_CONF_BURST_READOUT;
    }

    this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
    // NB: write twice because the BURST_READOUT bit can only be written to
    //     when the mode has been set to READOUT first !
    //     (discovered: 18 Jan 2011, Henk B)
    return this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
}

// ----------------------------------------------------------------------------

int MpxModule::testPulseEnable( bool enable )
{
    LOGCALL1( "testPulseEnable()", enable );
    u32 reg;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Note that in case of test pulses the hardware timer is used
    // (set in configTestPulse()), so in case of disabling it gets disabled too
    if ( enable )
        reg |= MPIX2_CONF_ENABLE_TPULSE;
    else
        reg &= ~( MPIX2_CONF_ENABLE_TPULSE | MPIX2_CONF_TIMER_USED );

    return this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
}

// ----------------------------------------------------------------------------

int MpxModule::selectChip( int chipnr )
{
    LOGCALL1( "selectChip()", chipnr );

    // Pixelman does not call stopAcquisition when 'Abort' is pressed
    // so in case further actions are taken, the thread needs to be stopped first
    // (and further action mostly starts with a call to 'selectChip()'...)
    if ( _waitingForTrigger )
    {
        _daqThreadHandler->stopDaqThread();
        _waitingForTrigger = false;
    }

    u32 reg;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Reset/set Chip Selection bits
    reg &= ~MPIX2_CONF_CHIP_SEL_MASK;
    reg |= ( ( chipnr & 0x3 ) << MPIX2_CONF_CHIP_SEL_SHIFT );

    return this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
}

// ----------------------------------------------------------------------------

int MpxModule::selectChipAll()
{
    LOGCALL( "selectChipAll()" );
    u32 reg;

    if ( this->readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
        return MPXERR_UNEXPECTED;

    // Reset/set Chip Selection bits
    reg &= ~MPIX2_CONF_CHIP_SEL_MASK;
    reg |= MPIX2_CONF_CHIP_ALL;

    return this->writeReg( MPIX2_CONF_REG_OFFSET, reg );
}

// ----------------------------------------------------------------------------

int MpxModule::writeFsr( Fsr &fsr )
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_SET_DACS );
    mpxm.header.length = htonl( sizeof( mpxm.data.fsr ) );

    // Add data (Fsr bytes in reversed order, i.e. MSB first)
    u8 *byte = fsr.bytes();
    int len  = fsr.byteLength();

    for ( int i = 0; i < len; ++i ) mpxm.data.fsr.bytes[i] = byte[len - 1 - i];

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.fsr ),
                        &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "writeFsr()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_SET_DACS:

        // Bytes returned come MSB first; reverse order to get LSB first
        for ( int i = 0; i < MPIX_FSR_BYTES; ++i )
            fsr.setBits( i * 8, 8, static_cast<int>
                         ( mpxr->data.fsr.bytes[MPIX_FSR_BYTES - 1 - i] ) );

        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "writeFsr()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_SETDACS;

    default:
        this->replyError( "writeFsr()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::readFsr( int chipnr, u8 *fsr_bytes )
{
    LOGCALL1( "readFsr()", chipnr );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_GET_DACS );
    mpxm.header.length       = htonl( sizeof( mpxm.data.get_reg ) );
    mpxm.data.get_reg.offset = htonl( chipnr );

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.get_reg ),
                        &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "readFsr()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_GET_DACS:

        // FSR bytes are returned LSB first
        for ( int i = 0; i < MPIX_FSR_BYTES; ++i )
            fsr_bytes[i] = mpxr->data.fsr.bytes[i];

        if ( _logVerbose )
        {
            // Log it
            _log->scratch << "FSR chip " << chipnr << " (LSB first):" << std::hex;

            for ( int i = 0; i < MPIX_FSR_BYTES; ++i )
                _log->scratch << " " << std::setw( 2 ) << std::setfill( '0' )
                      << static_cast<u32> ( fsr_bytes[i] );

            _log->scratch << std::dec;
			_log->logScratch(MPX_DEBUG);
        }

        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readFsr()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "readFsr()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::readFsr( int chipnr, int *dacs, int *sz )
{
    LOGCALL1( "readFsr(int *dac)", chipnr );
    int errval;

    Fsr fsr( MPIX_FSR_BITS );
    errval = this->readFsr( _chipMap[chipnr], fsr.bytes() );

    if ( errval != MPXERR_NOERROR ) return MPXERR_ATTRIB_NOTFOUND;

    if ( fsr.toDacSettings( _devInfo.mpxType, dacs, sz ) == false )
        return MPXERR_UNEXPECTED;

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::readChipId( int chipnr, u32 *id )
{
    LOGCALL1( "readChipId()", chipnr );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_GET_CHIPID );
    mpxm.header.length       = htonl( sizeof( mpxm.data.get_reg ) );
    mpxm.data.get_reg.offset = htonl( _chipMap[chipnr] );

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.get_reg ),
                        &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "readChipId()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_GET_CHIPID:
        *id = ntohl( mpxr->data.value );
        _chipId[_chipMap[chipnr]] = *id;
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readChipId()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "readChipId()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::readTsensor( int t_nr, u32 *degrees )
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_GET_TEMPERATURE );
    mpxm.header.length       = htonl( sizeof( mpxm.data.get_reg ) );
    mpxm.data.get_reg.offset = htonl( t_nr );

    // ###TEST: request ADC via _sock, then request Temp via _sockMon,
    //          then read Temp first
    //mpxm.header.type         = htonl( MPIXD_GET_ADC );
    //this->txMsg( &mpxm, sizeof(mpxm.header) + sizeof(mpxm.data.get_reg) );
    //mpxm.header.type         = htonl( MPIXD_GET_TEMPERATURE );

    // Talk to module
    if ( this->txRxMsgMon( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.get_reg ),
                           &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "readTsensor()/txRxMsgMon()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_GET_TEMPERATURE:
        *degrees = ntohl( mpxr->data.value );
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readTsensor()" );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "readTsensor()", ntohl( mpxr->header.type ) );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }

    // ###TEST: request ADC via _sock, then request Temp via _sockMon,
    //          then read Temp first followed by ADC statement below
    //this->rxMsg( &mpxr, true );
    _sockMonMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::readAdc( int adc_nr, u32 *value )
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_GET_ADC );
    mpxm.header.length       = htonl( sizeof( mpxm.data.get_reg ) );
    mpxm.data.get_reg.offset = htonl( adc_nr );

    // Talk to module
    if ( this->txRxMsgMon( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.get_reg ),
                           &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "readAdc()/txRxMsgMon()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_GET_ADC:
        *value = ntohl( mpxr->data.value );
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readAdc()" );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "readAdc()", ntohl( mpxr->header.type ) );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMonMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::writeDac( int dac_nr, u32 value )
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_SET_DACONVERTER );
    mpxm.header.length       = htonl( sizeof( mpxm.data.set_reg ) );
    mpxm.data.set_reg.offset = htonl( dac_nr );
    mpxm.data.set_reg.value  = htonl( value );

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.set_reg ),
                        &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "writeDac()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_SET_DACONVERTER:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "writeDac()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "writeDac()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------


int MpxModule::getDriverVersion( )
{
    return _version;
}

// ----------------------------------------------------------------------------

int MpxModule::getFirmwareVersion( u32 *version, u8 *boardrev )
{
    LOGCALL( "getFirmwareVersion()" );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_GET_SWVERSION );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
    mpxm.data.number   = htonl( 0 );

    // Talk to module
    int retval = this->txRxMsgMon( &mpxm,
                                   sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                                   &mpxr );

    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "readSwVersion()/txRxMsgMon()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_GET_SWVERSION:
        *version = ntohl( mpxr->data.value );
		if (boardrev != NULL) {
			*boardrev = 0xff & (*version >> 24);
		}
		*version = 0xffffff & *version;

        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readSwVersion()" );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "readSwVersion()", ntohl( mpxr->header.type ) );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMonMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::configTestPulse( u32 low, u32 high, u32 freq_05khz, u32 count )
{
    LOGCALL4( "configTestPulse(); low, high, freq, cnt:",
              low, high, freq_05khz, count );

    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_SET_TESTPULSE );
    mpxm.header.length       = htonl( sizeof( mpxm.data.tpulse ) );
    mpxm.data.tpulse.low     = htonl( low );
    mpxm.data.tpulse.high    = htonl( high );
    mpxm.data.tpulse.khzfreq = htonl( freq_05khz );
    mpxm.data.tpulse.count   = htonl( count );    // Not used by Relaxd module

    // Talk to module
    if ( this->txRxMsgMon( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.tpulse ),
                           &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "configTestPulse()/txRxMsgMon()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_SET_TESTPULSE:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "configTestPulse()" );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "configTestPulse()", ntohl( mpxr->header.type ) );
        _sockMonMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }

    // Also configure the hardware timer
    if ( count > 0 )
    {
        // Set the hardware timer to match the pulse frequency
        // and the requested number of pulses;
        // calculate the pulse period, rounded to the next 10us
        int period_us = ( ( 1000000 / ( 500 * freq_05khz ) + 5 ) / 10 ) * 10;

        if ( this->enableTimer( true, period_us * count ) != MPXERR_NOERROR )
            return MPXERR_UNEXPECTED;
    }
    else
    {
        // Disable the Relaxd timer mode
        this->enableTimer( false );
    }
    _sockMonMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}


// ----------------------------------------------------------------------------

int MpxModule::writeReg( u32 offset, u32 value )
{
    struct mpixd_msg       mpxm;

#ifdef WRITEREG_WITH_REPLY
    struct mpixd_reply_msg mpxr;
#endif
    if (offset == MPIX2_CONF_REG_OFFSET)
    {
        // Hack alert: In order to reduce the number of messages sent between burst readout
        // when using external trigger, if the conf reg is being written, and it hasn't changed
        // then just skip the write...
        // This is only enabled when acqPars.mode is hw trigger start and stop...
        // This is needed to ensure 120 fps on real time linux when using 8.3ms external trigger
        // CJD.
        if ((_confRegValid == true) && (_confReg == value) && (_acqPars.mode == ACQMODE_HWTRIGSTART_HWTRIGSTOP))
        {
            //return MPXERR_NOERROR;
        }
        _confReg = value;
        _confRegValid = true;
    }
    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_SET_REG );
    mpxm.header.length       = htonl( sizeof( mpxm.data.set_reg ) );
    mpxm.data.set_reg.offset = htonl( offset );
    mpxm.data.set_reg.value  = htonl( value );

    // Talk to module
    if ( this->txMsg( _sockMgr, &mpxm, sizeof( mpxm.header ) +
                      sizeof( mpxm.data.set_reg ) ) != MPXERR_NOERROR )
    {
        int err = errno;
        std::ostringstream oss;
        oss  << "### writeReg() txMsg(): ";
        std::string errStr = std::string( strerror( err ) );
        if ( errStr  == std::string( "No error" ) )
        {
            oss << "timeout";
        }
        else
        {
            oss << errStr;
        }

        _lastErrStr = oss.str();
        _log->error( _lastErrStr );
        return MPXERR_UNEXPECTED;
    }
#ifdef DEBUG_LOG
    if (_logVerbose)
    {
        std::ostringstream oss;
        oss<<"writeReg offset: 0x"<<std::hex<< offset << " value: 0x"<<value<<std::endl;
        _log->debug(oss.str());
        }
#endif
#ifdef WRITEREG_WITH_REPLY

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.set_reg ),
                        &mpxr ) != MPXERR_NOERROR )
    {
        this->commError( "writeReg()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr.header.type ) )
    {
    case MPIXD_SET_REG:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr.data.error );
        this->mpxError( "writeReg()" );
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "writeReg()", ntohl( mpxr.header.type ) );
        return MPXERR_UNEXPECTED;
    }

#endif // WRITEREG_WITH_REPLY
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::readReg( u32 offset, u32 *value )
{
    int errval;
    boost::lock_guard<boost::recursive_mutex> lock(_pimpl->_regReadMutex);
    if (offset == MPIX2_CONF_REG_OFFSET)
    {
        // Hack alert: In order to reduce the number of messages sent between burst readout
        // when using external trigger, if the conf reg is being read, and it hasn't changed
        // then just skip the read...
        // This is only enabled when acqPars.mode is hw trigger start and stop...
        // This is needed to ensure 120 fps on real time linux when using 8.3ms external trigger
        // CJD.
        if ((_confRegValid == true) && (_acqPars.mode == ACQMODE_HWTRIGSTART_HWTRIGSTOP))
        {
            *value = _confReg;
            //return MPXERR_NOERROR;
        }
    }
    errval = readRegHelper(offset, value);
    if (errval != MPXERR_NOERROR)
    {
        _log->error("readReg() retry");

        this->flushSockInput( true );
        errval = readRegHelper(offset, value);
    }
#ifdef DEBUG_LOG
    if ((_logVerbose) && (errval == MPXERR_NOERROR))
    {
        std::ostringstream oss;
        oss<<"readReg  offset: 0x"<<std::hex<< offset << " value: 0x"<<*value<<std::endl;
        _log->debug(oss.str());
    }
#endif
    if ((offset == MPIX2_CONF_REG_OFFSET) && (errval == MPXERR_NOERROR))
    {
        _confReg = *value;
        _confRegValid = true;
    }
    return errval;
}

int MpxModule::readRegHelper( u32 offset, u32 *value)
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type         = htonl( (u_long) MPIXD_GET_REG );
    mpxm.header.length       = htonl( sizeof( mpxm.data.get_reg ) );
    mpxm.data.get_reg.offset = htonl( offset );

    // Talk to module
    if ( this->txRxMsg( &mpxm, sizeof( mpxm.header ) + sizeof( mpxm.data.get_reg ),
        &mpxr ) != MPXERR_NOERROR )
    {
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_GET_REG:
        *value = ntohl( mpxr->data.value );
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readReg()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::rmwReg( u32 offset, u32 value, u32 mask )
{
    u32 reg;
    u32 rv = this->readReg( offset, &reg );

    if ( rv == MPXERR_NOERROR ) {
        reg = ( reg & ~mask ) | ( value & mask );
        rv = this->writeReg( offset, reg );
    }

    return rv;
}

// ----------------------------------------------------------------------------

int MpxModule::storeId( u32 id )
{
    LOGCALL1( "storeId()", id );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    /* The store ID operation takes a long time! */
    this->setSockTimeout( false, 2000 );

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_STORE_ID );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
    mpxm.data.number   = htonl( id );

    // Talk to module
    int retval = this->txRxMsg( &mpxm,
                                sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                                &mpxr );

    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "storeId()/txRxMsg()" );
        return MPXERR_COMM;
    }
    this->setSockTimeout( false, SOCKET_RCVTIMEO_MS );

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_STORE_ID:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "storeId()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "storeId()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::storeDacs()
{
    LOGCALL( "storeDacs()" );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_STORE_DACS );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
    mpxm.data.number   = htonl( 0 );

    // Talk to module
    int retval = this->txRxMsg( &mpxm,
                                sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                                &mpxr );

    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "storeDacs()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_STORE_DACS:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "storeDacs()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "storeDacs()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::eraseDacs()
{
    LOGCALL( "eraseDacs()" );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_ERASE_DACS );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
    mpxm.data.number   = htonl( 0 );

    // Talk to module
    int retval = this->txRxMsg( &mpxm,
                                sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                                &mpxr );

    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "eraseDacs()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_ERASE_DACS:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "eraseDacs()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "eraseDacs()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::erasePixelsCfg()
{
    LOGCALL( "erasePixelsCfg()" );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_ERASE_CONFIG );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
    mpxm.data.number   = htonl( 0 );

    // Talk to module
    int retval = this->txRxMsg( &mpxm,
                                sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                                &mpxr );
    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "erasePixelsCfg()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_ERASE_CONFIG:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "erasePixelsCfg()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "erasePixelsCfg()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::loadConfig()
{
    LOGCALL( "loadConfig()" );
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_LOAD_CONFIG );
    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );
    mpxm.data.number   = htonl( 0 );

    // Talk to module
    // This command takes time...
    // juhe - 08/10/2012 timeout increased 10x beacuse 2s were not enough to load settings to quad
    // causing detector init() to fail.
    this->setSockTimeout( false, 20000 );
    int retval = this->txRxMsg( &mpxm,
                                sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                                &mpxr );
    this->setSockTimeout( false, SOCKET_RCVTIMEO_MS );

    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "loadConfig()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_LOAD_CONFIG:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "loadConfig()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "loadConfig()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

bool MpxModule::ping()
{
    LOGCALL( "ping()" );

    // Are we using a valid local address ? If not, don't even bother...
    // (we do this to speed up the pinging process, otherwise
    //  the time-outs seem to take a long time!)

    if ( _sockMonMgr->bindSocket(( _ipAddress & 0xFFFFFF00 ) | 1, _portNumber + 1) != 0 )
    {
#if defined(_WINDLL) || defined(WIN32)

        if ( WSAGetLastError() == WSAEADDRNOTAVAIL ) return false;

#else // Linux
        int err = errno;

        if ( err == EADDRNOTAVAIL ) return false;

#endif
    }

    struct mpixd_msg       mpxm;

    struct mpixd_reply_msg *mpxr;

    // Construct message
    mpxm.header.type   = htonl( (u_long) MPIXD_PING );

    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );

    mpxm.data.number   = htonl( 0 );

    // Talk to module
    int retval = this->txMsg( _sockMonMgr, &mpxm,
        sizeof( mpxm.header ) + sizeof( mpxm.data.number ) );

    if ( retval == MPXERR_NOERROR )
    {
        std::vector<mpixd_type> msgs;
        msgs.push_back(MPIXD_PING);
        msgs.push_back(MPIXD_ERROR);
        retval = this->rxMsgMon(&mpxr, msgs);

        if ( retval == MPXERR_NOERROR )
        {
            switch ( ntohl( mpxr->header.type ) )
            {
            case MPIXD_PING:
                break;

            case MPIXD_ERROR:
                _lastMpxErr = ntohl( mpxr->data.error );
                this->mpxError( "ping()" );
                retval = MPXERR_UNEXPECTED;
                break;

            default:
                this->replyError( "ping()", ntohl( mpxr->header.type ) );
                retval = MPXERR_UNEXPECTED;
                break;
            }

            _sockMonMgr->releaseRxMsg(&mpxr);
            //_fLog << "ping successful" << endl;
        }
        else
        {
            _log->debug("ping reply not received");
        }
    }

    return( retval == MPXERR_NOERROR );
}

int MpxModule::readFirmware(int page, u8 * bytes)
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
	mpxm.header.type   = htonl( (u_long) MPIXD_READ_FIRMWARE );

    mpxm.header.length = htonl( sizeof( mpxm.data.number ) );

	mpxm.data.number = htonl(page);
	
    int retval = this->txRxMsg( &mpxm,
                                sizeof( mpxm.header ) + sizeof( mpxm.data.number ),
                                &mpxr );

    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "readFirmware()/txRxMsg()" );
        return MPXERR_COMM;
    }
    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_READ_FIRMWARE:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readFirmware()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "readFirmware()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }

	memcpy(bytes, mpxr->data.flash.bytes, 256);
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}

int MpxModule::writeFirmware(int page, u8 * bytes)
{
    struct mpixd_msg       mpxm;
    struct mpixd_reply_msg *mpxr;

    // Construct message
	mpxm.header.type   = htonl( (u_long) MPIXD_WRITE_FIRMWARE );

    mpxm.header.length = htonl( sizeof( mpxm.data.flash ) );

	mpxm.data.flash.pagenr = htons( (u_short) page);
	memcpy(mpxm.data.flash.bytes, bytes, 256);
	
    int retval = this->txRxMsg( &mpxm,
                                sizeof( mpxm.header ) + sizeof( mpxm.data.flash ),
                                &mpxr );

    if ( retval != MPXERR_NOERROR )
    {
        this->commError( "readFirmware()/txRxMsg()" );
        return MPXERR_COMM;
    }

    switch ( ntohl( mpxr->header.type ) )
    {
    case MPIXD_WRITE_FIRMWARE:
        break;

    case MPIXD_ERROR:
        _lastMpxErr = ntohl( mpxr->data.error );
        this->mpxError( "readFirmware()" );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;

    default:
        this->replyError( "readFirmware()", ntohl( mpxr->header.type ) );
        _sockMgr->releaseRxMsg(&mpxr);
        return MPXERR_UNEXPECTED;
    }

	memcpy(bytes, mpxr->data.flash.bytes, 256);
    _sockMgr->releaseRxMsg(&mpxr);

    return MPXERR_NOERROR;
}



// ----------------------------------------------------------------------------

int MpxModule::openSock()
{
    if (_sockMgr == NULL)
    {
	    _sockMgr = new MpxSocketMgrRt(_log);
    }
    //juhe - 05/04/2012
    int ret = MPXERR_NOERROR;

    // Open a datagram socket
    if ( _sockMgr->initSocket() == -1 )
    {
        return MPXERR_UNEXPECTED; // Could not create the socket
    }

    //int bufsz = _devInfo.numberOfChips * MPIX_PIXELS * 8;
    int bufsz = MPIX_MAX_DEVS * MPIX_PIXELS * 8;
	
    if ( _sockMgr->setSockOptions(SO_RCVBUF,
                     reinterpret_cast<char *> ( &bufsz ), sizeof( int )) == 0 )
    {
        socklen_t len = 4;
		_sockMgr->getSockOptions(SO_RCVBUF, reinterpret_cast<char *> ( &bufsz ), (int*)&len );
    }

    // Bind(): is this the way to receive *only* messages
    // on the (local) IP-address/port pair used below?

	;
    if ( _sockMgr->bindSocket(( _ipAddress & 0xFFFFFF00 ) | 1, _portNumber) != 0 )
    {
		//juhe - 05/04/2012
		//        ret = MPXERR_UNEXPECTED; // bind failed
		// this seems never actually work??
    }

    // Connect(): is this the way?
	if (_sockMgr->connectToSocket(_ipAddress, _portNumber) != 0)
    {
        //juhe - 05/04/2012
        ret = MPXERR_UNEXPECTED; // connect failed
    }

    this->setSockTimeout( false, SOCKET_RCVTIMEO_MS );
    this->flushSockInput( true );

    //juhe - 05/04/2012
    return ret; // Success ?
    
}

// ----------------------------------------------------------------------------

int MpxModule::openSockMon()
{
    // Open a datagram socket
    if (_sockMonMgr == NULL)
    {
	    _sockMonMgr = new MpxSocketMgr(_log);
    }

    if ( _sockMonMgr->initSocket() == -1 )
    {
        return MPXERR_UNEXPECTED; // Could not create the socket
    }

    int bufsz = 0;
    socklen_t len = 4;

    _sockMonMgr->getSockOptions(SO_RCVBUF, reinterpret_cast<char *> ( &bufsz ), (int*)&len );

    // Bind(): is this the way to receive *only* messages
    // on the (local) IP-address/port pair used below?

    _sockMonMgr->bindSocket(( _ipAddress & 0xFFFFFF00 ) | 1, _portNumber + 1);
    _sockMonMgr->connectToSocket(_ipAddress, _portNumber + 1);
    
    return MPXERR_NOERROR; // Success
}

// ----------------------------------------------------------------------------

void MpxModule::initSocket()
{
    // juhe - 05/04/2012 error check added  
    int err = this->openSock();

    if (err != MPXERR_NOERROR) 
    {
        setLastError( "Failed to fully open socket" );
        this->_lastMpxErr=err;
    }
    // juhe - 05/04/2012 error check added    
    err = this->openSockMon();
    if (err != MPXERR_NOERROR) 
    {
        setLastError( "Failed to fully open monitor socket" );
        this->_lastMpxErr=err;
    }
    _daqThreadHandler = new MpxDaq(this);
}

// ----------------------------------------------------------------------------

void MpxModule::closeSock()
{
    if (_daqThreadHandler != NULL)
    {
        delete (_daqThreadHandler);
    }
    if (_sockMgr != NULL)
	{
		delete (_sockMgr);
	}
    if (_sockMonMgr != NULL)
	{
		delete (_sockMonMgr);
	}
}

// ----------------------------------------------------------------------------

void MpxModule::setSockTimeout( bool forever, int ms )
{
    _sockMgr->setSockTimeout(forever, ms);
}

// ----------------------------------------------------------------------------

void MpxModule::flushSockInput( bool long_timeout )
{
    UNREFERENCED_PARAMETER(long_timeout);
    //_sockMgr->clearRxQueue();
    return ;
}

// -----------------------------------------------------------------------------

/*
    Checks whether or not a reply contains the trigger message, and if so, set
    triggered to True.
 */
inline bool MpxModule::checkForFrame(mpixd_reply_msg * mpxr)
{
    // incorrectly this message is called 'trigger', it should be shutter closed
    // or frame available, or something like that.
    if (ntohl( mpxr->header.type ) ==  MPIXD_TRIGGER)
    {
        _newFrame = true;
        return true;
    }
    return false;
}

// -----------------------------------------------------------------------------

bool MpxModule::newFrame(bool check)
{
    if (_newFrame == false) 
	{
        if (check) 
		{
            struct mpixd_reply_msg *rx_msg = NULL;
            bool retval;

            while (_sockMgr->msgAvailable(MPIXD_TRIGGER))
            {
				retval = _sockMgr->recvData(&rx_msg, MPIXD_TRIGGER);
                if ( retval == false )
				{
					break;
				}
                checkForFrame(rx_msg);
                _sockMgr->releaseRxMsg(&rx_msg);
            }

            if (!_newFrame)
			{
                return false;
            }
        }
		else 
		{
            return false;
        }
    }
    return true;
}

// ----------------------------------------------------------------------------

int MpxModule::txRxMsg(MpxSocketMgr           *sockMgr,
                       struct mpixd_msg       *tx_msg,
                       int                     tx_len,
                       struct mpixd_reply_msg **rx_msg)
{
    std::vector<mpixd_type> msgs;
    u32 msgId = ntohl(tx_msg->header.type);
    msgs.push_back((mpixd_type)msgId);
    msgs.push_back(MPIXD_ERROR);

    // Transmit message (command)
    if ( sockMgr->sendData(reinterpret_cast<char *> ( tx_msg ), tx_len) == -1 )
    {
        return MPXERR_COMM;
    }

    // Receive reply
    bool retval;

    retval = sockMgr->recvData(rx_msg, msgs);

    if ( retval == false )
    {
        return MPXERR_COMM;
    }

    return MPXERR_NOERROR;
}

int MpxModule::txRxMsg(struct mpixd_msg       *tx_msg,
                       int                     tx_len,
                       struct mpixd_reply_msg **rx_msg)
{
    return txRxMsg(_sockMgr, tx_msg, tx_len, rx_msg);
}

int MpxModule::txRxMsgMon(struct mpixd_msg       *tx_msg,
                       int                     tx_len,
                       struct mpixd_reply_msg **rx_msg)
{
    return txRxMsg(_sockMonMgr, tx_msg, tx_len, rx_msg);
}

// ----------------------------------------------------------------------------

int MpxModule::txMsg(MpxSocketMgr* sockMgr, struct mpixd_msg *tx_msg,
                      int               tx_len )
{
    // Transmit message (command)

    if ( sockMgr->sendData(reinterpret_cast<char *> ( tx_msg ), tx_len) == -1 )
    {
        return MPXERR_COMM;
    }
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::rxMsg(struct mpixd_reply_msg **rx_msg, const std::vector<mpixd_type> &msgs)
{
    // Receive
    bool retval;
    retval = _sockMgr->recvData(rx_msg, msgs);

    if ( retval == false )
    {
        return MPXERR_COMM;
    }
    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

int MpxModule::rxMsgMon( struct mpixd_reply_msg **rx_msg, const std::vector<mpixd_type> &msgs)
{
    // Receive
    bool retval;
    // No need to fill in the sender address...
	retval = _sockMonMgr->recvData(rx_msg, msgs);

    if (retval == false)
    {
        return MPXERR_COMM;
    }

    return MPXERR_NOERROR;
}

// ----------------------------------------------------------------------------

void MpxModule::commError( std::string name )
{
    int err = errno;
    std::ostringstream oss;
    oss  << "### " << name << ": ";

    if (  std::string( strerror( err ) ) == std::string( "No error" ) )
        oss << "No error -> comm timeout";
    else
        oss << std::string( strerror( err ) );

    _lastErrStr = oss.str();
    _log->error( _lastErrStr );
}

// ----------------------------------------------------------------------------

void MpxModule::mpxError( std::string name )
{
    std::ostringstream oss;
    oss  << "### " << name << " err: " << _lastMpxErr << " = ";

    for ( int i = 0; i < 16; ++i )
    {
        if ( _lastMpxErr & ( 1 << i ) ) 
        {
            oss << MPIXD_ERR_STR[i] << " ";
        }
    }

    _lastErrStr = oss.str();
    _log->error( _lastErrStr );

    // Testing recovery (added 29 Mar 2011)
    if ( _lastMpxErr & MPIXD_ERR_TIMEOUT )
    {
        if ( this->resetChips() != MPXERR_NOERROR )
            _log->error("Force MPX reset: FAILED");
        else
            _log->error("Force MPX reset: OK");
    }
}

// ----------------------------------------------------------------------------

void MpxModule::replyError( std::string name, unsigned int reply )
{
    std::ostringstream oss;
    oss  << "### " << name << " reply: 0x" << std::hex << reply << std::dec;
    _lastErrStr = oss.str();
    _log->error( _lastErrStr );
}

// ----------------------------------------------------------------------------

i16 MpxModule::mxrCfgToI16( u8 pixcfg )
{
    i16 mask   = ( ( pixcfg & MXR_CFG8_MASK ) >> MXR_CFG8_MASK_SHIFT );
    i16 test   = ( ( pixcfg & MXR_CFG8_TEST ) >> MXR_CFG8_TEST_SHIFT );
    i16 lo_thr = ( ( pixcfg & MXR_CFG8_LO_THR_MASK ) >> MXR_CFG8_LO_THR_MASK_SHIFT );
    i16 hi_thr = ( ( pixcfg & MXR_CFG8_HI_THR_MASK ) >> MXR_CFG8_HI_THR_MASK_SHIFT );

    i16 retval = 0x0000;
    retval |= ( mask << MXR_CFG16_MASK_SHIFT );
    retval |= ( test << MXR_CFG16_TEST_SHIFT );
    retval |= ( ( ( lo_thr & 0x1 ) >> 0 ) << MXR_CFG16_LO_THR0_MASK_SHIFT );
    retval |= ( ( ( lo_thr & 0x2 ) >> 1 ) << MXR_CFG16_LO_THR1_MASK_SHIFT );
    retval |= ( ( ( lo_thr & 0x4 ) >> 2 ) << MXR_CFG16_LO_THR2_MASK_SHIFT );
    retval |= ( ( ( hi_thr & 0x1 ) >> 0 ) << MXR_CFG16_HI_THR0_MASK_SHIFT );
    retval |= ( ( ( hi_thr & 0x2 ) >> 1 ) << MXR_CFG16_HI_THR1_MASK_SHIFT );
    retval |= ( ( ( hi_thr & 0x4 ) >> 2 ) << MXR_CFG16_HI_THR2_MASK_SHIFT );

    return retval;
}

// ----------------------------------------------------------------------------

i16 MpxModule::tpxCfgToI16( u8 pixcfg )
{
    i16 mask   = ( pixcfg & TPX_CFG8_MASK ) >> TPX_CFG8_MASK_SHIFT;
    i16 test   = ( pixcfg & TPX_CFG8_TEST ) >> TPX_CFG8_TEST_SHIFT;
    i16 thradj = ( pixcfg & TPX_CFG8_THRADJ_MASK ) >> TPX_CFG8_THRADJ_MASK_SHIFT;
    i16 mode   = ( pixcfg & TPX_CFG8_MODE_MASK ) >> TPX_CFG8_MODE_MASK_SHIFT;

    i16 retval = 0x0000;
    retval |= ( mask << TPX_CFG16_MASK_SHIFT );
    retval |= ( test << TPX_CFG16_TEST_SHIFT );
    retval |= ( ( ( mode & 0x2 ) >> 1 ) << TPX_CFG16_P1_SHIFT );
    retval |= ( ( ( mode & 0x1 ) >> 0 ) << TPX_CFG16_P0_SHIFT );
    retval |= ( ( ( thradj & 0x1 ) >> 0 ) << TPX_CFG16_THR0_SHIFT );
    retval |= ( ( ( thradj & 0x2 ) >> 1 ) << TPX_CFG16_THR1_SHIFT );
    retval |= ( ( ( thradj & 0x4 ) >> 2 ) << TPX_CFG16_THR2_SHIFT );
    retval |= ( ( ( thradj & 0x8 ) >> 3 ) << TPX_CFG16_THR3_SHIFT );

    return retval;
}

// ----------------------------------------------------------------------------

void MpxModule::decode2Pixels( u8 *bytes, i16 *pixels )
{
    if ( _parReadout && _devInfo.numberOfChips == MPIX_MAX_DEVS )
    {
        _codec->decodePar(bytes, pixels);
    }
    else
    {
        this->seqStream2Data( _devInfo.numberOfChips, bytes, pixels );
    }
}

// ----------------------------------------------------------------------------


void MpxModule::stream2Data( u8 *stream, i16 *data )
{
    _codec->decodeSgl( stream, data );
}

// ----------------------------------------------------------------------------

void MpxModule::seqStream2Data( int ndevs, u8 *stream, i16 *data )
{
    // Convert pixel bit stream from 'ndevs' devices in byte array 'stream'
    // into (14-bit) pixel values in array 'data'
    u8  *pbytes = stream;
    i16 *pdata  = data;

    for ( int chipnr = 0; chipnr < ndevs; ++chipnr )
    {
        _codec->decodeSgl( pbytes, pdata );
        pbytes += MPIX_FRAME_BYTES;
        pdata  += MPIX_PIXELS;
    }
}

// ----------------------------------------------------------------------------

void MpxModule::parStream2Data( u8 *stream, i16 *data )
{
    _codec->decodePar(stream, data);
}

// ----------------------------------------------------------------------------

void MpxModule::mask2Stream( i16 *mask, u8 *stream )
{
    _codec->encodeSgl( mask, stream );
}

// ----------------------------------------------------------------------------

std::string MpxModule::timestamp()
{
    // Current time with second resolution
    char   str[64] = { '\0' };
    time_t t;
    t = time( NULL );
#if defined(_WINDLL) || defined(WIN32)
    ctime_s( str, 64, &t );
#else // Linux
    ctime_r( &t, str );
#endif
    // Remove the year (and the <newline>)
    str[strlen( str ) - 5] = 0;
    // ..and the day
    return std::string( &str[4] );
}

// ----------------------------------------------------------------------------
void MpxModule::setLastError ( std::string lastErrStr )
{
	_lastErrStr = lastErrStr;
	_log->error(lastErrStr);
}

// ----------------------------------------------------------------------------

int MpxModule::chipCount()
{
    return _devInfo.numberOfChips;
}

// ----------------------------------------------------------------------------

int MpxModule::chipPosition( int chipnr )
{
    return _chipMap[chipnr];
}

// ----------------------------------------------------------------------------

int MpxModule::chipType( int chipnr )
{
    // Return the type of the device, i.e. TPX or MXR
    // as derived from the chip ID
    u32 id = _chipId[_chipMap[chipnr]];
    int type;

    // Just in case it may have been forgotten to shift the extra bit
    // needed to get a proper ID for a Timepix...
    if ( id & 0x2 ) id >>= 1;

    if ( id & 0x1 )
        type = MPX_TPX;
    else
        type = MPX_MXR;

    return type;
}

// ----------------------------------------------------------------------------

std::string MpxModule::chipName( int chipnr )
{
    // Return a string with syntax "Yxx-Wwwww-TTT",
    // based on wafer number and X/Y position in the chip fuses
    // as described on page 9 of document "MPIX2MXR20 Manual v2.3"
    // with Y=[A..M] (mapped from Y[1:13]), xx=[11..01] (mapped from X[1::11])
    // and wwwww the wafer number preceeded by zeroes;
    // TTT indicates the chip type (TPX or MXR).

    u32 id = _chipId[_chipMap[chipnr]];

    // Just in case it may have been forgotten to shift the extra bit
    // needed to get a proper ID for a Timepix...
    if ( id & 0x2 ) id >>= 1;

    if ( id == 0 || id > 0xFFFFFF ) return std::string( "Yxx-Wxxxx-TTT" );

    const int NIBBLE_REVERSED[0x10] = { 0x0, 0x8, 0x4, 0xC,
                                        0x2, 0xA, 0x6, 0xE,
                                        0x1, 0x9, 0x5, 0xD,
                                        0x3, 0xB, 0x7, 0xF
                                      };
    // Chip ID contents:
    // - Wafer number in bits 0-11 (reversed bit order)
    // - X in bits 12-15 (reversed bit order)
    // - Y in bits 16-19 (reversed bit order)
    // - Bits 20-23 should be 0
    int  nibble, wafer = 0, x, y;
    bool timepix;
    nibble = ( id & 0x00000F ) >> 0;
    wafer |= ( NIBBLE_REVERSED[nibble] << 8 );
    nibble = ( id & 0x0000F0 ) >> 4;
    wafer |= ( NIBBLE_REVERSED[nibble] << 4 );
    nibble = ( id & 0x000F00 ) >> 8;
    wafer |= ( NIBBLE_REVERSED[nibble] << 0 );

    if ( wafer & 0x800 ) timepix = true;
    else timepix = false;

    wafer &= 0x7FF;
    nibble = ( id & 0x00F000 ) >> 12;
    x      = NIBBLE_REVERSED[nibble];
    nibble = ( id & 0x0F0000 ) >> 16;
    y      = NIBBLE_REVERSED[nibble];
    nibble = ( id & 0xF00000 ) >> 20;

    // Compose a chip name as "Yxx-Wwwww-TTT"
    std::ostringstream oss;
    oss << static_cast<char> ( y - 1 + 'A' );
    x = 12 - x;

    if ( x < 10 ) oss << '0';

    oss << x << "-W";

    if ( wafer < 1000 ) oss << '0';

    if ( wafer <  100 ) oss << '0';

    if ( wafer <   10 ) oss << '0';

    oss << wafer;

    if ( timepix )
        oss << "-TPX";
    else
        oss << "-MXR";

    return oss.str();
}

// ----------------------------------------------------------------------------

/*
    Sets the high-speed read out mode. Changed the Relaxd modules read out
    clock speed from 62.5 MHz to 125 MHz. Allows for 120fps (else ~60) read out, but
    uses more power.
 */
int MpxModule::setHighSpeedReadout( bool h )
{
    u32 reg;
    int r = MPIXD_ERR_NONE;

    // check if something needs to change
    r = readReg( MPIX2_CONF_REG_OFFSET, &reg);

    if ( r != MPIXD_ERR_NONE ) return r;

    // if so, change it and write register
    if ( ( ( reg & MPIX2_CONF_RO_CLOCK_125MHZ ) != 0 ) != h) {
        reg &= ~MPIX2_CONF_RO_CLOCK_125MHZ;
        if ( h ) reg |= MPIX2_CONF_RO_CLOCK_125MHZ;

        r = writeReg( MPIX2_CONF_REG_OFFSET, reg );
        
    }
    return r;
}

// ----------------------------------------------------------------------------

/*
    Returns whether or not it is in high-speed read out mode.
 */
bool MpxModule::highSpeedReadout( )
{
    u32 reg;

    // assume this succeeds, else will result to false being returned.
    readReg( MPIX2_CONF_REG_OFFSET, &reg);

    return ( reg & MPIX2_CONF_RO_CLOCK_125MHZ ) != 0;
}


// ----------------------------------------------------------------------------

int MpxModule::openDataFile( std::string filename )
{
    _storedFrames = 0;
    _fData.open( filename.c_str() );

    if ( _fData.is_open() )
        return MPXERR_NOERROR;
    else
        return MPXERR_FILE_OPENWRITE;
}

// ----------------------------------------------------------------------------

void MpxModule::closeDataFile()
{
    if ( _fData.is_open() ) _fData.close();
}

// ----------------------------------------------------------------------------

int MpxModule::storeFrame( std::string info )
{
    // Read a frame, decode it, zerosuppress it and write it
    // to the data file (_fData) in "x y count" ascii format
    i16 data[MPIX_MAX_DEVS * MPIX_PIXELS], *pdata;
    int errval;

    errval = this->readMatrix( data, MPIX_MAX_DEVS * MPIX_PIXELS );

    if ( errval == MPXERR_NOERROR && _fData.is_open() )
    {
        ++_storedFrames;
        _fData << "# EVT " << _storedFrames << ", " << info << std::endl;
        pdata = data;

        // In case of more than 1 device assume the following positioning:
        //  0  1
        //  3  2
        // so
        //  0: x=[  0,255], y=[0,255]
        //  1: x=[256,511], y=[0,255]
        //  2: x=[256,511], y=[256,511]
        //  3: x=[  0,255], y=[256,511]
        for ( int dev = 0; dev < _devInfo.numberOfChips; ++dev )
        {
            int xoffs, yoffs;

            switch ( dev )
            {
            case 1:
                xoffs = 256;
                yoffs = 0;
                break;

            case 2:
                xoffs = 256;
                yoffs = 256;
                break;

            case 3:
                xoffs = 0;
                yoffs = 256;
                break;

            default:
                xoffs = 0;
                yoffs = 0;
                break;
            }

            for ( int y = 0; y < MPIX_ROWS; ++y )
            {
                // Use an intermediate ostringstream to speed things up
                std::ostringstream oss;

                for ( int x = 0; x < MPIX_COLS; ++x )
                {
                    if ( *pdata )
                    {
                        //_fData << setw(6) << x+xoffs << setw(6) << y+yoffs
                        oss << std::setw( 6 ) << x + xoffs << std::setw( 6 ) << y + yoffs
                            << std::setw( 8 ) << ( *pdata ) << std::endl;
                    }

                    ++pdata;
                }

                _fData << oss.str();
            }
        }

    }

    return errval;
}

// ----------------------------------------------------------------------------
