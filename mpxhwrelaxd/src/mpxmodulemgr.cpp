#include "mpxmodulemgrpimpl.h"
#include <sstream>
#include <cstdlib>

#if defined(_WINDLL) || defined(WIN32)
#include <windows.h>
#include <direct.h>
#define getcwd _getcwd
#else
#include <unistd.h>
#include <errno.h>
#endif

#include "common.h"
#include "mpxmodulemgr.h"
#include "mpxerrors.h"

// Static member: the one and only instance of the manager
MpxModuleMgr* MpxModuleMgr::_inst = 0;


// ----------------------------------------------------------------------------

MpxModuleMgr::MpxModuleMgr()
    : _modCnt(0),
    _globalLastErr(0),
    _groupSize(1),
    _moduleMgrPimpl(new AsiModuleMgrPimpl())
{
    _iniFileName = "MpxHwRelaxd.ini";
    _fLog.open("RelaxD-Manager.log");
}

// ----------------------------------------------------------------------------

MpxModuleMgr::~MpxModuleMgr()
{
    for (u32 i = 0; i < _modules.size(); ++i)
    {
        delete _modules[i];
    }
    if (_moduleMgrPimpl != NULL)
    {
        delete _moduleMgrPimpl;
    }
    if (_inst)
    {
        delete _inst;
    }
}

// ----------------------------------------------------------------------------

MpxModuleMgr* MpxModuleMgr::instance()
{
    if (_inst == 0)
    {
        _inst = new MpxModuleMgr();
        //#if !defined(_WINDLL) && !defined(WIN32)
        // Make the call to initMgr() for Linux here
        _inst->initMgr();
        //#endif
    }

    return _inst;
}

AsiModuleMgrPimpl *MpxModuleMgr::getPimpl()
{
    return instance()->_moduleMgrPimpl;
}

// ----------------------------------------------------------------------------

bool MpxModuleMgr::fileExists(std::string filename)
{
    std::ifstream test(filename.c_str(), std::ifstream::in);
    return test.is_open();
}

// -----------------------------------------------------------------------------

void MpxModuleMgr::initMgr()
{
    // Initialize this manager's stuff
    _fLog << "initMgr()" << std::endl;

    char tmp[128]; // Char array to please GetModuleFileName()
    //#if defined(_WINDLL) || defined(WIN32)
    //if( !GetModuleFileName( hInstance, tmp, MPX_MAX_PATH ) ) return;
    //_iniFileName = tmp;
    //#else
    getcwd(tmp, 128);
    _iniFileName = tmp;
    //#endif

    // Set 'ini' file name
#if defined(_WINDLL) || defined(WIN32)
    _iniFileName += "\\hwlibs\\MpxHwRelaxd.ini";
#else
    _iniFileName += "/hwlibs/MpxHwRelaxd.ini";
#endif

    if (fileExists(_iniFileName))
    {
        this->readIni();
    }
    else
    {
        this->autoDetect();
    }
}

// ----------------------------------------------------------------------------

#if !defined(_WINDLL) && !defined(WIN32)
// Versions of Windows stuff for Linux
int MpxModuleMgr::GetPrivateProfileInt(const char* app_name,
                                       const char* key_name,
                                       int dflt,
                                       const char* file_name)
{
    bool section_found = false;
    int val;
    std::ifstream fin;
    fin.open(file_name);

    if (fin.is_open())
    {
        char line[128];

        while (!fin.eof())
        {
            fin.getline(line, 128);

            if (section_found)
            {
                // Find the key name
                if ((size_t)fin.gcount() > strlen(key_name) + 1 &&
                    strncmp(line, key_name, strlen(key_name)) == 0)
                {
                    errno = 0;
                    val = strtol(&line[strlen(key_name) + 1], 0, 10);

                    if (errno == 0)
                    {
                        dflt = val;
                    }

                    break; // Break out of while-loop
                }
            }
            else
            {
                // Find the section name (NB: skip the initial '[')
                if ((size_t)fin.gcount() > strlen(app_name) + 1 &&
                    strncmp(&line[1], app_name, strlen(app_name)) == 0)
                {
                    section_found = true;
                }
            }
        }

        fin.close();
    }

    return dflt;
}

int MpxModuleMgr::GetPrivateProfileString(const char* app_name,
                                          const char* key_name,
                                          const char* dflt,
                                          char* returned_string,
                                          int sz,
                                          const char* file_name)
{
    bool section_found = false;
    std::ifstream fin;
    strncpy(returned_string, dflt, strlen(dflt));
    fin.open(file_name);

    if (fin.is_open())
    {
        char line[128];

        while (!fin.eof())
        {
            fin.getline(line, 128);

            if (section_found)
            {
                // Find the key name
                if ((size_t)fin.gcount() > strlen(key_name) + 1 &&
                    strncmp(line, key_name, strlen(key_name)) == 0)
                {
                    strcpy(returned_string, &line[strlen(key_name) + 1]);
                    break; // Break out of while-loop
                }
            }
            else
            {
                // Find the section name (NB: skip the initial '[')
                if ((size_t)fin.gcount() > strlen(app_name) + 1 &&
                    strncmp(&line[1], app_name, strlen(app_name)) == 0)
                {
                    section_found = true;
                }
            }
        }

        fin.close();
    }

    return strlen(returned_string);
}

#endif

// ----------------------------------------------------------------------------

void MpxModuleMgr::autoDetect()
{
    _fLog << "==> autoDetect()" << std::endl;

    MpxModule* module;
    for (int i = 0; i < 31; ++i)
    {
        module = 0;
        try
        {
            module = new MpxModule(i, true);
            if (module->ping())
            {
                _id.push_back(i);
                _chipCnt.push_back(0);
                _chipNr.push_back(0);
                _chipType.push_back(MPX_TPX);
                _fLog << "Found module with ID=" << i << std::endl;
                ++_modCnt;
            }
            delete module;
        }
        catch (...)
        {
            _fLog << "Some error occured!" << std::endl;
            if (module)
            {
                delete module;
            }
        }
    }
    _fLog << "autoDetect() done: found " << _modCnt << " module(s)" << std::endl;
}

// ----------------------------------------------------------------------------

void MpxModuleMgr::readIni()
{
    // Look for settings for up to 8 RelaxD modules
    // (with different ID, which determines their MAC and IP addresses)
    std::string relaxd("RelaxD-");
    std::string relaxd_x;
    int i, val;
    _fLog << "==> readIni(), filename=" << _iniFileName << std::endl;
    _modCnt = 0;

    _groupSize = GetPrivateProfileInt("GlobalConfig", "GroupSize", 1,
                                      _iniFileName.c_str());

    if (_groupSize > ASI_MAX_GROUP_SIZE)
    {
        _groupSize = ASI_MAX_GROUP_SIZE;
        _fLog << "GroupSize too big, setting to max of " << ASI_MAX_GROUP_SIZE << std::endl;
    }
    _fLog << "GroupSize=" << _groupSize << std::endl;
    for (i = 0; i < 8; ++i)
    {
        relaxd_x = relaxd + ( char ) ('0' + i);

        val = GetPrivateProfileInt(relaxd_x.c_str(), "ChipCount", -1,
                                   _iniFileName.c_str());

        if (val > -1)   // Allow ChipCount=0 (1 July 2011)
        {
            ++_modCnt;
            _id.push_back(i);
            _chipCnt.push_back(val);

            val = GetPrivateProfileInt(relaxd_x.c_str(), "ChipNr", 0,
                                       _iniFileName.c_str());
            _chipNr.push_back(val);

            char type[32];
            GetPrivateProfileString(relaxd_x.c_str(), "Type", "TPX",
                                    type, 32, _iniFileName.c_str());
            std::string t(type);
            val = MPX_TPX;

            if (t.length() == 3)
            {
                if (t == std::string("MXR"))
                {
                    val = MPX_MXR;
                }
                else if (t == std::string("MX3"))
                {
                    val = MPX_3;
                }
            }

            _chipType.push_back(val);

            _fLog << "PixMan-ID " << i << ": "
                  << " RelaxD-ID=" << _id[_modCnt - 1] << " [" << relaxd_x << "]"
                  << " type=";

            if (_chipType[_modCnt - 1] == MPX_MXR)
            {
                _fLog << "MXR";
            }
            else if (_chipType[_modCnt - 1] == MPX_TPX)
            {
                _fLog << "TPX";
            }
            else if (_chipType[_modCnt - 1] == MPX_3)
            {
                _fLog << "MX3";
            }
            else
            {
                _fLog << "???";
            }

            _fLog << " chips=" << _chipCnt[_modCnt - 1]
                  << " chipnr=" << _chipNr[_modCnt - 1] << std::endl;
        }
    }

    _fLog << "Ini file: " << _modCnt << " devices defined" << std::endl;
}

// ----------------------------------------------------------------------------

int MpxModuleMgr::scanDevices(  )
{
    if (_modCnt > ( int ) _modules.size())
    {
        // Still to instantiate one or more modules
        for (int i = ( int )_modules.size(); i < _modCnt; ++i)
        {
            int nrows = 1;

            if (_chipCnt[i] == 4)
            {
                nrows = 2;
            }

            MpxModule* module = new MpxModule(_id[i], _chipType[i], _chipCnt[i],
                                              nrows, _chipNr[i]);

// VvB - 27/11/2012 added check to see if it is really there.
            if (!module->ping())
            {
                delete module;
                continue;
            }

// juhe - 05/04/2012 error check added
            int lastErr = module->lastMpxErr();
            if (lastErr != MPXERR_NOERROR)
            {
                delete module;
                return lastErr; // error occured
            }

            _modules.push_back(module);
            _fLog << "Instantiated Relaxd ID=" << _id[i] << ": ";
            int type = module->chipType(i);

            if (type == MPX_MXR)
            {
                _fLog << "MXR";
            }
            else if (type == MPX_TPX)
            {
                _fLog << "TPX";
            }
            else if (type == MPX_3)
            {
                _fLog << "MX3";
            }
            else
            {
                _fLog << "???";
            }

            _fLog << " chips=" << module->chipCount()
                  << " first chip position=" << module->chipPosition(0)
                  << std::endl;
        }
    }

    if (_modules.size() > 0)
    {
        //_fLog << "getFirst() OKAY" << endl;
        return 0;
    }

    return 1;
}

// ----------------------------------------------------------------------------

MpxModule* MpxModuleMgr::device(int hw_id)
{
    if (( u32 ) hw_id < _modules.size())
    {
        return _modules[hw_id];
    }

    return 0;
}

// ----------------------------------------------------------------------------
