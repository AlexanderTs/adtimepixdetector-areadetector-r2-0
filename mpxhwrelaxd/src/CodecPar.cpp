/*
*  CodecPar.cpp
*
*  Created by Chris Desjardins on 25/01/13.
*  Copyright 2013 amscins.com All rights reserved.
*
*/
#include "CodecPar.h"
#include "luts.h"

void AsiCodecPar::decodeRows(const int startRow, const int endRow, const AsiFrame<AsiEncodeWord_t> *inFrame, AsiFrame<AsiDecodeWord_t> *outFrame) const
{
    for (int row = startRow; row < endRow; row ++)
    {
        decodeRows(row, inFrame, outFrame);
    }
}

void AsiCodecPar::decodeRows(const int startRow, const AsiFrame<AsiEncodeWord_t> *inFrame, AsiFrame<AsiDecodeWord_t> *outFrames) const
{
    AsiDecodeWord_t *pdev0, *pdev1, *pdev2, *pdev3;
    int col;
    int pixBit;
    AsiDecodeWord_t pixBitMask;
    AsiDecodeWord_t *pdata;
    const AsiEncodeWord_t *pstream = AsiFrame<AsiEncodeWord_t>::getFrameListRowPtr(inFrame, startRow * MPIX_MAX_DEVS);
    AsiEncodeWord_t byte;
    int offset = 0;

    pdev3 = outFrames[3].getFramePtr();
    pdev2 = outFrames[2].getFramePtr();
    pdev1 = outFrames[1].getFramePtr();
    pdev0 = outFrames[0].getFramePtr();

    for (pixBit = inFrame->getBitsPerPixel() - 1; pixBit >= 0; pixBit--)
    {
        pixBitMask = 1 << pixBit;
        offset = outFrames[0].getRowElements() * startRow;
        for (col = 0; col < inFrame->getColCnt(); col += 4)
        {
            byte = *pstream;
            pdata = &pdev3[offset];
            if ( byte & 0x80 ) { pdata[0] |= pixBitMask; }
            if ( byte & 0x40 ) { pdata[1] |= pixBitMask; }
            if ( byte & 0x20 ) { pdata[2] |= pixBitMask; }
            if ( byte & 0x10 ) { pdata[3] |= pixBitMask; }
            pdata = &pdev2[offset];
            if ( byte & 0x08 ) { pdata[0] |= pixBitMask; }
            if ( byte & 0x04 ) { pdata[1] |= pixBitMask; }
            if ( byte & 0x02 ) { pdata[2] |= pixBitMask; }
            if ( byte & 0x01 ) { pdata[3] |= pixBitMask; }
            pstream++;
            byte = *pstream;
            pdata = &pdev1[offset];
            if ( byte & 0x80 ) { pdata[0] |= pixBitMask; }
            if ( byte & 0x40 ) { pdata[1] |= pixBitMask; }
            if ( byte & 0x20 ) { pdata[2] |= pixBitMask; }
            if ( byte & 0x10 ) { pdata[3] |= pixBitMask; }
            pdata = &pdev0[offset];
            if ( byte & 0x08 ) { pdata[0] |= pixBitMask; }
            if ( byte & 0x04 ) { pdata[1] |= pixBitMask; }
            if ( byte & 0x02 ) { pdata[2] |= pixBitMask; }
            if ( byte & 0x01 ) { pdata[3] |= pixBitMask; }
            pstream++;
            offset += 4;
        }
    }
}

void AsiCodecPar::decodeSetup(AsiFrame<AsiDecodeWord_t> *outFrames) const 
{
    for (int i = 0; i < MPIX_MAX_DEVS; i++)
    {
        outFrames[i].zeroRows();
    }
}

void AsiCodecPar::decodeCleanup(AsiFrame<AsiDecodeWord_t> *outFrames) const 
{
    applyLookupTable(0, outFrames->getRowCnt() * MPIX_MAX_DEVS, outFrames);
}
