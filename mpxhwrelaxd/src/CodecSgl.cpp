/*
*  CodecSgl.cpp
*
*  Created by Chris Desjardins on 25/01/13.
*  Copyright 2013 amscins.com All rights reserved.
*
*/
#include "CodecSgl.h"
#include "luts.h"

void AsiCodecSgl::encodeRows(const AsiFrame<AsiDecodeWord_t> *inFrame, AsiFrame<AsiEncodeWord_t> *outFrame) const
{
    int bit;
    int offset;
    int row;
    int col;
    int pixBit;
    AsiEncodeWord_t *outFramePtr = outFrame->getFramePtr();
    const AsiDecodeWord_t *inFramePtr = inFrame->getFramePtr();
    const AsiDecodeWord_t *pmask;
    AsiDecodeWord_t pixBitMask;
    AsiEncodeWord_t byte;
    AsiEncodeWord_t bitMask;
    int colCnt = inFrame->getColCnt();

    for (row = 0; row < inFrame->getRowCnt(); row++)
    {
        for (pixBit = inFrame->getBitsPerPixel() - 1; pixBit >= 0; pixBit--)
        {
            pixBitMask = (1 << pixBit);
            offset = row * colCnt;
            for (col = 0; col < colCnt; col += 8)
            {
                byte = 0;
                bitMask = 0x80;
                pmask = &inFramePtr[offset];
                for (bit = 0; bit < 8; bit++)
                {
                    if (pmask[bit] & pixBitMask)
                    {
                        byte |= bitMask;
                    }
                    bitMask >>= 1;
                }
                *outFramePtr = byte;
                outFramePtr++;
                offset += 8;
            }
        }
    }
}

void AsiCodecSgl::decodeRows(const int startRow, const int endRow, const AsiFrame<AsiEncodeWord_t> *inFrame, AsiFrame<AsiDecodeWord_t> *outFrame) const
{
    int offset;
    int row;
    int col;
    int pixBit;
    AsiDecodeWord_t *outFramePtr = outFrame->getFramePtr();
    const AsiEncodeWord_t *inpPtr;
    AsiDecodeWord_t pixBitMask;
    AsiEncodeWord_t byte;
    AsiEncodeWord_t bitMask;
    int colCnt = inFrame->getColCnt();

    outFrame->zeroRows(startRow, endRow);
    inpPtr = inFrame->getFramePtr(startRow);
    for (row = startRow; row < endRow; row++)
    {
        for (pixBit = inFrame->getBitsPerPixel() - 1; pixBit >= 0; pixBit--)
        {
            pixBitMask = (1 << pixBit);
            offset = row * colCnt;
            for (col = 0; col < colCnt; col += 8)
            {
                bitMask = 0x80;
                byte = *inpPtr;
                while (bitMask != 0)
                {
                    if (byte & bitMask)
                    {
                        outFramePtr[offset] |= pixBitMask;
                    }
                    bitMask >>= 1;
                    offset++;
                }
                inpPtr++;
            }
        }
    }
    applyLookupTable(startRow, endRow, outFrame);
}

