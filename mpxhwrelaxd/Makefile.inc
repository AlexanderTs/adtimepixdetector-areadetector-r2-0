CC          = $(CROSS_COMPILE)gcc
CXX         = $(CROSS_COMPILE)g++
AR          = $(CROSS_COMPILE)ar
SDIR        = src
CPP_SRCS    = $(wildcard $(SDIR)/*.cpp)
C_SRCS      = $(wildcard $(SDIR)/*.c)
OBJECTS	    = $(addprefix $(ODIR)/,$(notdir $(CPP_SRCS:.cpp=.o) $(C_SRCS:.c=.o)))
LIBRARIES  += $(patsubst %,-l%,$(LIBS)) $(patsubst %,-L%,$(LIBDIRS))
GENFLAGS   += $(patsubst %,-I%,$(INCLUDEDIRS)) -Wall -pthread -D_THREAD_SAFE -DMPXMODULE_EXPORT 
CFLAGS     += $(GENFLAGS)
CXXFLAGS   += $(GENFLAGS)
DEPS        = $(patsubst %,%,$(OBJECTS:.o=.d))
MAKEDEPEND  = $(SILENT)$(CC) -MF"$(@:.o=.d)" -MG -MM -MP -MT"$(@:.d=.o)" "$<" $(CFLAGS) 
MKODIR      = -@mkdir -p $(ODIR)
DEBUG      ?= 0
BOOSTDIR	= ./boost_1_52_0

ifeq ($(STATIC), 1)
OUTLIB			= $(OUTSTATICLIB)
GENFLAGS		+= -fPIC -DPIC
LIBTYPE         = static
else
OUTLIB			= $(OUTSHAREDLIB)
GENFLAGS		+= -fPIC -DPIC
LIBTYPE         = shared
endif

.PHONY: boost
# use DEBUG=1 or DEBUG=0 to enable/disable debug builds, default is DEBUG=1.

ifneq ($(DEBUG), 0)
GENFLAGS     += -g -DDEBUG
RDIR		  = Debug
else
GENFLAGS     += -O2 -DNDEBUG
RDIR		  = Release
endif
ODIR          = $(RDIR)/$(LIBTYPE)

# use either V=1 or VERBOSE=1 to enable verbose mode

ifdef V
VERBOSE=1
endif

ifndef VERBOSE
SILENT=@
NICEDEPOUTPUT=@echo "  Depends  $@"
NICEBLDOUTPUT=@echo "  Building $@"
NICECLNOUTPUT=@echo "  Cleaning $(ODIR)"
endif

all: $(DEPS) $(OUTEXE) $(OUTLIB)

$(ODIR)/%.d: $(SDIR)/%.cpp
	$(NICEDEPOUTPUT)
	$(MKODIR)
	$(MAKEDEPEND)

$(ODIR)/%.o: $(SDIR)/%.c
	$(NICEBLDOUTPUT)
	$(MKODIR)
	$(SILENT)$(CC) -c -o $@ $< $(CFLAGS)

$(ODIR)/%.o: $(SDIR)/%.cpp
	$(NICEBLDOUTPUT)
	$(MKODIR)
	$(SILENT)$(CXX) -c -o $@ $< $(CXXFLAGS) 

$(ODIR)/%.a: $(OBJECTS)  $(ARCHIVES)
	$(NICEBLDOUTPUT)
	$(MKODIR)
	rm -rf *.o
	$(AR) x $(BOOSTDIR)/stage/lib/libboost_thread.a 
	$(AR) x $(BOOSTDIR)/stage/lib/libboost_system.a
	$(AR) x $(BOOSTDIR)/stage/lib/libboost_chrono.a
	$(AR) rcsv $@  $(OBJECTS) *.o
	rm -rf *.o
	
	#$(AR) rcsv $@  *.o $(OBJECTS)

$(ODIR)/%.so: $(OBJECTS)
	$(NICEBLDOUTPUT)
	$(MKODIR)
	$(SILENT)$(CXX) -shared -fPIC -o $@ $^ $(ARCHIVES) $(LIBRARIES) 

$(OUTEXE): $(OBJECTS) $(ARCHIVES)
	$(NICEBLDOUTPUT)
	$(MKODIR)
	$(SILENT)$(CXX) -o $@ $^ $(CFLAGS) $(LIBRARIES) $(ARCHIVES)

boost:
	cd $(BOOSTDIR); ./bootstrap.sh
	cd $(BOOSTDIR); ./b2 link=static -j 4 cxxflags=-fPIC stage -a --with-system --with-program_options --with-thread --with-chrono --with-date_time --with-regex

.PHONY: clean realclean

clean:
	$(NICECLNOUTPUT)
	$(SILENT)$(RM) $(ODIR)/*

realclean:
	$(RM) -rf Release
	$(RM) -rf Debug
