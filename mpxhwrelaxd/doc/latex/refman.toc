\contentsline {chapter}{\numberline {1}\discretionary {-}{}{}Class \discretionary {-}{}{}Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\discretionary {-}{}{}Class \discretionary {-}{}{}Hierarchy}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}\discretionary {-}{}{}Class \discretionary {-}{}{}Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}\discretionary {-}{}{}Class \discretionary {-}{}{}List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}\discretionary {-}{}{}Class \discretionary {-}{}{}Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}\discretionary {-}{}{}Fsr \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}mpixd\discretionary {-}{}{}\_\discretionary {-}{}{}flash\discretionary {-}{}{}\_\discretionary {-}{}{}pg \discretionary {-}{}{}Struct \discretionary {-}{}{}Reference}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}\discretionary {-}{}{}Mpx\discretionary {-}{}{}File\discretionary {-}{}{}Logger \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{6}{section.3.3}
\contentsline {section}{\numberline {3.4}\discretionary {-}{}{}Mpx\discretionary {-}{}{}Logger \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{6}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{7}{subsection.3.4.1}
\contentsline {section}{\numberline {3.5}\discretionary {-}{}{}Mpx\discretionary {-}{}{}Module \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{7}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{11}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}\discretionary {-}{}{}Constructor \& \discretionary {-}{}{}Destructor \discretionary {-}{}{}Documentation}{11}{subsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.2.1}\discretionary {-}{}{}Mpx\discretionary {-}{}{}Module}{11}{subsubsection.3.5.2.1}
\contentsline {subsection}{\numberline {3.5.3}\discretionary {-}{}{}Member \discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{11}{subsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.3.1}chip\discretionary {-}{}{}Count}{11}{subsubsection.3.5.3.1}
\contentsline {subsubsection}{\numberline {3.5.3.2}chip\discretionary {-}{}{}Name}{11}{subsubsection.3.5.3.2}
\contentsline {subsubsection}{\numberline {3.5.3.3}chip\discretionary {-}{}{}Position}{12}{subsubsection.3.5.3.3}
\contentsline {subsubsection}{\numberline {3.5.3.4}chip\discretionary {-}{}{}Type}{12}{subsubsection.3.5.3.4}
\contentsline {subsubsection}{\numberline {3.5.3.5}close\discretionary {-}{}{}Data\discretionary {-}{}{}File}{12}{subsubsection.3.5.3.5}
\contentsline {subsubsection}{\numberline {3.5.3.6}close\discretionary {-}{}{}Shutter}{13}{subsubsection.3.5.3.6}
\contentsline {subsubsection}{\numberline {3.5.3.7}config\discretionary {-}{}{}Test\discretionary {-}{}{}Pulse}{13}{subsubsection.3.5.3.7}
\contentsline {subsubsection}{\numberline {3.5.3.8}decode2\discretionary {-}{}{}Pixels}{13}{subsubsection.3.5.3.8}
\contentsline {subsubsection}{\numberline {3.5.3.9}enable\discretionary {-}{}{}Ext\discretionary {-}{}{}Trigger}{14}{subsubsection.3.5.3.9}
\contentsline {subsubsection}{\numberline {3.5.3.10}enable\discretionary {-}{}{}Timer}{14}{subsubsection.3.5.3.10}
\contentsline {subsubsection}{\numberline {3.5.3.11}erase\discretionary {-}{}{}Dacs}{14}{subsubsection.3.5.3.11}
\contentsline {subsubsection}{\numberline {3.5.3.12}erase\discretionary {-}{}{}Pixels\discretionary {-}{}{}Cfg}{14}{subsubsection.3.5.3.12}
\contentsline {subsubsection}{\numberline {3.5.3.13}get\discretionary {-}{}{}Driver\discretionary {-}{}{}Version}{15}{subsubsection.3.5.3.13}
\contentsline {subsubsection}{\numberline {3.5.3.14}get\discretionary {-}{}{}Firmware\discretionary {-}{}{}Version}{15}{subsubsection.3.5.3.14}
\contentsline {subsubsection}{\numberline {3.5.3.15}get\discretionary {-}{}{}Mpx\discretionary {-}{}{}Dac}{15}{subsubsection.3.5.3.15}
\contentsline {subsubsection}{\numberline {3.5.3.16}high\discretionary {-}{}{}Speed\discretionary {-}{}{}Readout}{16}{subsubsection.3.5.3.16}
\contentsline {subsubsection}{\numberline {3.5.3.17}init}{16}{subsubsection.3.5.3.17}
\contentsline {subsubsection}{\numberline {3.5.3.18}last\discretionary {-}{}{}Clock\discretionary {-}{}{}Tick}{16}{subsubsection.3.5.3.18}
\contentsline {subsubsection}{\numberline {3.5.3.19}last\discretionary {-}{}{}Err\discretionary {-}{}{}Str}{17}{subsubsection.3.5.3.19}
\contentsline {subsubsection}{\numberline {3.5.3.20}last\discretionary {-}{}{}Frame\discretionary {-}{}{}Count}{17}{subsubsection.3.5.3.20}
\contentsline {subsubsection}{\numberline {3.5.3.21}last\discretionary {-}{}{}Time\discretionary {-}{}{}Stamp}{17}{subsubsection.3.5.3.21}
\contentsline {subsubsection}{\numberline {3.5.3.22}load\discretionary {-}{}{}Config}{17}{subsubsection.3.5.3.22}
\contentsline {subsubsection}{\numberline {3.5.3.23}log\discretionary {-}{}{}Verbose}{17}{subsubsection.3.5.3.23}
\contentsline {subsubsection}{\numberline {3.5.3.24}msg\discretionary {-}{}{}Available}{18}{subsubsection.3.5.3.24}
\contentsline {subsubsection}{\numberline {3.5.3.25}new\discretionary {-}{}{}Frame}{18}{subsubsection.3.5.3.25}
\contentsline {subsubsection}{\numberline {3.5.3.26}open\discretionary {-}{}{}Data\discretionary {-}{}{}File}{18}{subsubsection.3.5.3.26}
\contentsline {subsubsection}{\numberline {3.5.3.27}open\discretionary {-}{}{}Shutter}{19}{subsubsection.3.5.3.27}
\contentsline {subsubsection}{\numberline {3.5.3.28}open\discretionary {-}{}{}Shutter}{19}{subsubsection.3.5.3.28}
\contentsline {subsubsection}{\numberline {3.5.3.29}par\discretionary {-}{}{}Readout}{19}{subsubsection.3.5.3.29}
\contentsline {subsubsection}{\numberline {3.5.3.30}ping}{20}{subsubsection.3.5.3.30}
\contentsline {subsubsection}{\numberline {3.5.3.31}read\discretionary {-}{}{}Adc}{20}{subsubsection.3.5.3.31}
\contentsline {subsubsection}{\numberline {3.5.3.32}read\discretionary {-}{}{}Chip\discretionary {-}{}{}Id}{20}{subsubsection.3.5.3.32}
\contentsline {subsubsection}{\numberline {3.5.3.33}read\discretionary {-}{}{}Firmware}{21}{subsubsection.3.5.3.33}
\contentsline {subsubsection}{\numberline {3.5.3.34}read\discretionary {-}{}{}Fsr}{21}{subsubsection.3.5.3.34}
\contentsline {subsubsection}{\numberline {3.5.3.35}read\discretionary {-}{}{}Fsr}{21}{subsubsection.3.5.3.35}
\contentsline {subsubsection}{\numberline {3.5.3.36}read\discretionary {-}{}{}Matrix}{22}{subsubsection.3.5.3.36}
\contentsline {subsubsection}{\numberline {3.5.3.37}read\discretionary {-}{}{}Matrix\discretionary {-}{}{}Raw}{22}{subsubsection.3.5.3.37}
\contentsline {subsubsection}{\numberline {3.5.3.38}read\discretionary {-}{}{}Reg}{22}{subsubsection.3.5.3.38}
\contentsline {subsubsection}{\numberline {3.5.3.39}read\discretionary {-}{}{}Tsensor}{23}{subsubsection.3.5.3.39}
\contentsline {subsubsection}{\numberline {3.5.3.40}refresh\discretionary {-}{}{}Fsr}{23}{subsubsection.3.5.3.40}
\contentsline {subsubsection}{\numberline {3.5.3.41}reset\discretionary {-}{}{}Chips}{23}{subsubsection.3.5.3.41}
\contentsline {subsubsection}{\numberline {3.5.3.42}reset\discretionary {-}{}{}Frame\discretionary {-}{}{}Counter}{24}{subsubsection.3.5.3.42}
\contentsline {subsubsection}{\numberline {3.5.3.43}reset\discretionary {-}{}{}Matrix}{24}{subsubsection.3.5.3.43}
\contentsline {subsubsection}{\numberline {3.5.3.44}rmw\discretionary {-}{}{}Reg}{24}{subsubsection.3.5.3.44}
\contentsline {subsubsection}{\numberline {3.5.3.45}set\discretionary {-}{}{}Fsr}{24}{subsubsection.3.5.3.45}
\contentsline {subsubsection}{\numberline {3.5.3.46}set\discretionary {-}{}{}High\discretionary {-}{}{}Speed\discretionary {-}{}{}Readout}{25}{subsubsection.3.5.3.46}
\contentsline {subsubsection}{\numberline {3.5.3.47}set\discretionary {-}{}{}Log\discretionary {-}{}{}Verbose}{25}{subsubsection.3.5.3.47}
\contentsline {subsubsection}{\numberline {3.5.3.48}set\discretionary {-}{}{}Par\discretionary {-}{}{}Readout}{26}{subsubsection.3.5.3.48}
\contentsline {subsubsection}{\numberline {3.5.3.49}set\discretionary {-}{}{}Pixels\discretionary {-}{}{}Cfg}{26}{subsubsection.3.5.3.49}
\contentsline {subsubsection}{\numberline {3.5.3.50}set\discretionary {-}{}{}Pixels\discretionary {-}{}{}Cfg}{26}{subsubsection.3.5.3.50}
\contentsline {subsubsection}{\numberline {3.5.3.51}set\discretionary {-}{}{}Pixels\discretionary {-}{}{}Cfg}{27}{subsubsection.3.5.3.51}
\contentsline {subsubsection}{\numberline {3.5.3.52}store\discretionary {-}{}{}Dacs}{27}{subsubsection.3.5.3.52}
\contentsline {subsubsection}{\numberline {3.5.3.53}store\discretionary {-}{}{}Frame}{27}{subsubsection.3.5.3.53}
\contentsline {subsubsection}{\numberline {3.5.3.54}store\discretionary {-}{}{}Id}{28}{subsubsection.3.5.3.54}
\contentsline {subsubsection}{\numberline {3.5.3.55}store\discretionary {-}{}{}Pixels\discretionary {-}{}{}Cfg}{28}{subsubsection.3.5.3.55}
\contentsline {subsubsection}{\numberline {3.5.3.56}test\discretionary {-}{}{}Pulse\discretionary {-}{}{}Enable}{28}{subsubsection.3.5.3.56}
\contentsline {subsubsection}{\numberline {3.5.3.57}timer\discretionary {-}{}{}Expired}{29}{subsubsection.3.5.3.57}
\contentsline {subsubsection}{\numberline {3.5.3.58}write\discretionary {-}{}{}Dac}{29}{subsubsection.3.5.3.58}
\contentsline {subsubsection}{\numberline {3.5.3.59}write\discretionary {-}{}{}Firmware}{29}{subsubsection.3.5.3.59}
\contentsline {subsubsection}{\numberline {3.5.3.60}write\discretionary {-}{}{}Reg}{30}{subsubsection.3.5.3.60}
\contentsline {section}{\numberline {3.6}\discretionary {-}{}{}Mpx\discretionary {-}{}{}Module\discretionary {-}{}{}Mgr \discretionary {-}{}{}Class \discretionary {-}{}{}Reference}{31}{section.3.6}
