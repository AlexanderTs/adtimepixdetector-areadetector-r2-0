=====================
 MpxHwRelaxd library
=====================
Version: 1.9.15

This directory contains the Medipix Relaxd library. This library is used as
part of the Relaxd software stack and is required by most Relaxd applications.

Also this library is used by Pixelman to access Medipix and Timepix devices
through the Relaxd module.

The library has been tested under:
Windows 7
Linux
OSX

Vincent van Beveren 
V.van.Beveren@nikhef.nl
29th of Februari 2011.

Building
========

For Windows
-----------
Open the mpxhwrelaxd.sln file in MSVS 2010 (Express) and compile, be sure
Debug or Release, and Win32 or x64 is correctly selected for what you intend.

In the 'release' directory a new file 'mpxhwrelaxd.dll' should have been created.

!!!
There is a CMakeLists.txt file now, so you can also do
mkdir build
cd build
cmake -A x64 ..
cmake --build . --config Release

For Linux/OSX
---------
Open a terminal and go to the directory in which the project is located.

Run the following command:

	make

In the 'Release/shared' directory a new file 'libmpxhwrelaxd.so' should have been created.

Alternatively use cmake:

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
