#include "mpxmodule.h"
#include "mpxerrors.h"
#include <iostream>
#include <sstream>
#include <cstdlib>

using namespace std;

// revision 1 is not used anymore
#define REV_BASE        2
#define PAGE_SIZE       256
#define PAGE_COUNT      4096

// Define 10MS sleep for errros
#ifdef _WIN32
#define xpSleep(ms)     Sleep(ms)
#endif
#ifdef __linux
#define xpSleep(ms)     usleep(1000 * ms)
#endif


void usage()
{
	cerr << "'flashprog' -- Loads a new firmware version into the Relaxd module" << endl;
	cerr << "Usage: flashprog <id> <operation> <file>" << endl << endl;
	cerr << "Operation can be:" << endl;
	cerr << "   w - write, the program in file is written to the Relaxd flash" << endl;
	cerr << "   r - read, the program is read from the Relaxd flash" << endl;
}

#pragma pack(push, 1)
struct fw_header {
    u8  magic[4];
    u32 firmware_ver;
    u8  board_rev;
    u8  reserved1;
    u16 reserved2;
    fw_header() {
        magic[0] = 'R'; magic[1] = 'X';
        magic[2] = 'F'; magic[3] = 'W';
    }
};
#pragma pack(pop)


void readFirmware(MpxModule * mpx, string file) 
{
	u8 buffer[PAGE_SIZE];
	int retries = 0;
    fw_header header;
    mpx->getFirmwareVersion(&header.firmware_ver, &header.board_rev);

	ofstream fo(file.c_str(), ofstream::binary);



    cout << "Relaxd firmware version " << header.firmware_ver << ", board rev " << static_cast<int>(header.board_rev + REV_BASE) << endl;

    fo.write(reinterpret_cast<const char*>(&header), sizeof(header));

	for (int page = 0; page < PAGE_COUNT; ++page) {
		if ( (page & 0xf) == 0xf) {
			cout << "Reading firmware, page " << (page + 1) << " of " << PAGE_COUNT << "\r";
			flush(cout);
		}
		retries = 4;
		while ( mpx->readFirmware( page, buffer ) != MPXERR_NOERROR ) {
			retries --;
			if (retries == 0) {
				cout << endl;
				cerr << "Failed reading firmware!" << endl;
				break;
			}
		}
		if (retries == 0)
			break;
		fo.write(reinterpret_cast<const char*>(&buffer[0]), PAGE_SIZE);
	}
	cout << endl << "Reading done" << endl;
	fo.close();

}

bool verifyFirmwareFile(MpxModule * mpx, ifstream * fi)
{
    int retries;
	u8 buffer[PAGE_SIZE];
    u8 verify[PAGE_SIZE];
    int errors = 0;
    for (int page = 0; page < PAGE_COUNT; ++page) {
        if ( (page & 0xf) == 0xf) {
            cout << "Verifying firmware, page " << (page + 1) << " of " << PAGE_COUNT << ", errors: " << errors << "\r";
	        flush(cout);
        }
        fi->read(reinterpret_cast<char*>(&verify[0]), PAGE_SIZE);

        retries = 4;
        while ( mpx->readFirmware( page, buffer ) != MPXERR_NOERROR ) {
	        retries --;
            if (retries == 0) {
                cerr << endl << "Verify operation failed, communication error." << endl;
		        return false;
            }
        }
        if (memcmp(buffer, verify, PAGE_SIZE) != 0) {
            errors++;
        }
    }
    if (errors > 0) {
        cerr << endl << "Verification failed, " << errors << " page(s) are/is incorrect" << endl;
        return false;
    } else {
        cout << endl << "Verification OK" << endl;
    }
    return true;
}


bool writeFirmwareFile(MpxModule * mpx, ifstream * fi)
{
    int retries;
	u8 buffer[PAGE_SIZE];
	u8 verify[PAGE_SIZE];
	int errors = 0;
	
	mpx->setSockTimeout(false,5000);

    for (int page = 0; page < PAGE_COUNT; ++page) {
        if ( (page & 0xf) == 0xf) {
	        cout << "Writing firmware, page " << (page + 1) << " of " << PAGE_COUNT << "\r";
	        flush(cout);
        }
        fi->read(reinterpret_cast<char*>(&buffer[0]), PAGE_SIZE);

        retries = 4;
        while ( mpx->writeFirmware( page, buffer ) != MPXERR_NOERROR ) {
	        retries --;
            if (retries == 0) {
                cerr << endl << "Writing failed, communication error!" << endl;
		        return false;
            }
        }
		retries = 4;
        while ( mpx->readFirmware( page, verify ) != MPXERR_NOERROR ) {
	        retries --;
            if (retries == 0) {
                cerr << endl << "Verification during upload failed, communication error." << endl;
		        return false;
            }
        }
        if (memcmp(buffer, verify, PAGE_SIZE) != 0) {
            errors++;
        }
    }

	if (errors > 0) {
        cerr << endl << "Verification during writing failed, " << errors << " page(s) are/is incorrect" << endl;
        return false;
    }

    cout << endl << "Writing done" << endl;

    return true;
}

void writeFirmware(MpxModule * mpx, string file)
{
	int retries = 4;
	int pagefail = 0;
    fw_header header;
    u32 cur_version;
    u8 cur_rev;

    mpx->getFirmwareVersion(&cur_version, &cur_rev);

    cout << "Relaxd firmware version " << cur_version << ", board rev " << static_cast<int>(cur_rev + REV_BASE) << endl << endl;


	ifstream fi(file.c_str(), ofstream::binary);

    if (!fi.is_open()) {
        cerr << "Could not open file " << file << endl;
        return;
    }

    fi.read(reinterpret_cast<char*>(&header), sizeof(header));

    unsigned long binOffset = fi.tellg();
    fi.seekg(0, ios_base::end);
    unsigned long size = fi.tellg();
    size -= binOffset;
    fi.seekg(binOffset);

    if (memcmp(header.magic, "RXFW", 4) != 0 || size != PAGE_SIZE * PAGE_COUNT) 
    {
        cerr << "Relaxd Firmware file invalid, aborting" << endl;
        return;
    }

    cout << "Relaxd firmware version in file: " << header.firmware_ver << ", board rev " << static_cast<int>(header.board_rev + REV_BASE) << endl << endl;

    if (cur_rev != header.board_rev) {
        cout << "The board revision is incompatible with this firmware, please obtain " << endl;
        cout << "the correct firmware version." << endl;
        return;
    }

    cout << "You are about to overwrite the Relaxd modules firmware. Please do not turn" << endl;
    cout << "off the device while writing, this will probably make your device un-usable." << endl << endl;
    cout << "Are you sure you wish to continue? Type [Y] + <enter> to confirm" << endl;

    char c;

    

    cin >> c;


    if (c != 'y' && c != 'Y') {
        cout << "Aborted" << endl;
        return;
    }
    
    retries = 8;

    while (retries > 0) {
        fi.seekg(binOffset);
        if (!writeFirmwareFile(mpx, &fi)) {
            cerr << "Write failed!" << endl;
            retries--;
            continue;
        }
        xpSleep(500);
        fi.seekg(binOffset);
        if (!verifyFirmwareFile(mpx, &fi)) {
            cerr << "Verify failed!" << endl;
            retries--;
            continue;
        } else break;
    }
    if (retries == 0) {
        cout << endl;
        cerr << "!!! Writing of new firmware failed !!!" << endl;
        cerr << "- Please try to start the firmware flashing utility again, and retry." << endl;
        cerr << "- Power cycling your device now may cause it not to boot." << endl;
    } else {
    	cout << endl << "Writing done, please reboot the device to activate the new firmware" << endl;
    }
	fi.close();
}

int main( int argc, char** argv )
{
	if( argc != 4 )
	{
		usage();
        system("pause");
		return 1;
    }
	int id = atoi(argv[1]);
	string op = string(argv[2]);
    string file = string(argv[3]);

	MpxModule *mpx = new MpxModule( id );

    if (!mpx->ping()) {
        cout << "Could not locate MPX module, is it connected? Is the ID correct?" << endl;
        return 1;
    }

	if (op == "r") {
		readFirmware(mpx, file);
	} else if (op == "w") {
		writeFirmware(mpx, file);
	}

    delete mpx;
	system("pause");
    return 0;
}


