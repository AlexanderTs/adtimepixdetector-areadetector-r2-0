#include "mpxmodule.h"
#include "mpxerrors.h"
#include <iostream>
#include <sstream>
#include <cstdlib>

using namespace std;

void usage()
{
  cerr << "'setid' -- replaces id of RelaxD module with new one" << endl;
  cerr << "Usage: setid old_id new_id" << endl;
}

int main( int argc, char** argv )
{
  if( argc != 3 )
    {
      usage();
      return 1;
    }
  else
    {
      int old_id = atoi(argv[1]);
      int new_id = atoi(argv[2]);

      MpxModule *mpx = new MpxModule( old_id );

      if( mpx->storeId( (u32) new_id ) != MPXERR_NOERROR )
	cout << mpx->lastErrStr() << endl;
      else
	cout << "New ID " << new_id << " was stored successfully." << endl;

      delete mpx;
    }

    return 0;
}
