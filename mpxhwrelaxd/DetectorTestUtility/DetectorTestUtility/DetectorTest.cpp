#include <iomanip>
#include <bt.h>
#include <boost/bind/protect.hpp>
#include "../shared.h"
#include "DetectorTest.h"
#include "mpxerrors.h"
#include "compat.h"
#include "common.h"

#define TIME_DECL \
    unsigned long startTime;\
    unsigned long endTime;\
    unsigned long timeDiffLast = 0;\
    unsigned long timeDiffCumulative = 0;\

#define TIME_IT(a) \
        startTime = getDetectorTestTime();\
        a; \
        endTime = getDetectorTestTime();\
        timeDiffLast = (endTime - startTime);\
        timeDiffCumulative += timeDiffLast;

class HwDacsReader : public CfgFileReader
{
public:
    HwDacsReader(std::string filename, void* data) : CfgFileReader(filename, data){};

protected:
    void varSetFunctionoid(std::string *key, std::string *value, void* data)
    {
        DetectorTest *dt = (DetectorTest*)data;
        dt->processHwDac(key, value);
    }
};

class RealDacsReader : public CfgFileReader
{
public:
    RealDacsReader(std::string filename, void* data) : CfgFileReader(filename, data)
    {
        _chipnr = 0;
        _index = 0;
    };

protected:
    void varSetFunctionoid(std::string *key, std::string *value, void* data)
    {
        DetectorTest *dt = (DetectorTest*)data;
        dt->processRealDac(&_chipnr, &_index, key, value);
    }
private:
    int _chipnr;
    int _index;
};

/*
** ioDir is the directory where input files are read and output files are written
** bQuiet is a flag that suppresses output that is not testable (VDD, board temp etc...)
*/
DetectorTest::DetectorTest(const int deviceId, const std::string &ioDir, bool bQuiet):
    MpxModule(deviceId, MPX_TPX, 0, 1, 0, new MpxFileLogger(ioDir)),
    _ndacs(0),
    _timerValue(0),
    _enableExternalTrigger(0),
    _enableSSE2(true),
    _callbackCount(0),
    _numOfCycles(0),
    _readRaw(true),
    _captureToFile(true),
    _bQuiet(bQuiet),
    _shutterCloseCount(0),
    _eTestType(EDT_CAPTURE_TEST),
    _eFrameNotification(EDT_FRAME_CALLBACK),
    _periodicFrameRate(false),
    _shutterCloseTimeoutUs(10000000),
    _multithreaded(false),
    _overwriteFrames(false)
{
    setLogVerbose(true);
    if (init() != MPXERR_NOERROR)
    {
        throw MyException("### init() failed");
    }
    resetChips();
    _ndevs = chipCount();
    if (_ndevs == 0)
    {
        throw MyException("Chip counting failed!\n");
    }
    std::cout << "chipCount = " << _ndevs << std::endl;
    int devtype = chipType( 0 );
    std::cout << "The type of a chip is: " << devtype << std::endl ;
    setParReadout(true);
    std::cout << "par readout = " << parReadout() << std::endl;
    _jobBatcher = AsiJobBatcher::create("DetectorTester", 1);
}

void DetectorTest::mpxCallback(INTPTR userData, int eventType, void *data)
{
    DetectorTest *relaxd = (DetectorTest*)userData;
    UNREFERENCED_PARAMETER(data);
    relaxd->_callbackCount++;
    if (eventType == HWCB_ACQFINISHED)
    {
        relaxd->_shutterClosed = true;
    }
}

DetectorTest::~DetectorTest()
{
    if (_log)
    {
        delete (MpxFileLogger*)_log;
    }
}

void DetectorTest::processRealDac(int *chipnr, int *index, std::string* key, std::string *value)
{
    int fsrValue;
    std::istringstream ( *value ) >> fsrValue;
    if ((*key).compare("Chip") == 0)
    {
        (*index) = 0;
        (*chipnr) = fsrValue;
    }
    else
    {
        _TDD[*chipnr][*index] = fsrValue;
        (*index)++;
    }
}

void DetectorTest::readFsrs()
{
    for (int i = 0; i < _ndevs; i++)
    {
        if (readFsr(i, _dacs[i], &_ndacs) != MPXERR_NOERROR)
        {
            std::stringstream s;
            s<<"### readFsr() chip " << i <<" failed";
            throw MyException(s.str());
        }
    }
}

void DetectorTest::writeFsrs()
{
    for (int i = 0; i < _ndevs; i++)
    {
        if (setFsr(i, _TDD[i]) != MPXERR_NOERROR)
        {
            std::stringstream s;
            s<<"### setFsr() chip " << i <<" failed";
            throw MyException(s.str());
        }
        else
        {
            std::cout << "setFsr() done for chip" << i  << std::endl;
        }
    }
}

void DetectorTest::readRealDacs(std::string filename)
{
    RealDacsReader realDacsReader(filename, this);
    readFsrs();
    realDacsReader.readCfgFile();
    writeFsrs();
    std::cout << "read DAC file done" << std::endl;
}

void DetectorTest::processConfigApis(std::string *key, std::string *value)
{
    int i;
    if (((*key).compare("TestType") == 0))
    {
        std::transform(value->begin(), value->end(), value->begin(), ::tolower);
        if (value->compare("capture") == 0)
        {
            _eTestType = EDT_CAPTURE_TEST;
        }
        else if (value->compare("digital") == 0)
        {
            _eTestType = EDT_DIGITAL_TEST;
        }
        else if (value->compare("codec") == 0)
        {
            _eTestType = EDT_CODEC_TEST;
        }
        else if (value->compare("ping") == 0)
        {
            _eTestType = EDT_PING_TEST;
        }
        else if (value->compare("id") == 0)
        {
            _eTestType = EDT_ID_TEST;
        }
        else
        {
            std::stringstream s;
            s<< "### Unknown TestType '" <<*value<<"'";
            throw MyException(s.str());
        }
    }
    else
    {
        std::istringstream ( *value ) >> i;
        if (((*key).compare("EnableExtTrigger") == 0))
        {
            _enableExternalTrigger = (i == 0) ? false : true;
        }
        else if (((*key).compare("EnableSSE2") == 0))
        {
            _enableSSE2 = (i == 0) ? false : true;
        }
        else if (((*key).compare("EnableTimer") == 0))
        {
            _timerValue = i;
        }
        else if (((*key).compare("NumOfCycles") == 0))
        {
            _numOfCycles = i;
        }
        else if (((*key).compare("ReadRaw") == 0))
        {
            _readRaw = (i == 0) ? false : true;
        }
        else if (((*key).compare("CaptureToFile") == 0))
        {
            _captureToFile = (i == 0) ? false : true;
        }
        else if (((*key).compare("PeriodicFrameRate") == 0))
        {
            _periodicFrameRate = (i == 0) ? false : true;
        }
        else if (((*key).compare("InvalidLutZero") == 0))
        {
            setInvalidLutZero((i == 0) ? false : true);
        }
        else if (((*key).compare("Multithreaded") == 0))
        {
            _multithreaded = ((i == 0) ? false : true);
        }
        else if (((*key).compare("OverwriteFrames") == 0))
        {
            _overwriteFrames = ((i == 0) ? false : true);
        }
        else if (((*key).compare("FrameNotification") == 0))
        {
            std::transform(value->begin(), value->end(), value->begin(), ::tolower);
            if (value->compare("getbusy") == 0)
            {
                _eFrameNotification = EDT_FRAME_GETBUSY;
            }
            else if (value->compare("newframe") == 0)
            {
                _eFrameNotification = EDT_FRAME_NEWFRAME;
            }
            else if (value->compare("callback") == 0)
            {
                _eFrameNotification = EDT_FRAME_CALLBACK;
            }
            else
            {
                std::stringstream s;
                s<< "### Unknown FrameNotification type '" <<*value<<"'";
                throw MyException(s.str());
            }
        }
        else
        {
            std::stringstream s;
            s<< "### Unknown option '" <<*key<<"'";
            throw MyException(s.str());
        }
    }
}

void DetectorTest::processHwDac(std::string *key, std::string *value)
{
    int index = 0;
    bool isFloat = false;

    if (((*key).compare("TestPulseHigh") == 0))
    {
        index = HW_ITEM_TESTPULSEHI;
        isFloat = true;
    }
    else if (((*key).compare("TestPulseLow") == 0))
    {
        index = HW_ITEM_TESTPULSELO;
        isFloat = true;
    }
    else if (((*key).compare("TestPulseFreq") == 0))
    {
        index = HW_ITEM_TESTPULSEFREQ;
    }
    else if (((*key).compare("BiasAdjust") == 0))
    {
        index = HW_ITEM_BIAS_VOLTAGE_ADJUST;
    }
    else if (((*key).compare("MpixVddAdjust") == 0))
    {
        index = HW_ITEM_VDD_ADJUST;
    }
    else if (((*key).compare("FirstChipNumber") == 0))
    {
        //index = HW_ITEM_FIRSTCHIPNR;
        index = -1;
    }
    else if (((*key).compare("LogVerbose") == 0))
    {
        index = HW_ITEM_LOGVERBOSE;
    }
    else if (((*key).compare("ParReadout") == 0))
    {
        index = HW_ITEM_PARREADOUT;
    }
    else if (((*key).compare("StorePixelsCfg") == 0))
    {
        index = HW_ITEM_STOREPIXELSCFG;
    }
    else if (((*key).compare("StoreDacs") == 0))
    {
        index = HW_ITEM_STOREDACS;
    }
    else if (((*key).compare("EraseStoredConfig") == 0))
    {
        index = HW_ITEM_ERASE_STORED_CFG;
    }
    else if (((*key).compare("ConfTpxClock") == 0))
    {
        index = HW_ITEM_CONF_TPX_CLOCK;
    }
    else if (((*key).compare("ConfRoClockMHz") == 0))
    {
        index = HW_ITEM_CONF_RO_CLOCK_125MHZ;
    }
    else if (((*key).compare("ConfTpxPreclocks") == 0))
    {
        index = HW_ITEM_CONF_TPX_PRECLOCKS;
    }
    else if (((*key).compare("TriggerType") == 0))
    {
        index = HW_ITEM_SET_TRIG_TYPE;
    }
    else
    {
        index = -1;
        processConfigApis(key, value);
    }
 
    if (index >= 0)
    {
        if (isFloat == true)
        {
            float f;
            std::istringstream ( *value ) >> f;
            if (setHwInfo(index, &f, 4) != MPXERR_NOERROR)
            {
                throw MyException(" setup of hw parameter TestPulse failed");
            }
        }
        else
        {
            int i;
            std::istringstream ( *value ) >> i;
            if (setHwInfo(index, &i, 4) != MPXERR_NOERROR)
            {
                std::stringstream s;
                s<< " setup of hw parameter " <<( i+11) << " failed";
                throw MyException(s.str());
            }
        }
    }
}

void DetectorTest::readHwDacs(std::string filename)
{
    HwDacsReader hwDacsReader(filename, this);
    hwDacsReader.readCfgFile();
    if ((_multithreaded == true) && (_readRaw == false))
    {
        throw MyException("When running multithreaded capture test, ReadRaw must be 1");
    }
}

void DetectorTest::readPixelsCfg(std::string filename)
{
    if (setPixelsCfg(filename) != MPXERR_NOERROR)
    {
        std::stringstream s;
        s<< "Error: setPixelsCfg() failed " << filename;
        throw MyException(s.str());
    }
    else
    {
        std::cout << "setPixelsCfg() set" << std::endl;
    }
}

void DetectorTest::printRegisters()
{
    u32 reg;
    if ( readReg( MPIX2_CONF_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        throw MyException("### Unable to read config register");
    }
    std::cout<<"Config: 0x"<<std::setw(8)<<std::setfill('0')<<std::hex<<reg<<std::dec<<std::endl;
    if ( readReg( MPIX2_STATUS_REG_OFFSET, &reg ) != MPXERR_NOERROR )
    {
        throw MyException("### Unable to read status register");
    }
    std::cout<<"Status: 0x"<<std::setw(8)<<std::setfill('0')<<std::hex<<reg<<std::dec<<std::endl;
}

void DetectorTest::printHwInfo(std::vector<int> hwpar, bool isFloat)
{
    std::vector<int>::iterator i;
    HwInfoItem hwitm;
    char tmp[255];
    hwitm.data = tmp;
    int sz = 4;
    unsigned long data_lu = 0;
    float data_f = 0;

    for (i = hwpar.begin(); i != hwpar.end(); i++)
    {
        if( getHwInfo(*i, &hwitm, &sz) != MPXERR_NOERROR )
        {
            throw MyException("### getHwInfo(0, hwitm, zz) failed !!!");
        }
        if (isFloat == false)
        {
            data_lu = *(static_cast<unsigned int *> ( hwitm.data ));
            std::cout << hwitm.name << " --> " <<data_lu<< std::endl;
        }
        else
        {
            data_f = *(static_cast<float *> ( hwitm.data ));
            std::cout << hwitm.name << " --> " <<data_f<< std::endl;
        }
    }
}

void DetectorTest::printHwInfo()
{
    std::vector<int> hwparsi;
    hwparsi.push_back(HW_ITEM_LIBVERSION);
    hwparsi.push_back(HW_ITEM_RELAXDSWVERSION);
    hwparsi.push_back(HW_ITEM_PORTNR);
    if (_bQuiet == false)
    {
        hwparsi.push_back(HW_ITEM_BOARDTEMP);
        hwparsi.push_back(HW_ITEM_DETECTORTEMP);
    }
    hwparsi.push_back(HW_ITEM_TESTPULSEFREQ);
    hwparsi.push_back(HW_ITEM_BIAS_VOLTAGE_ADJUST);
    hwparsi.push_back(HW_ITEM_VDD_ADJUST);
    hwparsi.push_back(HW_ITEM_FIRSTCHIPNR);
    hwparsi.push_back(HW_ITEM_LOGVERBOSE);
    hwparsi.push_back(HW_ITEM_PARREADOUT);
    hwparsi.push_back(HW_ITEM_STOREPIXELSCFG);
    hwparsi.push_back(HW_ITEM_STOREDACS);
    hwparsi.push_back(HW_ITEM_ERASE_STORED_CFG);
    hwparsi.push_back(HW_ITEM_CONF_TPX_CLOCK);
    hwparsi.push_back(HW_ITEM_CONF_RO_CLOCK_125MHZ);
    hwparsi.push_back(HW_ITEM_CONF_TPX_PRECLOCKS);
    hwparsi.push_back(HW_ITEM_SET_TRIG_TYPE);


    std::vector<int> hwparsf;
    if (_bQuiet == false)
    {
        hwparsf.push_back(HW_ITEM_VDD);
        hwparsf.push_back(HW_ITEM_VDDA);
    }
    hwparsf.push_back(HW_ITEM_TESTPULSELO);
    hwparsf.push_back(HW_ITEM_TESTPULSEHI);
    if (_bQuiet == false)
    {
        hwparsf.push_back(HW_ITEM_ANALOGCURR);
        hwparsf.push_back(HW_ITEM_DIGITALCURR);
    }
    printHwInfo(hwparsi, false);
    printHwInfo(hwparsf, true);
    printRegisters();
}

void DetectorTest::resetDetectorMatrix()
{
    if(resetMatrix() != MPXERR_NOERROR)
    {
        std::stringstream s;
        s << "Error: resetMatrix() 3 failed" << std::endl;
        throw MyException(s.str());
    }
    else
    {
        std::cout << "resetMatrix() 3 OK" << std::endl;
    }
}

void DetectorTest::openRawDataFile(std::string *dirName)
{
    if (_captureToFile == true)
    {
        std::string fileName = getRawDataFileName(*dirName);
        _rawDataFile.open(fileName.c_str());
        if (_rawDataFile.is_open() == false)
        {
            std::stringstream s;
            s << "%Raw datafile not open "<< fileName;
            throw MyException(s.str());
        }
        else if (_bQuiet == false)
        {
            std::cout<<"Opened raw datafile: "<<fileName<<std::endl;
        }
    }
}

void DetectorTest::writeRawData(u8* data, u32 nbytes, int lostRows)
{
    if (_captureToFile == true)
    {
        if (_overwriteFrames == true)
        {
            _rawDataFile.seekp(0, std::ios_base::beg);
        }
        _rawDataFile.write((const char*)&lostRows, sizeof(int));
        _rawDataFile.write((const char*)&nbytes, sizeof(u32));
        _rawDataFile.write((const char*)data, sizeof(u8) * nbytes);
    }
}

void DetectorTest::closeRawDataFile()
{
    if (_rawDataFile.is_open())
    {
        _rawDataFile.close();
    }
}

bool DetectorTest::isShutterClosed()
{
    bool ret = false;
    if (_enableExternalTrigger)
    {
        if (_eFrameNotification == EDT_FRAME_CALLBACK)
        {
            ret = _shutterClosed;
        }
        else if (_eFrameNotification == EDT_FRAME_GETBUSY)
        {
            bool busy;
            getBusy(&busy);
            ret = !busy;
        }
        else if (_eFrameNotification == EDT_FRAME_NEWFRAME)
        {
            ret = newFrame(true);
        }
    }
    else
    {
        ret = timerExpired();
    }
    if (ret == true)
    {
        _shutterCloseCount++;
    }
    return ret;
}

void DetectorTest::setRealTimeTimeouts(bool realtime)
{
    if (realtime == true)
    {
        _shutterCloseTimeoutUs = 10000;
    }
}

int DetectorTest::waitForShutter()
{
    int cycles = 0;
    unsigned long t0 = getDetectorTestTime();
    unsigned long dt;
    _shutterClosed = false;
    while (isShutterClosed() == false)
    {
        cycles++;
        dt = getDetectorTestTime() - t0;
        if  (dt >  _shutterCloseTimeoutUs)
        {
            std::cout<<"Wait for shutter Timeout"<<std::endl;
            break;
        }
    }
    return cycles;
}

void DetectorTest::runCodecAndSave(void* data, u32 nbytes, int lostRows)
{
    i16* decodeOutput = NULL;
    if (_multithreaded == true)
    {
        decodeOutput = (i16*)getBuffer(false);
        if (parReadout() == true)
        {
            _codec->decodePar((u8*)data, (i16*)decodeOutput);
        }
        else
        {
            _codec->decodeSgl((u8*)data, (i16*)decodeOutput);
        }
        releaseBuffer(data, (_readRaw == true));
        data = decodeOutput;
        nbytes = MPIX_PIXELS * MPIX_MAX_DEVS;
    }
    writeRawData((u8*)data, nbytes, lostRows);
    if (decodeOutput != NULL)
    {
        releaseBuffer(decodeOutput, false);
    }
    else
    {
        releaseBuffer(data, (_readRaw == true));
    }
}

int DetectorTest::captureFrames()
{
    void *myarray;
    int cycles = 0;
    int lostRows = 0;
    u32 nbytes = 0;
    int ret = MPXERR_NOERROR;
    int myarraySize;
    if (_readRaw == true)
    {
        myarraySize = MPIX_MAX_DEVS * MPIX_FRAME_BYTES;
    }
    else
    {
        myarraySize = MPIX_MAX_DEVS * MPIX_PIXELS;
    }

    if (_enableExternalTrigger)
    {
        startAcquisition();
        cycles = waitForShutter();
        stopAcquisition();
    }
    else
    {
        openShutter();
        cycles = waitForShutter();
    }
    myarray = getBuffer(_readRaw == true);
    if (myarray == NULL)
    {
        std::cout << "Unable to get buffer..." << std::endl;
    }
    else
    {
        if (_readRaw == true)
        {
            ret = readMatrixRaw((u8*)myarray, &nbytes, &lostRows);
        }
        else
        {
            ret = readMatrix((i16*)myarray, myarraySize);
        }
        if (ret != MPXERR_NOERROR)
        {
            std::cout<<"Could not readmatrix "<<std::hex<<ret<<std::dec<<std::endl;
            releaseBuffer(myarray, _readRaw == true);
        }
        else
        {
            // runCodecAndSave will release the buffer when it is done...
            if (_multithreaded == true)
            {
                _jobBatcher->enqueueJob(boost::protect(boost::bind(&DetectorTest::runCodecAndSave, this, myarray, myarraySize, lostRows)));
            }
            else
            {
                runCodecAndSave(myarray, myarraySize, lostRows);
            }
        }
    }
    return cycles;
}

void DetectorTest::configureExternalTrigger()
{
    if (_eFrameNotification == EDT_FRAME_CALLBACK)
    {
        setCallbackData((INTPTR)this);
        setCallback((HwCallback)&DetectorTest::mpxCallback);
    }

    if (_enableExternalTrigger)
    {
        AcqParams  acqPars;
        acqPars.enableCst        = FALSE;
        acqPars.mode             = ACQMODE_HWTRIGSTART_HWTRIGSTOP;
        acqPars.polarityPositive = TRUE;
        acqPars.time             = _timerValue;
        acqPars.useHwTimer       = (_timerValue == 0) ? false : true;
        setAcqPars(&acqPars);
        configAcqMode(_enableExternalTrigger);
    }
}

void DetectorTest::printFramerate(unsigned long timeDiff, int numOfCycles)
{
    if (numOfCycles != 0)
    {
        std::cout << "Framerate, " << 1 / (static_cast<float>(timeDiff) * 1e-6 / (static_cast<float>(numOfCycles))) << ", per sec" << std::endl;
    }
}

#define CIRCULAR_BUFFER_SIZE 10

void printCircBuffs(const std::vector<u16>& lastFrameCnts, const std::vector<u16> &expectedLastFrameCnts)
{
    for (int i = 0; i < CIRCULAR_BUFFER_SIZE; i++)
    {
        std::cout << std::hex << "L=" << lastFrameCnts[i] << " E=" << expectedLastFrameCnts[i] << std::dec << std::endl;
    }
}

#define NUM_BUFFERS 1024
void DetectorTest::allocateBuffers()
{
    u8* p8;
    u16* p16;
    int bufferIndex;
    int size;
    int prefaultIndex;
    for (bufferIndex = 0; bufferIndex < NUM_BUFFERS; bufferIndex++)
    {
        size = MPIX_MAX_DEVS * MPIX_FRAME_BYTES;
        p8 = new u8[size];
        for (prefaultIndex = 0; prefaultIndex < size; prefaultIndex++)
        {
            p8[prefaultIndex] = 0;
        }
        _encodedBufferQueue.enqueue(p8);

        size = MPIX_MAX_DEVS * MPIX_PIXELS;
        p16 = new u16[size];
        for (prefaultIndex = 0; prefaultIndex < size; prefaultIndex++)
        {
            p16[prefaultIndex] = 0;
        }
        _decodedBufferQueue.enqueue(p16);
    }
}

void DetectorTest::freeBuffers()
{
    int bufferIndex;
    u8* p8;
    u16* p16;
    for (bufferIndex = 0; bufferIndex < NUM_BUFFERS; bufferIndex++)
    {
        _encodedBufferQueue.dequeue(p8);
        delete []p8;

        _decodedBufferQueue.dequeue(p16);
        delete []p16;
    }
}

void DetectorTest::releaseBuffer(void *buffer, bool encoded)
{
    if (encoded == true)
    {
        u8 *p = (u8*)buffer;
        _encodedBufferQueue.enqueue(p);
    }
    else
    {
        u16 *p = (u16*)buffer;
        _decodedBufferQueue.enqueue(p);
    }
}

void* DetectorTest::getBuffer(bool encoded)
{
    void *ret = NULL;
    if (encoded == true)
    {
        u8 *p;
        if (_encodedBufferQueue.waitDequeue(p, 10) == true)
        {
            ret = p;
        }
    }
    else
    {
        u16 *p;
        if (_decodedBufferQueue.waitDequeue(p, 10) == true)
        {
            ret = p;
        }
    }
    return ret;
}

void getTimeStampNow(std::string *buf)
{
    std::stringstream s;
    time_t now;
    struct tm *current;
    now = time(0);
    current = localtime(&now);
    s << current->tm_hour << ":" << current->tm_min << ":" << current->tm_sec;
    *buf = s.str();
}


void DetectorTest::runCaptureTest(std::string *dirName)
{
    TIME_DECL;
    int cycles = 0;
    u16 expectedFrameCnt = 1;
    std::vector<u16> lastFrameCnts;
    std::vector<u16> expectedLastFrameCnts;
    int printCircs = 0;
    int cycleCnt = 0;
    lastFrameCnts.resize(CIRCULAR_BUFFER_SIZE);
    expectedLastFrameCnts.resize(CIRCULAR_BUFFER_SIZE);

    allocateBuffers();
    openRawDataFile(dirName);
    for (int n = 0; n < _numOfCycles; n++)
    {
        TIME_IT(
            cycles += captureFrames()
        );
        u16 lastFrameCnt = lastFrameCount();
        lastFrameCnts[n % CIRCULAR_BUFFER_SIZE] = lastFrameCnt;
        expectedLastFrameCnts[n % CIRCULAR_BUFFER_SIZE] = expectedFrameCnt;
        if (_bQuiet == false)
        {
            if ((lastFrameCnt != expectedFrameCnt) || (printCircs > 0))
            {
                std::string time;
                getTimeStampNow(&time);
                std::cout << "Issue with last frame cnt: " << std::hex << lastFrameCnt << " != " << expectedFrameCnt << " at cycle: " << std::dec << n << " " << time << std::endl;
                printCircBuffs(lastFrameCnts, expectedLastFrameCnts);
                if (lastFrameCnt != expectedFrameCnt)
                {
                    printCircs = 5;
                }
                else
                {
                    printCircs--;
                }
                expectedFrameCnt = lastFrameCnt;
            }
            if (timeDiffLast > _shutterCloseTimeoutUs)
            {
                std::string time;
                getTimeStampNow(&time);
                std::cout << "Timediff longer than 8.3ms " << timeDiffLast << " Frame number " << expectedFrameCnt << " " << time << std::endl;
            }
        }
        expectedFrameCnt++;
        cycleCnt++;
        if ((_periodicFrameRate == true) && (timeDiffCumulative > 1000000))
        {
            printFramerate(timeDiffCumulative, cycleCnt);
            timeDiffCumulative = 0;
            cycleCnt = 0;
        }
    }
    _jobBatcher->waitForJobs();
    enableExtTrigger(false);

    std::cout << "Last frame count " << std::dec << lastFrameCount() << std::endl;
    std::cout << "Callback count " << std::dec << _callbackCount << std::endl;
    std::cout << "Shutter close count " << std::dec << _shutterCloseCount << std::endl;
    if (_bQuiet == false)
    {
        std::cout << "done! in " << std::dec << cycles << " cycles " << std::endl;
    }
    if (_periodicFrameRate == false)
    {
        printFramerate(timeDiffCumulative, _numOfCycles);
    }

    closeRawDataFile();
    freeBuffers();
}

void DetectorTest::runDigitalTest()
{
    TIME_DECL;
    int i;
    i16 *inputData;
    i16 *outputData;
    int errCnt = 0;
    int dataSize = MPIX_PIXELS * MPIX_MAX_DEVS;

    inputData = new i16[dataSize];
    outputData = new i16[dataSize];

    for (int n = 0; n < _numOfCycles; n++)
    {
        errCnt = 0;
        for (i = 0; i < dataSize; i++)
        {
            inputData[i] = (i + n) % 11811;
            outputData[i] = 0;
        }

        TIME_IT(
            writeMatrix(inputData, dataSize);
            readMatrix(outputData, dataSize);
        );
        for (i = 0; (i < dataSize) && (errCnt < 10); i++)
        {
            if (inputData[i] != outputData[i])
            {
                errCnt++;
                std::cout<<"Error at cycle: "<<n<<" offset "<<std::dec<<i<<" "<<inputData[i]<<" != "<<outputData[i]<<std::endl;
            }
        }
    }
    printFramerate(timeDiffCumulative, _numOfCycles);
    delete []inputData;
    delete []outputData;
}

void DetectorTest::runCodecTest(CodecTest *ct, const std::string ioDir)
{
    TIME_DECL;
    std::string filename = ioDir + "/actual.dat";
    /* cache fill */
    ct->runTest();
    for (int i = 0; i < _numOfCycles; i++)
    {
        TIME_IT(
            ct->runTest()
        );
    }
    ct->writeOutputFile(filename);
    printFramerate(timeDiffCumulative, _numOfCycles);
}

void DetectorTest::runCodecTest(std::string *dirName)
{
    CodecTest *ct = new CodecTest(dirName, _bQuiet, parReadout(), _codec);
    ct->generateInputStream();
    runCodecTest(ct, (*dirName));
    delete ct;
}

void DetectorTest::runPingTest()
{
    MpxModule *module;
    int id = 0;
    module = new MpxModule( id, true );
    if (module->ping()) 
    {
        std::cout<< "Found module with ID=" << id << std::endl;
    }
    else
    {
        std::cout<< "Module with ID=" << id << " not found"<<std::endl;
    }
    delete module;
}

void DetectorTest::changeId(int id)
{
    int ret = storeId(id);
    if (ret != MPXERR_NOERROR)
    {
        std::cout<<"Error: Unable to store ID: "<<ret<<std::endl;
    }
    else
    {
        std::cout<<"Wrote ID to "<<id<<std::endl;
    }
}

void DetectorTest::runIdTest()
{
    TIME_DECL;

    for (int i = 0; i < _numOfCycles; i++)
    {
        TIME_IT(changeId(1);changeId(0);)
    }
    printFramerate(timeDiffCumulative, _numOfCycles);
}

void DetectorTest::runTest(std::string *dirName)
{
    _codec->setUseSSE2(_enableSSE2);
    configureExternalTrigger();
    enableTimer((_timerValue == 0) ? false : true, _timerValue);
    resetFrameCounter();
    resetFrameCounter();
    boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
    enableExtTrigger(_enableExternalTrigger);

    std::string SSE2 = (_codec->useSSE2() == true) ? "sse2" : "norm";
    std::cout<<"Using "<< SSE2<<std::endl;
    _log->scratch << "Starting test";
    _log->logScratch(MPX_DEBUG);
    switch (_eTestType)
    {
    case EDT_CAPTURE_TEST:
        runCaptureTest(dirName);
        break;
    case EDT_DIGITAL_TEST:
        runDigitalTest();
        break;
    case EDT_CODEC_TEST:
        runCodecTest(dirName);
        break;
    case EDT_PING_TEST:
        runPingTest();
        break;
    case EDT_ID_TEST:
        runIdTest();
        break;
    }
}
