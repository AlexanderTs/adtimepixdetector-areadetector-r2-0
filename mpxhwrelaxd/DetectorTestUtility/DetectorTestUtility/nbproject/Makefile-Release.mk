#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1472/shared.o \
	${OBJECTDIR}/CodecTest.o \
	${OBJECTDIR}/DetectorTest.o \
	${OBJECTDIR}/compat.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L../../Release/shared -lmpxhwrelaxd ../../boost_1_52_0/stage/lib/libboost_system.a ../../boost_1_52_0/stage/lib/libboost_thread.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ../../Release/shared/DetectorTestUtility

../../Release/shared/DetectorTestUtility: ../../boost_1_52_0/stage/lib/libboost_system.a

../../Release/shared/DetectorTestUtility: ../../boost_1_52_0/stage/lib/libboost_thread.a

../../Release/shared/DetectorTestUtility: ${OBJECTFILES}
	${MKDIR} -p ../../Release/shared
	${LINK.cc} -o ../../Release/shared/DetectorTestUtility ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1472/shared.o: ../shared.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1472
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../common -I../../src -I../../boost_1_52_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1472/shared.o ../shared.cpp

${OBJECTDIR}/CodecTest.o: CodecTest.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../common -I../../src -I../../boost_1_52_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CodecTest.o CodecTest.cpp

${OBJECTDIR}/DetectorTest.o: DetectorTest.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../common -I../../src -I../../boost_1_52_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DetectorTest.o DetectorTest.cpp

${OBJECTDIR}/compat.o: compat.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../common -I../../src -I../../boost_1_52_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/compat.o compat.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../common -I../../src -I../../boost_1_52_0 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ../../Release/shared/DetectorTestUtility

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
