#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=../../Debug/shared
CND_ARTIFACT_NAME_Debug=DetectorTestUtility
CND_ARTIFACT_PATH_Debug=../../Debug/shared/DetectorTestUtility
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=detectortestutility.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/detectortestutility.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=../../Release/shared
CND_ARTIFACT_NAME_Release=DetectorTestUtility
CND_ARTIFACT_PATH_Release=../../Release/shared/DetectorTestUtility
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=detectortestutility.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/detectortestutility.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
