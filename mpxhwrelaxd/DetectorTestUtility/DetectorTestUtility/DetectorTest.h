#ifndef DETECTORTEST_H
#define DETECTORTEST_H
#include "JobBatcher.h"
#include "ThreadSafeQueue.h"

#include <iostream>
#include <fstream>
#include <string>
#include "mpxmodule.h"
#include "CodecTest.h"

enum EDTestType
{
    EDT_CAPTURE_TEST,
    EDT_DIGITAL_TEST,
    EDT_CODEC_TEST,
    EDT_PING_TEST,
    EDT_ID_TEST,
};

enum EDFrameNotification
{
    EDT_FRAME_CALLBACK,
    EDT_FRAME_GETBUSY,
    EDT_FRAME_NEWFRAME,
};

class DetectorTest : public MpxModule
{
public:
    DetectorTest(const int deviceId, const std::string &ioDir, bool bQuiet);
    ~DetectorTest();

    void readRealDacs(std::string filename);
    void readHwDacs(std::string filename);
    void readPixelsCfg(std::string filename);
    void printHwInfo();
    void resetDetectorMatrix();
    void runTest(std::string *dirName);

    void processRealDac(int *chipnr, int *index, std::string* key, std::string *value);
    void processHwDac(std::string *key, std::string *value);
    void setRealTimeTimeouts(bool realtime);
protected:
    void readFsrs();
    void writeFsrs();
    void openRawDataFile(std::string *dirName);
    void writeRawData(u8* data, u32 nbytes, int lostRows);
    void closeRawDataFile();
    void printRegisters();
    void processConfigApis(std::string *key, std::string *value);
    static void mpxCallback(INTPTR userData, int eventType, void *data);
    int waitForShutter();
    void allocateBuffers();
    void freeBuffers();
    int captureFrames();
    void configureExternalTrigger();
    bool isShutterClosed();
    void printHwInfo(std::vector<int> hwpar, bool isFloat);
    void runCaptureTest(std::string *dirName);
    void runDigitalTest();
    void runCodecTest(CodecTest *ct, const std::string ioDir);
    void runCodecTest(std::string *dirName);
    void runPingTest();
    void runIdTest();
    void changeId(int id);
    void printFramerate(unsigned long timeDiff, int numOfCycles);
    void runCodecAndSave(void* data, u32 nbytes, int lostRows);
    void* getBuffer(bool encoded);
    void releaseBuffer(void *buffer, bool encoded);

    int _ndevs;
    int _dacs[4][20];
    int _TDD[4][20];
    int _ndacs;
    std::ofstream _rawDataFile;
    int _timerValue;
    bool _enableExternalTrigger;
    bool _enableSSE2;
    volatile bool _shutterClosed;
    volatile int  _callbackCount;
    int _numOfCycles;
    bool _readRaw;
    bool _captureToFile;
    EDTestType _bDigitalTest;
    bool _bQuiet;
    int _shutterCloseCount;
    EDTestType _eTestType;
    EDFrameNotification _eFrameNotification;
    bool _periodicFrameRate;
    unsigned long _shutterCloseTimeoutUs;
    bool _multithreaded;
    bool _overwriteFrames;
    boost::shared_ptr<AsiJobBatcher> _jobBatcher;
    AsiThreadSafeQueue<u16*> _decodedBufferQueue;
    AsiThreadSafeQueue<u8*> _encodedBufferQueue;
private:
};
#endif
