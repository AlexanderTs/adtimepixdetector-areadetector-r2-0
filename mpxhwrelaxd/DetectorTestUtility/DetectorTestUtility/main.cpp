#include <iostream>
#include <boost/bind/protect.hpp>
#include <boost/lexical_cast.hpp>
#include "DetectorTest.h"
#include "../shared.h"

bool setPriority(int distanceFromTop)
{
    bool ret = false;
#if defined(_WINDLL) || defined(WIN32)
    UNREF_PARAM(distanceFromTop);
#else
#ifndef __APPLE__
    sched_param param;
    int priority = sched_get_priority_max(SCHED_FIFO) - distanceFromTop;
    param.sched_priority = priority;
    if (sched_setscheduler(0, SCHED_FIFO, &param) == -1)
    {
        std::cout << "Unable to change priority (might not have preempt_rt, or might not have permission to invoke sched_setscheduler)" << std::endl;
    }
    else
    {
        ret = true;
    }
#endif
#endif
    return ret;
}

int main(int argc, char** argv)
{
    DetectorTest *dt = NULL;
    try
    {
        bool realtime = setPriority(6);
        std::string ioDir = getParamArgument(argc, argv, "-d", ".");
        bool bQuiet = getParam(argc, argv, "-q");
        std::string deviceIdStr = getParamArgument(argc, argv, "-i", "0");
        int deviceId = boost::lexical_cast<int>(deviceIdStr);
        dt = new DetectorTest(deviceId, ioDir, bQuiet);
        dt->readHwDacs(ioDir + "/HW.dacs");
        dt->readPixelsCfg(ioDir + "/pixels.bpc");
        dt->readRealDacs(ioDir + "/real.dacs");
        dt->resetDetectorMatrix();
        dt->printHwInfo();
        if (realtime == true)
        {
            dt->setRealTimeTimeouts(true);
        }
        dt->runTest(&ioDir);
    }
    catch (MyException& e)
    {
        std::cout<<e.what()<<std::endl;
    }
    if (dt != NULL)
    {
        delete dt;
    }
}
