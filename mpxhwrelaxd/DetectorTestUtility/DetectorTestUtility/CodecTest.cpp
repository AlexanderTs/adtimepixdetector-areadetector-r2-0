#include <fstream>
#include <iostream>
#include <sstream>
#include "../shared.h"
#include "CodecTest.h"
#include "mpxerrors.h"
#include "mpxmodule.h"
#include "common.h"



void CodecTest::generateInputStream(uint8_t *decodeInput)
{
    int16_t *tmp = new int16_t[MPIX_FRAME_BYTES];

    for (int i = 0; i < MPIX_FRAME_BYTES; i++)
    {
        tmp[i] = i % 5000;
    }
    _codec->encodeSgl(tmp, decodeInput);
    delete []tmp;
}

int CodecTest::getNrChips()
{
    int nrChips = 1;
    if (_bDecodePar == true)
    {
        nrChips = 4;
    }
    return nrChips;
}

void CodecTest::dumpInputStreamToFile(u8 *decodeInput, int size)
{
    std::ofstream o;
    std::string filename = _ioDir + "input.dat";
    o.open(filename.c_str(), std::ios::binary | std::ios::out);
    o.write((const char*)decodeInput, size);
    o.close();
}

void CodecTest::generateInputStream()
{
    int nrChips = getNrChips();
    _decodeInput = new u8[MPIX_FRAME_BYTES * nrChips];
    _decodeOutput = new u16[MPIX_PIXELS * nrChips];

    std::cout<<"MPIX_FRAME_BYTES: "<<std::dec<<MPIX_FRAME_BYTES<<std::endl;
    std::cout<<"MPIX_PIXELS: "<<std::dec<<MPIX_PIXELS<<std::endl;

    for (int chip = 0; chip < nrChips; chip++)
    {
        generateInputStream(_decodeInput + (MPIX_FRAME_BYTES * chip));
    }
    //dumpInputStreamToFile(_decodeInput, MPIX_FRAME_BYTES * nrChips);
}

void CodecTest::writeOutputFile(std::string filename)
{
    int nrChips = getNrChips();
    std::ofstream rawDataFile;
    rawDataFile.open(filename.c_str(), std::ios::out | std::ios::binary);
    if (rawDataFile.is_open() == false)
    {
        std::stringstream s;
        s << "%Raw datafile not open "<< filename;
        throw MyException(s.str());
    }
    else if (_bQuiet == false)
    {
        std::cout<<"Opened raw datafile: "<<filename<<std::endl;
    }
    for (int i = 0; i < MPIX_PIXELS * nrChips; i++)
    {
        rawDataFile.write((const char*)&(_decodeOutput[i]), sizeof(_decodeOutput[i]));
    }
    rawDataFile.close();
}

void CodecTest::runTest()
{
    if (_bDecodePar == true)
    {
        _codec->decodePar(_decodeInput, (i16*)_decodeOutput);
    }
    else
    {
        _codec->decodeSgl(_decodeInput, (i16*)_decodeOutput);
    }
}

CodecTest::CodecTest(std::string *ioDir, bool bQuiet, bool bDecodePar, MpxCodecMgr* codec)
{
    _ioDir = *ioDir;
    _bQuiet = bQuiet;
    _codec = codec;
    _bDecodePar = bDecodePar;
    _decodeInput = NULL;
    _decodeOutput = NULL;
}

CodecTest::~CodecTest()
{
    if (_decodeInput)
    {
        delete []_decodeInput;
    }
    if (_decodeOutput)
    {
        delete []_decodeOutput;
    }
}

