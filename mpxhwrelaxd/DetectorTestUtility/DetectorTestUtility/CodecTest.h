#ifndef CODECTEST_H
#define CODECTEST_H
#include <string>
#include "mpxcodecmgr.h"
#include "asitypes.h"

class CodecTest
{
public:
    CodecTest(std::string *ioDir, bool bQuiet, bool bDecodePar, MpxCodecMgr *codec);
    ~CodecTest();
    void generateInputStream();
    void runTest();
    void writeOutputFile(std::string filename);

protected:
    void generateInputStream(u8 *decodeInput);
    void dumpInputStreamToFile(u8 *decodeInput, int size);
    int getNrChips();

    MpxCodecMgr *_codec;
    bool _bDecodePar;
    bool _bQuiet;
    
    u8 * _decodeInput;
    u16* _decodeOutput;
    std::string _ioDir;

private:
};

#endif