#include <sys/stat.h>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include "shared.h"

std::string getParamArgument(int argc, char** argv, std::string param, std::string defVal)
{
    std::string ret = defVal;
    for (int i = 1; i < argc; i++)
    {
        if ((param.compare(argv[i]) == 0) && (argc > (i + 1)))
        {
            ret = argv[i+1];
            break;
        }
    }
    return ret;
}

bool getParam(int argc, char** argv, std::string param)
{
    bool ret = false;
    for (int i = 1; i < argc; i++)
    {
        if (param.compare(argv[i]) == 0)
        {
            ret = true;
        }
    }
    return ret;
}

void getLineFromFile(std::ifstream *f, std::string* key, std::string *value)
{
    size_t index;
    std::string line;
    getline( *f, line, '\n');
    index = line.find('#');
    if (index != std::string::npos)
    {
        line = line.substr(0, index);
    }
    if (line.length() > 0)
    {
        index = line.find(':');
        if (index != std::string::npos)
        {
            *key = line.substr(0, index);
            *value = line.substr(index+1, line.length());
        }
        else
        {
            *key = "";
        }
    }
}


CfgFileReader::CfgFileReader(std::string filename, void* data)
{
    _filename = filename;
    _data = data;
}

CfgFileReader::~CfgFileReader()
{
}

// trim from start
static inline std::string &ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) 
{
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) 
{
    return ltrim(rtrim(s));
}

void CfgFileReader::readCfgFile()
{
    std::ifstream f;
    std::string key, value;
    f.open(_filename.c_str());
    if (f.is_open() == false)
    {
        std::stringstream s;
        s<< "Failed to open file " <<_filename;
        throw MyException(s.str());
    }

    while (f.eof() == false)
    {
        getLineFromFile(&f, &key, &value);
        if (key.length() > 0)
        {
            trim(key);
            trim(value);
            varSetFunctionoid(&key, &value, _data);
        }
    }
    f.close();
}

bool fileExists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}

std::string getRawDataFileName(std::string dirName)
{
    std::ostringstream filename;
    if (dirName[dirName.length() - 1] != '/')
    {
        dirName += "/";
    }
	for (int i = 0; i < 1000000; i++)
	{
	    filename.str("");
	    filename.clear();
	    filename<<dirName<<"test_"<<i<<".tpxRaw";
	    if (fileExists(filename.str()) == false)
	    {
	        break;
	    }
	}
	return filename.str();
}
