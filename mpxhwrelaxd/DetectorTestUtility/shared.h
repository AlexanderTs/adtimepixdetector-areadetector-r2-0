#ifndef SHARED_H
#define SHARED_H
#include <exception>
#include <fstream>

class MyException : public std::exception
{
public:
    MyException(const std::string m) : msg(m) {}
    ~MyException() throw() {};
    const char* what(){ return msg.c_str(); }
private:
    std::string msg;
};

class CfgFileReader
{
public:
    CfgFileReader(std::string filename, void* data);
    ~CfgFileReader();
    void readCfgFile();

protected:
    virtual void varSetFunctionoid(std::string *key, std::string *value, void* data) = 0;
private:
    std::string _filename;
    void *_data;
};

std::string getParamArgument(int argc, char** argv, std::string param, std::string defVal);
bool getParam(int argc, char** argv, std::string param);
void getLineFromFile(std::ifstream *f, std::string* key, std::string *value);
std::string getRawDataFileName(std::string dirName);
bool fileExists(const std::string& filename);

#endif