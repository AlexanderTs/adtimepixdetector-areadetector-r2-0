#!/usr/bin/env python
import getopt, sys, difflib, datetime, os, runtestutil, re, getpass, platform
from shutil import copyfile
from pprint import pprint
from glob import glob

def usage():
    print("-d dir - Specify test directory to run (-d test1)")
    print("         -d Can be specified multiple times")
    print("-s dir - Skip test directory (-s test1)")
    print("         -s Can be specified multiple times")
    print("--valgrind - Run against valgrind (non-windows platforms)")
    print("--UPDATE - Update expected results")
    print("-r, --release - Run with release binaries")
    print("-i id - Use relaxd board with specified ID")
    os._exit(1);

class DetectorTest:
    _framerates = {}
    _firmwareVer = ""
    _driverVer = ""
    _valgrindRegEx = "^==\d+==.*" # valgrind output starts with ==12345==
    _valgrindLeaks = []
    def __init__(self, id, release, runValgrind):
        self._release = release
        self._runValgrind = runValgrind
        self._id = id

    def writeActualResults(self, actualOutput, actualOutputFilename):
        f = open(actualOutputFilename, 'wb')
        for out in actualOutput:
            f.write(out + "\n")
        f.close()

    def updateTest(self, testDir, update, actualOutputFilename, expectedOutputFilename):
        if (update == True):
            backupFile = expectedOutputFilename + "." + datetime.datetime.now().strftime("%Y%m%d%H%M%S")
            copyfile(expectedOutputFilename, backupFile)
            print "Updating", testDir, "-> backing up current expected results to:", backupFile
            copyfile(actualOutputFilename, expectedOutputFilename)
    
    def diffResults(self, testDir, update, actualOutputFilename, expectedOutputFilename):
        diffList = []
        diffList = list(difflib.context_diff(open(actualOutputFilename).readlines(), open(expectedOutputFilename).readlines()))
        if (len(diffList) > 0):
            self.updateTest(testDir, update, actualOutputFilename, expectedOutputFilename)
            diffList.append("\n" + actualOutputFilename + " differs from " + expectedOutputFilename)
        return diffList
    
    def diffBinaryResults(self, testDir, update, actualOutputFilename, expectedOutputFilename):
        diffList = []
        actualOutput = open(actualOutputFilename, "rb").read()
        expectedOutput = open(expectedOutputFilename, "rb").read()
        if (len(actualOutput) != len(expectedOutput)):
            diffList.append("\n" + actualOutputFilename + " != " + expectedOutputFilename)
        for index in range (len(actualOutput)):
            if (actualOutput[index] != expectedOutput[index]):
                diffList.append("\n" + actualOutputFilename + " differs from " + expectedOutputFilename)
                break
        if (len(diffList) > 0):
            self.updateTest(testDir, update, actualOutputFilename, expectedOutputFilename)
        return diffList
        
    def checkResults(self, testDir, diffList):
        ret = 0
        if (len(diffList) == 0):
            if (len(self._valgrindLeaks) == 0):
                print("\t\tPASS\t\t" + self._framerates[testDir])
                ret = 1
            else:
                print "\n"
                for line in self._valgrindLeaks:
                    print line,
                print "\n\nMemory leaks in", testDir, "\t\tFAILURE\t\t"  + self._framerates[testDir]
        else:
            print "\n"
            for line in diffList:
                print line,
            print "\n\nDiff in", testDir, "\t\tFAILURE\t\t"  + self._framerates[testDir]
        return ret

    def getLines(self, actualOutput, searchStr):
        ret = []
        for line in actualOutput:
            if (re.search(searchStr, line) != None):
                ret.append(line)
        return ret

    def grepOut(self, actualOutput, searchStr):
        ret = [];
        for line in actualOutput:
            if (re.search(searchStr, line) == None):
                ret.append(line)
        return ret

    def getElement(self, lines, index):
        if (len(lines) > index):
            return lines[index]
        else:
            return ""

    def collectData(self, testDir, actualOutput):
        framerate = self.getElement(self.getLines(actualOutput, "Framerate"), 0)
        self._framerates[testDir] = framerate
        self._driverVer = self.getElement(self.getLines(actualOutput, "HW-lib version"), 0)
        self._firmwareVer = self.getElement(self.getLines(actualOutput, "FW version"), 0)
        if (self._runValgrind):
            self._valgrindLeaks = self.getLines(actualOutput, self._valgrindRegEx + "lost.*[1-9][0-9]* bytes.*")

    def runTest(self, testDir, update):
        self.cleanAllFiles(testDir)
        actualOutputFilename = testDir + "/actual.out"
        expectedOutputFilename = testDir + "/expected.out"
        actualOutputDatFilename = testDir + "/actual.dat"
        expectedOutputDatFilename = testDir + "/expected.dat"
        actualOutput = runtestutil.runTestUtil(self._id, self._release, "-q", testDir, self._runValgrind)
        self.collectData(testDir, actualOutput)
        actualOutput = self.grepOut(actualOutput, "FW version")
        actualOutput = self.grepOut(actualOutput, "Framerate")
        actualOutput = self.grepOut(actualOutput, "HW-lib version")
        actualOutput = self.grepOut(actualOutput, self._valgrindRegEx) # remove valgrind output
        self.writeActualResults(actualOutput, actualOutputFilename)
        diffList = self.diffResults(testDir, update, actualOutputFilename, expectedOutputFilename)
        if (os.path.isfile(expectedOutputDatFilename)):
            diffList += self.diffBinaryResults(testDir, update, actualOutputDatFilename, expectedOutputDatFilename)
        return self.checkResults(testDir, diffList)

    def sortedNicely(self, l ): 
        """ Sort the given iterable in the way that humans expect.""" 
        convert = lambda text: int(text) if text.isdigit() else text 
        alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
        return sorted(l, key = alphanum_key)
        
    def removeSkips(self, dirList, skipDirs):
        for skip in skipDirs:
            while (dirList.count(skip) > 0):
                dirList.remove(skip)
        return dirList
        
    def removeNonTests(self, dirList):
        skipDirs = []
        for dir in dirList:
            if (dir.startswith("test") == False):
                skipDirs.append(dir)
        return self.removeSkips(dirList, skipDirs)

    def storeResults(self, dirList):
        release = "Release"
        if (self._release == False):
            release = "Debug"
        username = getpass.getuser()
        filename = "results_log.csv"
        uname = platform.uname()
        f = open(filename, "ab")
        f.write(datetime.datetime.now().strftime("%d/%m/%Y - %H:%M:%S"))
        f.write(", " + self._firmwareVer.replace(" -->", ","))
        f.write(", " + self._driverVer.replace(" -->", ","))
        f.write(", " + release)
        for un in uname:
            f.write(", " + un)
        for dir in dirList:
            framerate = self._framerates[dir]
            if (len(framerate) > 0):
                f.write(", " + dir + ", " + framerate)
        f.write("\n")
        f.close()
        print "Appending results to:", filename

    def runTests(self, testDirs, skipDirs, update):
        storeResults = False
        numTests = 0
        numPass = 0
        # if no tests directories were specified with -d then run all tests
        if (len(testDirs) == 0):
            testDirs = os.listdir(".")
            if (len(skipDirs) == 0):
                storeResults = True
        testDirs = self.removeSkips(testDirs, skipDirs)
        testDirs = self.removeNonTests(testDirs)
        dirList = self.sortedNicely(testDirs)
        for dir in dirList:
            numTests += 1
            numPass += self.runTest(dir, update)
        # only attempt to store results if all tests are run and all tests pass
        if ((storeResults) and (numTests == numPass)):
            self.storeResults(dirList)
        print "Total tests", numTests, "Pass:", numPass, "Fail:", numTests - numPass

    def cleanFiles(self, g):
        for f in glob (g):
            os.unlink (f)
    
    def cleanAllFiles(self, testDir):
        g = testDir + "/mpx*.log"
        self.cleanFiles(g)
        g = testDir + "/*.tpxRaw"
        self.cleanFiles(g)

def main(argv):
    testDirs = []
    skipDirs = []
    update = False
    release = False
    runValgrind = False
    id = "0"
    try:
        opts, args = getopt.getopt(argv, "rhd:s:i:", ["release", "help", "dir=", "skip=", "UPDATE", "valgrind" ])
        for opt, arg in opts:
            if (opt in ('-h', '--help')):
                usage()
            elif (opt in ('-d', '--dir')):
                testDirs.append(arg);
            elif (opt in ('-s', '--skip')):
                skipDirs.append(arg);
            elif (opt in ('--UPDATE')):
                update = True
            elif (opt in ('-r', '--release')):
                release = True
            elif (opt in ('--valgrind')):
                runValgrind = True
            elif (opt in ('-i')):
                id = arg

        detectorTest = DetectorTest(id, release, runValgrind)
        detectorTest.runTests(testDirs, skipDirs, update)
    except IOError, e:
        print e
    except:
        e = sys.exc_info()
        print e
        usage()

if __name__ == "__main__":
    main(sys.argv[1:])
