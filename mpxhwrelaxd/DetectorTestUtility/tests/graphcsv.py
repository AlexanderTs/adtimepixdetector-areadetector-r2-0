#!/usr/bin/env python
from Tkinter import *
import getopt, sys, os, csv

def usage():
    print("-i csv --infile csv - Specify csv file to graph")
    os._exit(1);

class GraphCsv:
    _csvData = {}
    def __init__(self, inFile):
        with open(inFile, 'rb') as csvFile:
            rdr = csv.reader(csvFile, delimiter=',', quotechar='|')
            rowCnt = 0;
            for row in rdr:
                for index in range (len(row)):
                    row[index] = row[index].strip()
                    if (row[index].startswith("test")):
                        key = row[index]
                        val = row[index+2]
                        if (key not in self._csvData):
                            self._csvData[key] = []
                        self._csvData[key].append(val)
                rowCnt = rowCnt + 1

    def graphCol(self, key, values):
        gx = 0
        for x in range (len(values)-1):
            gx += 40
            self._canvas.create_line(gx, values[x], gx + 40, values[x+1], fill="red", tags=key)
        
    def graphIt(self):
        master = Tk()
        self._canvas = Canvas(master, width=640, height=480)
        self._canvas.pack()
        for key, values in self._csvData.iteritems():
            self.graphCol(key, values)
        mainloop()
        
def main(argv):
    inFile = ""
    try:
        opts, args = getopt.getopt(argv, "i:", ["infile=" ])
        for opt, arg in opts:
            if (opt in ('-i', '--infile')):
                inFile = arg
        if (len(inFile) > 0):
            graphCsv = GraphCsv(inFile)
            graphCsv.graphIt()
        else:
            raise Exception("No input CSV file specified")
    except IOError, e:
        print "IOError", e
    except Exception as e:
        print "Exception: ", type(e), e.args, e
        usage()
    except:
        e = sys.exc_info()
        print "Random error", e
        usage()
    
if __name__ == "__main__":
    main(sys.argv[1:])
#master = Tk()
#w = Canvas(master, width=200, height=100)
#w.pack()
#w.create_line(0, 0, 200, 100)
#w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
#w.create_rectangle(50, 25, 150, 75, fill="blue")
#mainloop()