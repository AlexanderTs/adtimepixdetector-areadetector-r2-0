#!/usr/bin/env python
import fnmatch
import os

class CleanTmps:
    _matches = []

    def deleteTmpFiles(self):
        for f in self._matches:
            os.unlink (f)
    
    def getFileList(self, pattern):
        for root, dirnames, filenames in os.walk('.'):
            for filename in fnmatch.filter(filenames, pattern):
                f = os.path.join(root, filename)
                self._matches.append(f)
                print (f)

    def cleanTmpFiles(self):
        self.getFileList('mpxhwrelaxd.dll')
        self.getFileList('DetectorTestUtility.exe')
        self.getFileList('*.tpxRaw')
        self.getFileList('mpx*.log')
        self.getFileList('actual.*')
        self.getFileList('expected.out.*')
        self.getFileList('*.bak')
        if (len(self._matches) > 0):
            answer = raw_input("Delete these files? [y/N]: ")
            if (answer in ('y', 'Y')):
                self.deleteTmpFiles()
        else:
            print("No tmp files found")
            
if __name__ == "__main__":
    cleanTmps = CleanTmps()
    cleanTmps.cleanTmpFiles()
