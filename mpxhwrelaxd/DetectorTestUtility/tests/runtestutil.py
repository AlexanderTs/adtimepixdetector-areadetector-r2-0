#!/usr/bin/env python
import getopt, sys, os, platform, fnmatch
from subprocess import Popen, PIPE, STDOUT
from shutil import copyfile

def usage():
    print("-d dir - Specify test directory to run (-d test1)")
    print("-r, --release - Run with release binaries")
    print("--valgrind - Run against valgrind (non-windows platforms)")
    print("-q  - Run in quiet mode")

def getDebugRelease(release):
    if (release == True):
        rval = "Release"
    else:
        rval = "Debug"
    return rval

def copyFile(filename):
    index = filename.rfind("\\")
    if (index == -1):
        index = filename.rfind("/")
    dest = filename[index + 1:]
    #print "Copy", filename, "to", dest
    copyfile(filename, dest)

def setupEnv(release):
    plat = platform.system()
    if (plat == "Windows"):
        pass
    else:
        ldLibPath = 'LD_LIBRARY_PATH'
        os.environ[ldLibPath] = "../../" + getDebugRelease(release) + "/shared"

def getExecName(release, testDir, runValgrind):
    execname = ""
    if (platform.system() == "Windows"):
        execname = "..\\..\\" + getDebugRelease(release) + "\\DetectorTestUtility.exe"
    else:
        if (runValgrind == True):
            execname = "valgrind "
        execname += "../../" + getDebugRelease(release) + "/shared/DetectorTestUtility"
    return execname

def stripLines(actualOutput):
    ret = []
    for out in actualOutput:
        out = out.strip()
        if (len(out) > 0):
            ret.append(out)
    return ret

def catLog(testDir):
    result = []
    for root, dirnames, filenames in os.walk(testDir):
        for filename in fnmatch.filter(filenames, "mpx-*.log"):
            result.append(filename)
    result = sorted(result)
    f = result[len(result) - 1]
    data = open(testDir + "/" + f).readlines()
    for d in data:
        print d,

def runTestUtil(id, release, quiet, testDir, runValgrind):
    setupEnv(release)
    execname = getExecName(release, testDir, runValgrind)
    execname += " -d " + testDir + " " + quiet + " " + "-i " + id
    print 'Running', execname,
    sys.stdout.flush()
    p = Popen(execname, shell=True, stdout=PIPE, stderr=STDOUT)
    actualOutput = stripLines(p.stdout.read().split("\n"))
    return actualOutput

def runTestUtilCmdLine(argv):
    testDir = ""
    release = False
    quiet = ""
    runValgrind = False
    opts, args = getopt.getopt(argv, "qhrd:", ["help", "release", "dir=", "valgrind"])
    for opt, arg in opts:
        if (opt in ('-h', '--help')):
            usage()
        elif (opt in ('-d', '--dir')):
            testDir = arg
        elif (opt in ('-r', '--release')):
            release = True
        elif (opt in ('-q')):
            quiet = " -q"
        elif (opt in ('--valgrind')):
            runValgrind = True
    actualOutput = runTestUtil(release, quiet, testDir, runValgrind)
    for out in actualOutput:
        print out

if __name__ == "__main__":
    runTestUtilCmdLine(sys.argv[1:])
