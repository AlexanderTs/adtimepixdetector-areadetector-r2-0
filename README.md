# ADTimepixDetector #

### It is **Timepix Detector** project for **AreaDetector-R2**. ###
### It has: ###
*  folder with source codes - **ADTimepixDetecto**r
*  tarball that could be installed on user's computer - **TimepixDetectorInstaller.tar.gz**



**TimepixDetectorInstaller.tar.gz** needs Ubuntu-14 or Ubuntu-16.

For other Linuxes you have to compile libmpxhwrelaxd.a (see https://bitbucket.org/amscins/mpxhwrelaxd).

**ADTimepixDetector** has following subfolders


 - configure
 - db
 - dbd
 - include
 - iocs - with main() application and 'start_epics' script
 - lib
 - libmpxhwrelaxd - MPX driver
 - scripts - 'make-install-tarball' and 'install*' for installing
 - tpxDetectorApp - with tpxDetector.cpp that is core of this application